package madcat.studio.thread;

import java.io.Serializable;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.StopTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.dialog.SCompleteAlarmDialog;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.service.ScheduleAlarmService;
import madcat.studio.utils.ContextPool;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

public class STimerThread extends Thread implements Serializable {
	
	private final String TAG										=	"TimerThread";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private int mHour, mMin, mSec;
	private long mMilSec;
	private boolean mThreadFlag;
	
	private STimer mTimer;
	private int mTimerMode;
	private ArrayList<TimerScheduleList> mCurrentSchedule;
	
	public STimerThread(STimer timer) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Thread 객체 생성");
		}
		
		this.mTimer = timer;
		this.mTimerMode = mTimer.getType();
		
		setTimerToThread();
	}
	
	public void setTimerToThread() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Thread 표시 값 설정");
		}
		
		
		this.mHour = mTimer.getHour();
		this.mMin = mTimer.getMin();
		this.mSec = mTimer.getSec();
		this.mMilSec = mTimer.getMilsec();
		
		if(mTimerMode == STimer.TIMER_TYPE_SCHEDULE_TIMER) {
			mCurrentSchedule = ((StopTimer)mTimer).getScheduleTimerList();
		}
	}
	
	public void setThreadToTimer() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Thread 종료전 시간 상태 값 저장");
		}
		
		mTimer.setHour(mHour);
		mTimer.setMin(mMin);
		mTimer.setSec(mSec);
		mTimer.setMsec(mMilSec);
		
	}
	
	@Override
	public void run() {
		super.run();
		
		while(mThreadFlag) {
			countTimer();
			SystemClock.sleep(100);
		}
		
		setThreadToTimer();
		
	}

	
	public boolean getThreadFlag() {	return mThreadFlag;	}
	public void setThreadFlag(boolean flag) {	
		mThreadFlag = flag;	
	
		if(mThreadFlag) {
			mTimer.setState(STimer.TIMER_PLAYING);
		} else {
			mTimer.setState(STimer.TIMER_PAUSE);
		}
	}
	
	private void countTimer() {
		switch(mTimerMode) {
			case STimer.TIMER_TYPE_STOPWATCH:
				++mMilSec;
				
				if(mMilSec == 10) {
					mMilSec = 0;
					++mSec;
				}
				
				if(mSec == 60) {
					mSec = 0;
					++mMin;
				}
				
				if(mMin == 60) {
					mMin = 0;
					++mHour;
				}
				break;
				
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
			case STimer.TIMER_TYPE_GENERAL_TIMER:
				--mMilSec;
				
				if(mMilSec == -1) {
					mMilSec = 9;
					--mSec;
				}
				
				if(mSec == -1) {
					mSec = 59;
					--mMin;
				}
				
				if(mMin == -1) {
					mMin = 59;
					--mHour;
				}
				
				if(mTimerMode == STimer.TIMER_TYPE_SCHEDULE_TIMER) {
					runScheduleAlarm();
				}
				
				runTimerAlarm();
				
				break;
		}
		
		
		mTimer.setHour(mHour);
		mTimer.setMin(mMin);
		mTimer.setSec(mSec);
		mTimer.setMsec(mMilSec);
		
	}
	
	private void runTimerAlarm() {
		// 타이머 시간이 되었을 시, 타이머를 작동 중지 시키고 알람 서비스를 작동하는 부분 
		if(mHour <= 0 && mMin <= 0 && mSec <= 0 && mMilSec <= 0) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "타이머 시간이 다 되었습니다. 타이머를 정지합니다");
				Log.d(TAG, "현재 울리고 있는 알람 종류 : " + ((StopTimer)mTimer).getAlarmType());
			}
			
			STimerManage timerManage = STimerManage.getInstance();
//			timerManage.pauseTimerThread(((StopTimer)mTimer).getIndexKey());
			timerManage.removeTimerThread(((StopTimer)mTimer).getIndexKey());
			
//			setThreadFlag(false);
			((StopTimer)mTimer).setTimerSetFlag(false);
			
			// 알람 다이얼로그를 실행한다.
			ContextPool contextPool = ContextPool.getInstance();
			
			Intent alarmIntent = new Intent(contextPool.getContext(), SCompleteAlarmDialog.class);
			alarmIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			alarmIntent.putExtra(Constants.PUT_TIMER_COMPELETE_ALARM_NAME, ((StopTimer)mTimer).getName());
			alarmIntent.putExtra(Constants.PUT_TIMER_COMPELETE_ALARM_TYPE, ((StopTimer)mTimer).getAlarmType());
			contextPool.getContext().startActivity(alarmIntent);
		}
	}
	
	private void runScheduleAlarm() {
		// 스케쥴에 따른 알람 서비스 설정 부분
		if(mCurrentSchedule != null) {
			StopTimer currentTimer = ((StopTimer)mTimer);
			int currentIndex = currentTimer.getScheduleIndex();
			
			if(currentIndex < mCurrentSchedule.size()) {
				TimerScheduleList currentSchedule = mCurrentSchedule.get(currentIndex);
				
				if(mHour == currentSchedule.getHour() && mMin == currentSchedule.getMin() && mSec == currentSchedule.getSec()) {
					if(DEVELOPE_MODE) {
						Log.d(TAG, "스케쥴 시간이 되었습니다. 울리는 알람 종류 : " + currentSchedule.getAlarmType());
					}
					
					
					ScheduleAlarmService sAlarmService = ScheduleAlarmService.getInstance();
					sAlarmService.playAlarm(currentSchedule.getAlarmType());

					currentIndex++;
					currentTimer.setScheduleIndex(currentIndex);
				}
			}
		}
	}
}
