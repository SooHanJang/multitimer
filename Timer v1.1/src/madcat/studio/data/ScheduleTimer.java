package madcat.studio.data;

import android.os.Parcel;
import android.os.Parcelable;

public class ScheduleTimer implements Parcelable {
	
	public static final int SCHEDULE_OFF									=	0;
	public static final int SCHEDULE_ON										=	1;

	public static final int MODE_ALARM										=	0;
	public static  final int MODE_SHAKE										=	1;
	public static  final int MODE_MUTE										=	2;
	public static  final int MODE_ALARM_SHAKE								=	3;
	
	private String mTitle, mCategory;
	private int mScheduleFlag, mAlarmType;
	private int mHour, mMin, mSec;
	
	private byte[] mScheduleBytes;
	
	public ScheduleTimer() {}
	
	public ScheduleTimer(Parcel in) {
		readFromParcel(in);
	}
	
	public String getTitle() {	return mTitle;	}
	public void setTitle(String title) {	this.mTitle = title;	}
	
	public String getCategory() {	return mCategory;	}
	public void setCategory(String category) {	this.mCategory = category;	}
	
	public int getHour() {	return mHour;	}
	public void setHour(int hour) {	this.mHour = hour;	}
	
	public int getMin() {	return mMin;	}
	public void setMin(int min) {	this.mMin = min;	}
	
	public int getSec() {	return mSec;	}
	public void setSec(int sec) {	this.mSec = sec;	}
	
	public int getScheduleFlag() {	return mScheduleFlag;	}
	public void setScheduleFlag(int scheduleFlag) {	this.mScheduleFlag = scheduleFlag;	}
	
	public int getAlarmType() {	return mAlarmType;	}
	public void setAlarmType(int alarmType) {	this.mAlarmType = alarmType;	}
	
	public byte[] getScheduleBytes() {	return mScheduleBytes;	}
	public void setScheduleBytes(byte[] scheduleBytes) {	this.mScheduleBytes = scheduleBytes;	}
	
	public void readFromParcel(Parcel in) {
		mTitle = in.readString();
		mCategory = in.readString();
		mHour = in.readInt();
		mMin = in.readInt();
		mSec = in.readInt();
		mScheduleFlag = in.readInt();
		mAlarmType = in.readInt();
		
		mScheduleBytes = in.createByteArray();
	}
	
	public int describeContents() {
		return 0;
	}
	
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(mTitle);
		dest.writeString(mCategory);
		dest.writeInt(mHour);
		dest.writeInt(mMin);
		dest.writeInt(mSec);
		dest.writeInt(mScheduleFlag);
		dest.writeInt(mAlarmType);
		dest.writeByteArray(mScheduleBytes);
	}
	
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {

		public Object createFromParcel(Parcel in) {
			return new ScheduleTimer(in);
		}

		public Object[] newArray(int size) {
			// TODO Auto-generated method stub
			return new ScheduleTimer[size];
		}
		
	};
}
