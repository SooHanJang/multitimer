package madcat.studio.data;

import java.util.ArrayList;

import madcat.studio.constants.Constants;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class StopWatch extends STimer {
	
	private final String TAG										=	"StopWatch";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private ArrayList<StopWatchLap> mTimerLapArray;
	
	private int mHour, mMin, mSec;
	private long mMilSec;
	
	public StopWatch(String name, int type) {
		super(name, type);
		this.mTimerLapArray = new ArrayList<StopWatchLap>();
	}
	
	// 타이머 랩 관련 함수
	
	public void addTimerLap() {
		if(mTimerLapArray != null) {
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "타이머 랩 추가");
			}

			mHour = getHour();
			mMin = getMin();
			mSec = getSec();
			mMilSec = getMilsec();
			
			int timerLapIndex = mTimerLapArray.size();
			
			StopWatchLap timerLap = new StopWatchLap();
			timerLap.setIndex(timerLapIndex);
			timerLap.setLapHour(mHour);
			timerLap.setLapMin(mMin);
			timerLap.setLapSec(mSec);
			timerLap.setLapMilSec(mMilSec);

			if(mTimerLapArray.isEmpty()) {			//	비교할 이전의 값이 존재하지 않는 경우
				timerLap.setIvHour(mHour);
				timerLap.setIvMin(mMin);
				timerLap.setIvSec(mSec);
				timerLap.setIvMilSec(mMilSec);
			} else {								//	비교할 이전의 값이 존재하는 경우
				
				StopWatchLap lastLap = mTimerLapArray.get(0);			//	마지막 값을 불러온다.
				
				int ivHour = mHour - lastLap.getLapHour();
				int ivMin = mMin - lastLap.getLapMin();
				int ivSec = mSec - lastLap.getLapSec();
				long ivMilSec = mMilSec - lastLap.getLapMilSec();
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "m value : " + mHour + ", " + mMin + ", " + mSec + ", " + mMilSec);
					Log.d(TAG, "last value : " + lastLap.getLapHour() + ", " + lastLap.getLapMin() + ", " + lastLap.getLapSec() + ", " + lastLap.getLapMilSec());
					Log.d(TAG, "iv value : " + ivHour + ", " + ivMin + ", " + ivSec + ", " + ivMilSec);
				}
				
				if(ivMin < 0) {
					ivHour--;
					ivMin += 60;
				}
				
				if(ivSec < 0) {
					ivMin--;
					ivSec += 60;
				}
				
				if(ivMilSec < 0) {
					ivSec--;
					ivMilSec += 10;
				}
				
				timerLap.setIvHour(ivHour);
				timerLap.setIvMin(ivMin);
				timerLap.setIvSec(ivSec);
				timerLap.setIvMilSec(ivMilSec);
			}
			
			mTimerLapArray.add(0, timerLap);				//	설정된 STimerLap 객체를 ArrayList 에 추가 (역순으로 삽입)
		}
	}
	
	public void removeTimerAllLap() {
		if(mTimerLapArray != null) {
			mTimerLapArray.clear();
		}
	}
	
	public ArrayList<StopWatchLap> getTimerLapArray() {
		return mTimerLapArray;
	}
	
	public void initStopWatch() {
		this.setHour(0);
		this.setMin(0);
		this.setSec(0);
		this.setMsec(0);
	}

}
