package madcat.studio.data;

import java.io.Serializable;
import java.util.Comparator;

public class TimerScheduleList implements Serializable {
	
	public static final int MODE_ALARM										=	0;
	public static  final int MODE_SHAKE										=	1;
	public static  final int MODE_MUTE										=	2;
	public static  final int MODE_ALARM_SHAKE								=	3;
	
	private String mTime, mTitle;
	
	private int mAlarmType;
	private int mTotalSecTime;
	private int hour, min, sec;
	
	public int getHour() {	return hour;	}
	public void setHour(int hour) {	this.hour = hour;	}
	
	public int getMin() {	return min;	}
	public void setMin(int min) {	this.min = min;	}
	
	public int getSec() {	return sec;	}
	public void setSec(int sec) {	this.sec = sec;	}
	
	public String getTime() {	return mTime;	}
	public void setTime(String time) {	this.mTime = time;	}
	
	public String getTitle() {	return mTitle;	}
	public void setTitle(String title) {	this.mTitle = title;	}
	
	public int getAlarmType() {	return mAlarmType;	}
	public void setAlarmType(int type) {	this.mAlarmType = type;	}
	
	public int getTotalSecTime() {	return mTotalSecTime;	}
	public void setTotalSecTime(int sec) {	this.mTotalSecTime = sec;	}
	
}
