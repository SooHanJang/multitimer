package madcat.studio.data;

import madcat.studio.thread.STimerThread;

public class STotal {

	private STimer mTimer;
	private STimerThread mTimerThread;
	private int mKey;
	
	public STotal(int key, STimerThread timerThread, STimer timer) {
		this.mKey = key;
		this.mTimer = timer;
		this.mTimerThread = timerThread;
	}
	
	public int getKey()	{	return mKey;	}
	public void setKey(int key)	{	this.mKey = key;	}
	
	public STimer getTimer() {	return mTimer;	}
	public void setTimer(STimer timer) {	this.mTimer = timer;	}
	
	public STimerThread getTimerThread() {	return mTimerThread;	}
	public void setTimerThread(STimerThread asyncTimerThread) {	this.mTimerThread = asyncTimerThread;	}
	
}
