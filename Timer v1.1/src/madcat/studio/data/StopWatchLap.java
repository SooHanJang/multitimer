package madcat.studio.data;

public class StopWatchLap {
	
	private int mIndex;;
	
	private int mLapHour, mLapMin, mLapSec, mIvHour, mIvMin, mIvSec;
	private long mLapMilSec, mIvMilSec;

	public int getLapHour() {
		return mLapHour;
	}

	public void setLapHour(int lapHour) {
		this.mLapHour = lapHour;
	}

	public int getLapMin() {
		return mLapMin;
	}

	public void setLapMin(int lapMin) {
		this.mLapMin = lapMin;
	}

	public int getLapSec() {
		return mLapSec;
	}

	public void setLapSec(int lapSec) {
		this.mLapSec = lapSec;
	}

	public int getIvHour() {
		return mIvHour;
	}

	public void setIvHour(int ivHour) {
		this.mIvHour = ivHour;
	}

	public int getIvMin() {
		return mIvMin;
	}

	public void setIvMin(int ivMin) {
		this.mIvMin = ivMin;
	}

	public int getIvSec() {
		return mIvSec;
	}

	public void setIvSec(int ivSec) {
		this.mIvSec = ivSec;
	}

	public long getLapMilSec() {
		return mLapMilSec;
	}

	public void setLapMilSec(long lapMilSec) {
		this.mLapMilSec = lapMilSec;
	}

	public long getIvMilSec() {
		return mIvMilSec;
	}

	public void setIvMilSec(long ivMilSec) {
		this.mIvMilSec = ivMilSec;
	}

	public int getIndex() {
		return mIndex;
	}

	public void setIndex(int index) {
		this.mIndex = index;
	}

}
