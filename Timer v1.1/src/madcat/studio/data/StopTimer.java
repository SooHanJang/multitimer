package madcat.studio.data;

import java.util.ArrayList;

public class StopTimer extends STimer {
	
private boolean mTimerSetFlag											=	false;
	
	private int mTimerHour, mTimerMin, mTimerSec, mTimerMilSec;
	
	private int mAlarmType												=	0;
	private ArrayList<TimerScheduleList> mTimerScheduleArray;
	private int mScheduleIndex											=	0;
	
	private int mIndexKey;

	public StopTimer(String name, int type) {
		super(name, type);
	}
	
	// 타이머가 사용자에 의해 설정되었는지 여부 함수
	public boolean getTimerSetFlag() {	return mTimerSetFlag;	}
	public void setTimerSetFlag(boolean flag) {	this.mTimerSetFlag = flag;	}
	
	public int getIndexKey() {	return mIndexKey;	}
	public void setIndexKey(int indexKey) {	this.mIndexKey = indexKey;	}
	
	public int getTimerHour() {	return mTimerHour;	}
	public void setTimerHour(int timerHour) {	
		this.mTimerHour = timerHour;
		this.setHour(mTimerHour);
	}

	public int getTimerMin() {	return mTimerMin;	}
	public void setTimerMin(int timerMin) {	
		this.mTimerMin = timerMin;
		this.setMin(mTimerMin);
	}

	public int getTimerSec() {	return mTimerSec;	}
	public void setTimerSec(int timerSec) {	
		this.mTimerSec = timerSec;
		this.setSec(mTimerSec);
	}
	
	public int getTimerMilSec() {	return mTimerMilSec;	}
	public void setTimerMilSec(int timerMilSec) {
		this.mTimerMilSec = timerMilSec;
		this.setMsec(mTimerMilSec);
	}
	
	public int getAlarmType() {	return mAlarmType;	}
	public void setAlarmType(int alarmType) {	this.mAlarmType = alarmType;	}
	
	public ArrayList<TimerScheduleList> getScheduleTimerList() {	return mTimerScheduleArray;	}
	public void setScheduleTimerList(ArrayList<TimerScheduleList> timerScheduleArray) {	this.mTimerScheduleArray = timerScheduleArray;	}
	
	public int getScheduleIndex() {	return mScheduleIndex;	}
	public void setScheduleIndex(int scheduleIndex) {	this.mScheduleIndex = scheduleIndex;	}

}
