package madcat.studio.data;

import java.util.ArrayList;

import android.util.Log;

public abstract class STimer {
	
	private final String TAG												=	"STimer";
	private final boolean DEVELOPE_MODE										=	true;
	
	public final static int TIMER_TYPE_STOPWATCH							=	1;
	public final static int TIMER_TYPE_SCHEDULE_TIMER						=	2;
	public final static int TIMER_TYPE_GENERAL_TIMER						=	3;
	
	public final static int TIMER_INIT										=	0;
	public final static int TIMER_PLAYING									=	1;
	public final static int TIMER_PAUSE										=	2;
	
	private String mName;
	private int mHour, mMin, mSec;
	private long mMilSec;
	
	private int mType;
	private int mState														=	TIMER_INIT;
	
	public STimer(String name, int type) {
		this.mName = name;
		this.mType = type;
	}
	
	// 타이머와 스탑워치 관련 함수
	
	public String getName() {	return mName;	}
	public void setName(String name) {	this.mName = name;	}
	
	public int getType() {	return mType;	}
	public void setType(int type) {	this.mType = type;	}
	
	public int getHour() {	return mHour;	}
	public void setHour(int hour) {	this.mHour = hour;	}
	
	public int getMin() {	return mMin;	}
	public void setMin(int min) {	this.mMin = min;	}
	
	public int getSec() {	return mSec;	}
	public void setSec(int sec) {	this.mSec = sec;	}
	
	public long getMilsec() {	return mMilSec;	}
	public void setMsec(long mSec) {	this.mMilSec = mSec;	}
	
	public int getState() {	return mState;	}
	public void setState(int state) {	this.mState = state;	}
	
}
