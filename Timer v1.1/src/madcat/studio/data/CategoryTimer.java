package madcat.studio.data;

public class CategoryTimer {

	private String mName;
	private int mTimerSize;
	
	public String getName() {	return mName;	}
	public void setName(String name) {	this.mName = name;	}
	
	public int getTimerSize() {	return mTimerSize;	}
	public void setTimerSize(int timerSize) {	this.mTimerSize = timerSize;	}
}
