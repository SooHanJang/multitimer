package madcat.studio.timer;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterTimerList;
import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.STotal;
import madcat.studio.manage.timer.STimerManage;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class STimerList extends ListActivity implements OnClickListener {
	
	private final String TAG										=	"TimerList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private Context mContext;
	private STimerManage mManageTimer;
	private SharedPreferences mConfigPref;

	private ArrayList<STotal> mItems;
	private AdapterTimerList mAdapter;
	
	private Button mBtnAddTimer, mBtnTimerMenuSList, mBtnTimerMenuConfig;
	
	// 리스트 화면 갱신 쓰레드 관련 변수
	private Thread mTimerListUpdateThread;
	private boolean mTimerListRefreshFlag;
	
	// 화면 갱신 시 스크롤 위치를 저장해두기 위한 변수
	private int mSelectionIndex;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer_list);
		mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		mManageTimer = STimerManage.getInstance();
		
		// 화면 설정		
		switch(mConfigPref.getInt(Constants.PREF_DISPLAY_MODE, STimerConfig.DISPLAY_OFF)) {
			case STimerConfig.DISPLAY_ON:
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				break;
			case STimerConfig.DISPLAY_OFF:
				break;
		}
		
		mBtnAddTimer = (Button)findViewById(R.id.timer_list_btn_add_timer);
		mBtnTimerMenuSList = (Button)findViewById(R.id.timer_list_btn_menu_schedule_list);
		mBtnTimerMenuConfig = (Button)findViewById(R.id.timer_list_btn_menu_config);
		
		mBtnAddTimer.setOnClickListener(this);
		mBtnTimerMenuSList.setOnClickListener(this);
		mBtnTimerMenuConfig.setOnClickListener(this);

		// 어댑터 설정
		mItems = new ArrayList<STotal>();
		mItems.addAll(mManageTimer.getHashMap().values());
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 타이머 해시 맵 크기 : " + mItems.size());
		}
		
		mAdapter = new AdapterTimerList(mContext, R.layout.timer_list_row, mItems);
		setListAdapter(mAdapter);
		
		// 타이머 리스트 화면 업데이트 쓰레드 실행
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 리스트 갱신 쓰레드 상태 : " + mTimerListUpdateThread);
		}
		
		if(mTimerListUpdateThread == null) {
			mTimerListRefreshFlag = true;
			
			mTimerListUpdateThread = new Thread(new Runnable() {
		
				private void updateUI() {
					if(mTimerListUpdateThread.isInterrupted()) {
						return;
					}
					
					runOnUiThread(new Runnable() {
						public void run() {
							refreshList();
						}
					});
				}
				
				public void run() {
					while(mTimerListRefreshFlag) {
						updateUI();
						
						if(mManageTimer.isHashMapEmpty()) {
							if(DEVELOPE_MODE) {
								Log.d(TAG, "등록되어있는 타이머 값이 존재하지 않으므로, 초기 타이머 화면을 호출합니다.");
							}
								
							stopTimerListUpdate();
							
							Intent timerIntent = new Intent(STimerList.this, STimerStopWatch.class);
							timerIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_SINGLE_MODE);			//	싱글 모드 전달

							startActivity(timerIntent);
							overridePendingTransition(0, 0);
							finish();
								
							
						}
						
						SystemClock.sleep(500);
					}
				}
			});
		}
		
		mTimerListUpdateThread.start();
		
	}

	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}
		
		stopTimerListUpdate();
		finish();
		overridePendingTransition(0, 0);
		
		super.onPause();
	}
	
	private void refreshList() {
		int index = getListView().getFirstVisiblePosition();
		View v = getListView().getChildAt(0);
		int top = (v == null) ? 0 : v.getTop();

		mItems.clear();
		
		mItems.addAll(mManageTimer.getHashMap().values());
		mAdapter.setItems(mItems);
		mAdapter.notifyDataSetChanged();
		setListAdapter(mAdapter);
		
		this.getListView().setSelectionFromTop(index, top);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// 리스트 갱신 쓰레드 종료
		stopTimerListUpdate();
		
		Intent timerIntent = null;
		
		// 타이머의 타입에 따른 인텐트 설정
		if(mItems.get(position).getTimer().getType() == STimer.TIMER_TYPE_STOPWATCH) {
			timerIntent = new Intent(STimerList.this, STimerStopWatch.class);
		} else if(mItems.get(position).getTimer().getType() == STimer.TIMER_TYPE_SCHEDULE_TIMER ||
				mItems.get(position).getTimer().getType() == STimer.TIMER_TYPE_GENERAL_TIMER) {
			timerIntent = new Intent(STimerList.this, STimerStopTimer.class);
		}
		
		// 타이머 화면을 호출
		timerIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_LOAD_MODE);				//	타이머를 로드한다는 '로드 모드' 전달
		timerIntent.putExtra(Constants.PUT_TIMER_LOAD_KEY, mItems.get(position).getKey());		//	타이머를 실행할 '키' 값을 전달
		
		startActivity(timerIntent);
		overridePendingTransition(0, 0);
		
		finish();
		
		super.onListItemClick(l, v, position, id);
	}
	
	private void stopTimerListUpdate() {
		mTimerListRefreshFlag = false;
		mTimerListUpdateThread = null;
	}

	// 버튼 클릭 이벤트 처리 함수
	public void onClick(View v) {
		
		switch(v.getId()) {
			case R.id.timer_list_btn_add_timer:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머를 추가합니다");
				}
				
				stopTimerListUpdate();
				
				Intent timerViewIntent = new Intent(STimerList.this, STimerStopWatch.class);
				timerViewIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_SINGLE_MODE);
				
				startActivity(timerViewIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;

				
			case R.id.timer_list_btn_menu_schedule_list:
				Intent timerSListIntent = new Intent(STimerList.this, STimerCategoryList.class);
				startActivity(timerSListIntent);
				overridePendingTransition(0, 0);
				finish();
				break;
			case R.id.timer_list_btn_menu_config:
				Intent timerConfigIntent = new Intent(STimerList.this, STimerConfig.class);
				startActivity(timerConfigIntent);
				overridePendingTransition(0, 0);
				finish();
				break;
		}
	}
	
	// 뒤로가기 버튼을 누르면, 리스트 갱신 쓰레드를 중지하고 Activity를 종료한다.
	@Override
	public void onBackPressed() {
		stopTimerListUpdate();
		finish();
	}
}
