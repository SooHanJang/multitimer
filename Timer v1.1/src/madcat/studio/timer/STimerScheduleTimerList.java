package madcat.studio.timer;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterScheduleTimerDel;
import madcat.studio.adapter.AdapterScheduleTimerList;
import madcat.studio.constants.Constants;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.dialog.SCategoryDialog;
import madcat.studio.dialog.SConfirmDialog;
import madcat.studio.dialog.STimerScheduleDialog;
import madcat.studio.quick.action.ActionItem;
import madcat.studio.quick.action.QuickAction;
import madcat.studio.quick.action.QuickAction.OnActionItemClickListener;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class STimerScheduleTimerList extends ListActivity implements OnClickListener, OnActionItemClickListener {
	
	private final String TAG										=	"STimerScheduleTimerList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int MODE_IN_CATEGORY						=	1;
	public static final int MODE_IN_SEARCH							=	2;
	
	private final int QUICK_ADD_TIMER								=	1;
	private final int QUICK_REMOVE_TIMER							=	2;
	private final int QUICK_ADD_CATEGORY							=	3;
	
	private Context mContext;
	
	private LinearLayout mLinearDelArea;
	private EditText mEditSearch;
	private Button mBtnQuick, mBtnDelOk, mBtnDelCancel;
	private Button mBtnTimerMenuList, mBtnTimerMenuScheduleList, mBtnTimerMenuConfig, mBtnSearchTimer;
	
	private ArrayList<ScheduleTimer> mItems;
	private AdapterScheduleTimerList mListAdapter;
	private AdapterScheduleTimerDel mDelAdapter;
	
	private String mCategory, mSearchName;
	private int mMode												=	MODE_IN_CATEGORY;
	
	// Quick Action 관련 변수
	private QuickAction mQuickAction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer_category_list);
		this.mContext = this;
		this.getIntenter();
		
		// Resource Id 설정
		mLinearDelArea = (LinearLayout)findViewById(R.id.timer_schedule_list_del_layout);
		
		mEditSearch = (EditText)findViewById(R.id.timer_schedule_list_input_timer);
		
		mBtnQuick = (Button)findViewById(R.id.timer_schedule_list_quick_action);
		mBtnDelOk = (Button)findViewById(R.id.timer_schedule_list_btn_del_ok);
		mBtnDelCancel = (Button)findViewById(R.id.timer_schedule_list_btn_del_cancel);
		mBtnSearchTimer = (Button)findViewById(R.id.timer_schedule_list_btn_find);
		
		mBtnTimerMenuList = (Button)findViewById(R.id.timer_schedule_list_btn_menu_view);
		mBtnTimerMenuScheduleList = (Button)findViewById(R.id.timer_schedule_list_btn_menu_list);
		mBtnTimerMenuConfig = (Button)findViewById(R.id.timer_schedule_list_btn_menu_config);
		
		switch(mMode) {
			case MODE_IN_CATEGORY:
				if(mCategory.equals(Constants.DEFAULT_ALL_CATEGORY)) {
					mItems = Utils.getTimerAllList(mContext);
				} else {
					mItems = Utils.getTimerList(mContext, mCategory);
				}
				break;
			case MODE_IN_SEARCH:
				if(mItems == null && mSearchName != null) {
					mItems = Utils.getSearchTimerList(mContext, mSearchName);
				}
				
				break;
		}
		
		
		// Quick Action 설정
		mQuickAction = new QuickAction(mContext);
		ActionItem quickAddTimer = new ActionItem(QUICK_ADD_TIMER, 
				getString(R.string.quick_menu_add_timer), getResources().getDrawable(R.drawable.quickaction_icon_timer_add));
		ActionItem quickRemoveTimer = new ActionItem(QUICK_REMOVE_TIMER, 
				getString(R.string.quick_menu_remove_timer), getResources().getDrawable(R.drawable.quickaction_icon_timer_delete));
		ActionItem quickAddCategory = new ActionItem(QUICK_ADD_CATEGORY, 
				getString(R.string.quick_menu_add_category), getResources().getDrawable(R.drawable.quickaction_icon_category_add));
		
		mQuickAction.addActionItem(quickAddTimer);
		mQuickAction.addActionItem(quickRemoveTimer);
		mQuickAction.addActionItem(quickAddCategory);
		
		// Adapter 설정
		mListAdapter = new AdapterScheduleTimerList(mContext, R.layout.timer_schedule_timer_list_row, mItems, mMode);
		setListAdapter(mListAdapter);
		
		// Listener 설정
		mBtnQuick.setOnClickListener(this);
		mBtnDelOk.setOnClickListener(this);
		mBtnDelCancel.setOnClickListener(this);
		
		mBtnTimerMenuList.setOnClickListener(this);
		mBtnTimerMenuScheduleList.setOnClickListener(this);
		mBtnTimerMenuConfig.setOnClickListener(this);
		mBtnSearchTimer.setOnClickListener(this);
		
		mQuickAction.setOnActionItemClickListener(this);
	}
	
	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}
		
		overridePendingTransition(0, 0);
		finish();
		
		super.onPause();
	}
	
	private void refreshTimerListAdapter() {
		if(mListAdapter != null) {
			mListAdapter.clear();
		}
		
		mLinearDelArea.setVisibility(View.GONE);
		
		switch(mMode) {
			case MODE_IN_CATEGORY:
				if(mCategory.equals(Constants.DEFAULT_ALL_CATEGORY)) {
					mItems = Utils.getTimerAllList(mContext);
				} else {
					mItems = Utils.getTimerList(mContext, mCategory);
				}
				break;
			case MODE_IN_SEARCH:
				mItems = Utils.getSearchTimerList(mContext, mSearchName);
				
				break;
		}
		
		mListAdapter = new AdapterScheduleTimerList(mContext, R.layout.timer_schedule_timer_list_row, mItems, mMode);
		setListAdapter(mListAdapter);
	}
	
	private void refreshTimerDelAdapter() {
		if(mDelAdapter != null) {
			mDelAdapter.clear();
		}
		
		mLinearDelArea.setVisibility(View.VISIBLE);
		
		switch(mMode) {
			case MODE_IN_CATEGORY:
				if(mCategory.equals(Constants.DEFAULT_ALL_CATEGORY)) {
					mItems = Utils.getTimerAllList(mContext);
				} else {
					mItems = Utils.getTimerList(mContext, mCategory);
				}
				break;
			case MODE_IN_SEARCH:
				mItems = Utils.getSearchTimerList(mContext, mSearchName);
				break;
		}
		
		mDelAdapter = new AdapterScheduleTimerDel(mContext, R.layout.timer_schedule_timer_list_del_row, mItems, mMode);
		setListAdapter(mDelAdapter);
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Intent timerModifyIntent = new Intent(STimerScheduleTimerList.this, STimerScheduleDialog.class);
		timerModifyIntent.putExtra(Constants.PUT_TIMER_DIALOG_MODE, STimerScheduleDialog.MODIFY_TIMER);
		timerModifyIntent.putExtra(Constants.PUT_TIMER_DIALOG_MODIFY_NAME, mItems.get(position).getTitle());
		timerModifyIntent.putExtra(Constants.PUT_TIMER_LIST_MODE, mMode);
		timerModifyIntent.putExtra(Constants.PUT_TIMER_SEARCH_TIMER_NAME, mSearchName);
		
		
		startActivity(timerModifyIntent);
		overridePendingTransition(0, 0);
		finish();
		
		super.onListItemClick(l, v, position, id);
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.timer_schedule_list_btn_menu_view:
			Intent viewIntent = new Intent(STimerScheduleTimerList.this, STimerList.class);
			startActivity(viewIntent);
			overridePendingTransition(0, 0);
			finish();
			
			break;
		
		case R.id.timer_schedule_list_btn_menu_list:
			
			
			break;
			
		case R.id.timer_schedule_list_btn_menu_config:
			Intent configIntent = new Intent(STimerScheduleTimerList.this, STimerConfig.class);
			startActivity(configIntent);
			overridePendingTransition(0, 0);
			finish();
			
			break;
		
		
			
		
		
		case R.id.timer_schedule_list_quick_action:
			mQuickAction.show(v);
			break;
		
		case R.id.timer_schedule_list_btn_del_ok:
			// Database 를 돌면서 해당 타이머를 삭제한다.
			
			final ArrayList<ScheduleTimer> checkedItems = mDelAdapter.getCheckedList();
			
			// CheckList 를 읽어와서 database 에서 삭제한다.
			if(!checkedItems.isEmpty()) {
				final SConfirmDialog confirmDialog = new SConfirmDialog(mContext, getString(R.string.dialog_title_category_remove));
				confirmDialog.show();
				
				confirmDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(confirmDialog.getDialogStateFlag()) {
							SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
									null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

							int checkedCategorySize = checkedItems.size();
							for(int i=0; i < checkedCategorySize; i++) {
								// 타이머 테이블에서 타이머 삭제
								sdb.delete(Constants.TABLE_TIMER, 
										Constants.TIMER_NAME + " = '" + checkedItems.get(i).getTitle() + "'", null);
							}
							
							sdb.close();
							
							refreshTimerListAdapter();
							
							if(mItems.isEmpty()) {
								onBackPressed();
							}
						}
					}
				});
			} 
			
			
			break;
		case R.id.timer_schedule_list_btn_del_cancel:
			refreshTimerListAdapter();
			
			break;
			

		// 검색 버튼
		case R.id.timer_schedule_list_btn_find:
			String searchName = mEditSearch.getText().toString().trim();
			
			if(searchName.length() != 0) {
				if(searchName.length() >= 2) {
					mMode = MODE_IN_SEARCH;
					mSearchName = searchName;
					
					refreshTimerListAdapter();

					if(mItems.isEmpty()) {
						Toast.makeText(mContext, getString(R.string.toast_not_search_result), Toast.LENGTH_SHORT).show();
					}
					
				} else {
					Toast.makeText(mContext, getString(R.string.toast_need_two_search_character), Toast.LENGTH_SHORT).show();
				}
				
				mEditSearch.setText("");
			}
			
			break;
			
		}
	}
	
	public void onItemClick(QuickAction source, int pos, int actionId) {
		switch(actionId) {
			case QUICK_ADD_TIMER:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 추가 퀵 액션 클릭");
				}
				
				Intent addScheduleTimer = new Intent(STimerScheduleTimerList.this, STimerScheduleDialog.class);
				addScheduleTimer.putExtra(Constants.PUT_TIMER_DIALOG_MODE, STimerScheduleDialog.ADD_TIMER);
				startActivity(addScheduleTimer);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
			case QUICK_REMOVE_TIMER:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 삭제 퀵 액션 클릭");
				}
				
				refreshTimerDelAdapter();
				
				break;
			case QUICK_ADD_CATEGORY:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "카테고리 추가 퀵 액션 클릭");
				}
				
				final SCategoryDialog addCategoryDialog = new SCategoryDialog(mContext, SCategoryDialog.MODE_ADD_CATEGORY);
				addCategoryDialog.show();
				
				break;
		}
	}
	
	private void getIntenter() {
		Intent intent = this.getIntent();
		
		if(intent != null) {
			this.mMode = intent.getExtras().getInt(Constants.PUT_TIMER_LIST_MODE);
			
			switch(mMode) {
				case MODE_IN_CATEGORY:
					this.mCategory = intent.getExtras().getString(Constants.PUT_TIMER_CATEGORY);
					break;
				case MODE_IN_SEARCH:
					this.mSearchName = intent.getExtras().getString(Constants.PUT_TIMER_SEARCH_TIMER_NAME);
					this.mItems = intent.getParcelableArrayListExtra(Constants.PUT_TIMER_SEARCH_TIMER_ARRAY);

					if(DEVELOPE_MODE) {
						Log.d(TAG, "넘어온 타이머 이름 : " + mSearchName);
						Log.d(TAG, "넘어온 타이머 객체 : " + mItems);
					}
					
					break;
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		// 타이머 목록 호출
		Intent categoryListIntent = new Intent(STimerScheduleTimerList.this, STimerCategoryList.class);
		startActivity(categoryListIntent);
		overridePendingTransition(0, 0);
		
		finish();
		
	}
}
