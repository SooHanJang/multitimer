package madcat.studio.timer;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterTimerLap;
import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.StopWatch;
import madcat.studio.data.StopWatchLap;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.service.NotificationTimer;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class STimerStopWatch extends ListActivity implements OnClickListener {
	
	private final boolean DEVELOPE_MODE									=	true;
	private final String TAG											=	"TimerStopWatch";

	public static final int DEFAULT_LOAD_KEY							=	1;
	
	public static final int TIMER_SINGLE_MODE							=	0;
	public static final int TIMER_LOAD_MODE								=	1;
	
	private final int TIMER_STATE_INIT_MODE								=	100;
	private final int TIMER_STATE_PLAYING_MODE							=	200;
	private final int TIMER_STATE_PAUSE_MODE							=	300;
	
	private Context mContext;
	private STimerManage mManageTimer;
	private SharedPreferences mConfigPref;
	
	private TextView mTextHour, mTextMin, mTextSec, mTextMsec;
	private Button mBtnResetNLap, mBtnStartNPause;
	private Button mBtnSelectStopTimer;
	private Button mBtnTimerMenuList, mBtnTimerScheduleMenuList, mBtnTimerMenuConfig;

	// 타이머 제어 관련 변수
	private int mTimerMode, mTimerState, mTimerLoadKey;
	private boolean mTimerViewUpdateFlag;
	
	// 쓰레드 관련 변수
	private StopWatch mSWatch;
	private Thread mTimerViewUpdateThread;
	
	// 타이머 랩 어댑터 관련 변수
	private ArrayList<StopWatchLap> mTimerLapItems;
	private AdapterTimerLap mAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer_stop_watch);
		
		// 기본 전역 변수 초기화 및 설정
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.getIntenter();
		this.mManageTimer = STimerManage.getInstance();
		
		// 화면 설정		
		switch(mConfigPref.getInt(Constants.PREF_DISPLAY_MODE, STimerConfig.DISPLAY_OFF)) {
			case STimerConfig.DISPLAY_ON:
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				break;
			case STimerConfig.DISPLAY_OFF:
				break;
		}
		
		// Resource Id 설정
		mTextHour = (TextView)findViewById(R.id.timer_stopwatch_text_hour);
		mTextMin = (TextView)findViewById(R.id.timer_stopwatch_text_min);
		mTextSec = (TextView)findViewById(R.id.timer_stopwatch_text_sec);
		mTextMsec = (TextView)findViewById(R.id.timer_stopwatch_text_msec);
		
		mBtnResetNLap = (Button)findViewById(R.id.timer_stopwatch_btn_reset_n_lab);
		mBtnStartNPause = (Button)findViewById(R.id.timer_stopwatch_btn_start_n_pause);
		
		mBtnSelectStopTimer = (Button)findViewById(R.id.timer_stopwatch_btn_timer);

		mBtnTimerMenuList = (Button)findViewById(R.id.timer_stopwatch_btn_menu_list);
		mBtnTimerScheduleMenuList = (Button)findViewById(R.id.timer_stopwatch_btn_menu_schedule_list);
		mBtnTimerMenuConfig = (Button)findViewById(R.id.timer_stopwatch_btn_menu_config);
		
		// 화면 테마 설정
		int themeIndex = mConfigPref.getInt(Constants.PREF_CHOOSE_THEME_INDEX, 0);
		Utils.setDisplayTheme(themeIndex, mTextHour, mTextMin, mTextSec, mTextMsec);
		
		
		// Event Listener 설정
		
		mBtnResetNLap.setOnClickListener(this);
		mBtnStartNPause.setOnClickListener(this);
		
		mBtnSelectStopTimer.setOnClickListener(this);
		
		mBtnTimerMenuList.setOnClickListener(this);
		mBtnTimerScheduleMenuList.setOnClickListener(this);
		mBtnTimerMenuConfig.setOnClickListener(this);

		// 싱글 모드, 로드 모드에 따른 초기화 설정
		initTimerState();
	}
	
	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}
		
		stopTimerViewUpdateThread();
		finish();
		overridePendingTransition(0, 0);
		
		super.onPause();
	}
	
	// 타이머가 단일 타이머인지, 로드 된 타이먼지에 따라 설정하는 함수
	private void initTimerState() {
		switch(mTimerMode) {
			case TIMER_SINGLE_MODE:
				ctrlTimerState(TIMER_STATE_INIT_MODE);
				break;
			case TIMER_LOAD_MODE:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머를 로드합니다");
				}
				
				mSWatch = (StopWatch)mManageTimer.getItem(mTimerLoadKey).getTimer();
				
				mTimerLapItems = new ArrayList<StopWatchLap>();
				mTimerLapItems = mSWatch.getTimerLapArray();
				mAdapter = new AdapterTimerLap(mContext, R.layout.timer_view_lab_row, mTimerLapItems);
				setListAdapter(mAdapter);
				
				
				// 정지 되기 전의 타이머 상태 값을 로드
				changeTimerViewText(mSWatch.getHour(), mSWatch.getMin(), mSWatch.getSec(), mSWatch.getMilsec());
				
				switch(mSWatch.getState()) {
					case STimer.TIMER_INIT:							//	로드된 타이머가 생성 뒤, 한번도 시작이 되지 않았을 때
						mBtnSelectStopTimer.setVisibility(View.VISIBLE);
						
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);		//	타이머 모드를 '정지' 모드로 변경
						
						
						break;
				
					case STimer.TIMER_PLAYING:						//	타이머 쓰레드가 실행 중이라면, 타이머 뷰 화면을 갱신하는 AsyncTask 를 실행한다.

						if(DEVELOPE_MODE) {
							Log.d(TAG, "현재 쓰레드 객체 상태 : " + mTimerViewUpdateThread);
						}
						
						mBtnSelectStopTimer.setVisibility(View.INVISIBLE);
						
						// 쓰레드 객체 생성
						updateTimerView();
						
						
						// 타이머 모드를 '실행' 모드로 변경
						ctrlTimerState(TIMER_STATE_PLAYING_MODE);
						
						break;
					case STimer.TIMER_PAUSE:
						
						mBtnSelectStopTimer.setVisibility(View.INVISIBLE);
						
						// 타이머 모드를 '정지' 모드로 변경 
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);
						
						break;
				}
				
				break;
		}
	}
	
	private void updateTimerView() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "타이머 뷰 갱신 쓰레드 실행");
		}
		
		// 타이머 뷰 갱신 쓰레드 실행
		if(mTimerViewUpdateThread == null) {
		
			mTimerViewUpdateFlag = true;
			
			mTimerViewUpdateThread = new Thread(new Runnable() {
				
				private void updateUI() {
					if(mTimerViewUpdateThread.isInterrupted()) {
						return;
					}
					
					
					
					runOnUiThread(new Runnable() {
						public void run() {
							changeTimerViewText(mSWatch.getHour(), mSWatch.getMin(), mSWatch.getSec(), mSWatch.getMilsec());
							refreshLapList();
							
							
						}
					});
				}
				
				public void run() {
					while(mTimerViewUpdateFlag) {
						updateUI();
						SystemClock.sleep(80);
					}
				}
			});
			
			mTimerViewUpdateThread.start();
		}
	}
	
	private void refreshLapList() {
		mTimerLapItems = mSWatch.getTimerLapArray();
		mAdapter.setItems(mTimerLapItems);
		mAdapter.notifyDataSetChanged();
	}

	// 타이머의 작동 상태에 따른 컨트롤 함수
	private void ctrlTimerState(int state) {
		mTimerState = state;
		
		switch(state) {
			case TIMER_STATE_INIT_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_start));
				mBtnResetNLap.setText(getString(R.string.btn_text_timer_reset));
				
				break;
			case TIMER_STATE_PAUSE_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_start));
				mBtnResetNLap.setText(getString(R.string.btn_text_timer_reset));
				
				break;
			case TIMER_STATE_PLAYING_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_stop));
				mBtnResetNLap.setText(getString(R.string.btn_text_timer_lap));
				
				break;
		}
	}
	
	private void startTimerListActivity() {
		Intent timerListIntent = new Intent(STimerStopWatch.this, STimerList.class);
		startActivity(timerListIntent);
		overridePendingTransition(0, 0);
		finish();
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.timer_stopwatch_btn_start_n_pause:				//	시작 혹은 정지 버튼 클릭
				switch(mTimerState) {
				
					case TIMER_STATE_INIT_MODE:
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "INIT 상태에서 시작 버튼 클릭");
						}

						mManageTimer.addStopWatchThread(mContext, "");
						mManageTimer.startTimerThread(mManageTimer.getLastKey());
						
						startTimerListActivity();
						
						break;
						
					case TIMER_STATE_PLAYING_MODE:				//	정지 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "정지 버튼 클릭");
						}
						

						// 화면 갱신 쓰레드 종료
						stopTimerViewUpdateThread();
						
						// 타이머 정지
						mManageTimer.pauseTimerThread(mTimerLoadKey);
						
						// 정지 모드로 변환
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);
						
						break;
				
					case TIMER_STATE_PAUSE_MODE:				//	시작 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "시작 버튼 클릭");
							Log.d(TAG, "현재 타이머 갱신 쓰레드 상태 : " + mTimerViewUpdateThread);
						}
						

						if(mSWatch.getState() == STimer.TIMER_INIT) {				//	타이머를 추가한 뒤, 처음 실행하는 것이라면 Start 를 호출
							mManageTimer.startTimerThread(mTimerLoadKey);

							startTimerListActivity();
							
							
						} else {
							// 화면 갱신 쓰레드 재생성
							updateTimerView();
							
							mManageTimer.resumeTimerThread(mTimerLoadKey);			//	생성 된 타이머를 재실행 하는 것이라면 resume 을 호출
							
							
						}
						
						// 실행 모드로 변환
						ctrlTimerState(TIMER_STATE_PLAYING_MODE);
						
						break;
				}
				
				break;
				
			case R.id.timer_stopwatch_btn_reset_n_lab:				//	랩 혹은 재설정 버튼 클릭
				switch(mTimerState) {
					case TIMER_STATE_PLAYING_MODE:				//	랩 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "랩 버튼 클릭");
						}
						
						mSWatch.addTimerLap();					//	타이머 랩 추가
						
						break;
						
						
					case TIMER_STATE_PAUSE_MODE:				//	재설정 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "재설정 버튼 클릭");
						}
						
						// 타이머 랩 초기화
						mSWatch.removeTimerAllLap();			//	먼저 가지고 있던 타이머의 랩을 전부 삭제
						mTimerLapItems.clear();
						mAdapter.clear();
						
						// 타이머의 값을 재설정
						mSWatch.initStopWatch();
						
						// 쓰레드의 값을 재설정
						mManageTimer.getItem(mTimerLoadKey).getTimerThread().setTimerToThread();
						
						// 화면에 표시되는 값을 재설정
						changeTimerViewText(mSWatch.getHour(), mSWatch.getMin(), mSWatch.getSec(), mSWatch.getMilsec());
						
						break;
				}
				
				break;
				
			// 타이머 모드 선택 부분
			
			case R.id.timer_stopwatch_btn_timer:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 모드를 선택");
					Log.d(TAG, "타이머 모드에 보낼 모드 : " + mTimerMode);
				}
				
				
				Intent timerModeIntent = new Intent(STimerStopWatch.this, STimerStopTimer.class);
				timerModeIntent.putExtra(Constants.PUT_TIMER_MODE, mTimerMode);
				startActivity(timerModeIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
				
			
			// 메뉴 버튼 부분
			case R.id.timer_stopwatch_btn_menu_list:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 리스트 메뉴 버튼 클릭");
				}
				
				
				if(!mManageTimer.isHashMapEmpty()) {
					// 화면 갱신 쓰레드 종료
					stopTimerViewUpdateThread();
					
					Intent timerListIntent = new Intent(STimerStopWatch.this, STimerList.class);
					startActivity(timerListIntent);
					overridePendingTransition(0, 0);
					
					finish();
				}
				
				
				break;
			case R.id.timer_stopwatch_btn_menu_schedule_list:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 스케쥴 리스트 메뉴 버튼 클릭");
				}

				// 화면 갱신 쓰레드를 종료
				stopTimerViewUpdateThread();
				
				// 스케쥴 리스트 Activity 호출
				Intent scheduleIntent = new Intent(STimerStopWatch.this, STimerCategoryList.class);
				startActivity(scheduleIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				
				
				break;
			case R.id.timer_stopwatch_btn_menu_config:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 설정 메뉴 버튼 클릭");
				}
				
				stopTimerViewUpdateThread();
				
				// 타이머 설정 Activity 호출
				Intent configIntent = new Intent(STimerStopWatch.this, STimerConfig.class);
				startActivity(configIntent);
				overridePendingTransition(0, 0);
				
				finish();
				break;
		}
	}
	
	private void refreshList() {
		mAdapter.clear();

		mTimerLapItems = mSWatch.getTimerLapArray();
		mAdapter = new AdapterTimerLap(mContext, R.layout.timer_view_lab_row, mTimerLapItems);
		mAdapter.notifyDataSetChanged();
		
		setListAdapter(mAdapter);
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mTimerMode = intent.getExtras().getInt(Constants.PUT_TIMER_MODE);
			
			switch(mTimerMode) {
				case TIMER_SINGLE_MODE:
//					mTimerLoadKey = DEFAULT_LOAD_KEY;
					break;
				case TIMER_LOAD_MODE:
					mTimerLoadKey = intent.getExtras().getInt(Constants.PUT_TIMER_LOAD_KEY);
					break;
			}
		}
	}
	
	private void changeTimerViewText(int hour, int min, int sec, long milSec) {
		String strHour = null, strMin = null, strSec = null, strMilsec = null;
		
		strHour = String.format("%02d", hour);
		strMin = String.format("%02d", min);
		strSec = String.format("%02d", sec);
		strMilsec = String.valueOf(milSec);
		
		mTextHour.setText(strHour);
		mTextMin.setText(strMin);
		mTextSec.setText(strSec);
		mTextMsec.setText(strMilsec);
	}
	
	private void stopTimerViewUpdateThread() {
		// 화면 갱신 쓰레드 종료
		mTimerViewUpdateFlag = false;
		mTimerViewUpdateThread = null;
	}
	
	// 뒤로가기 버튼을 누르면, Timer 화면 갱신을 중지하고, Activity 를 종료한다.
	@Override
	public void onBackPressed() {
		// 화면 갱신 쓰레드 종료
		stopTimerViewUpdateThread();
		finish();
	}
}






