package madcat.studio.timer;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterCategoryDel;
import madcat.studio.adapter.AdapterCategoryList;
import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryTimer;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.dialog.SCategoryDialog;
import madcat.studio.dialog.SConfirmDialog;
import madcat.studio.dialog.STimerScheduleDialog;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.quick.action.ActionItem;
import madcat.studio.quick.action.QuickAction;
import madcat.studio.quick.action.QuickAction.OnActionItemClickListener;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

public class STimerCategoryList extends ListActivity implements OnClickListener, OnActionItemClickListener {
	
	private final String TAG										=	"STimerCategoryList";
	private boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	
	private final int QUICK_ADD_CATEGORY							=	1;
	private final int QUICK_REMOVE_CATEGORY							=	2;
	private final int QUICK_ADD_TIMER								=	3;
	
	private final int MODIFY_CATEGORY								=	1;
	private final int DELETE_CATEGORY								=	2;
	
	private Context mContext;
	private STimerManage mManageTimer;

	private EditText mEditSearch;
	private Button mBtnTimerMenuView, mBtnTimerMenuConfig, mBtnSearchTimer, mBtnQuickAction;
	
	private LinearLayout mLinearDelArea;
	private Button mBtnDelOk, mBtnDelCancel;

	// Spinner 관련 변수
	private String[] mCategoryList;
	
	// Category 관련 변수
	private ArrayList<CategoryTimer> mCategoryItems;
	private AdapterCategoryList mCategoryListAdapter;
	private AdapterCategoryDel mCategoryDelAdapter;
	
	// Quick Action 관련 변수
	private QuickAction mQuickAction;
	
	// LongClick 처리 관련 변수
	private boolean mLongClickFlag;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer_category_list);
		this.mContext = this;
		this.mManageTimer = STimerManage.getInstance();

		// Resource Id 설정
		mEditSearch = (EditText)findViewById(R.id.timer_schedule_list_input_timer);
		
		mBtnTimerMenuView = (Button)findViewById(R.id.timer_schedule_list_btn_menu_view);
		mBtnTimerMenuConfig = (Button)findViewById(R.id.timer_schedule_list_btn_menu_config);
		mBtnSearchTimer = (Button)findViewById(R.id.timer_schedule_list_btn_find);
		
		mBtnQuickAction = (Button)findViewById(R.id.timer_schedule_list_quick_action);
		
		mLinearDelArea = (LinearLayout)findViewById(R.id.timer_schedule_list_del_layout);
		mBtnDelOk = (Button)findViewById(R.id.timer_schedule_list_btn_del_ok);
		mBtnDelCancel= (Button)findViewById(R.id.timer_schedule_list_btn_del_cancel);
		
		// Quick Action 설정
		mQuickAction = new QuickAction(mContext);
		ActionItem quickAddCategory = new ActionItem(QUICK_ADD_CATEGORY, 
				getString(R.string.quick_menu_add_category), getResources().getDrawable(R.drawable.quickaction_icon_category_add));
		ActionItem quickRemoveCategory = new ActionItem(QUICK_REMOVE_CATEGORY, 
				getString(R.string.quick_menu_remove_category), getResources().getDrawable(R.drawable.quickaction_icon_category_delete));
		ActionItem quickAddTimer = new ActionItem(QUICK_ADD_TIMER, 
				getString(R.string.quick_menu_add_timer), getResources().getDrawable(R.drawable.quickaction_icon_timer_add));
		
		mQuickAction.addActionItem(quickAddCategory);
		mQuickAction.addActionItem(quickRemoveCategory);
		mQuickAction.addActionItem(quickAddTimer);
		
		// 카테고리 List Adapter 설정
		refreshCategoryListAdapter();
		
		// Event Listener 설정
		mBtnTimerMenuView.setOnClickListener(this);
		mBtnTimerMenuConfig.setOnClickListener(this);
		mBtnSearchTimer.setOnClickListener(this);
		mBtnQuickAction.setOnClickListener(this);
		
		mQuickAction.setOnActionItemClickListener(this);
		
		mBtnDelOk.setOnClickListener(this);
		mBtnDelCancel.setOnClickListener(this);
		
		registerForContextMenu(getListView());
		
	}

	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}
		
		finish();
		overridePendingTransition(0, 0);
		
		super.onPause();
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);

		int index = ((AdapterView.AdapterContextMenuInfo)menuInfo).position;
		
		if(index != 0) {
			menu.setHeaderTitle(getString(R.string.dialog_title_category_manage));
			menu.add(0, MODIFY_CATEGORY, Menu.NONE, getString(R.string.menu_title_category_modify));
			menu.add(0, DELETE_CATEGORY, Menu.NONE, getString(R.string.menu_title_category_delete));
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onContextItemSelected 호출");
		}
		
		AdapterView.AdapterContextMenuInfo menuInfo;
		final int index;
		
		switch(item.getItemId()) {
			case MODIFY_CATEGORY:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "카테고리 수정 클릭");
				}
				
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				
				final SCategoryDialog categoryModifyDialog = new SCategoryDialog(mContext, 
						SCategoryDialog.MODE_MODIFY_CATEGORY, mCategoryItems.get(index).getName());
				
				categoryModifyDialog.show();
				
				categoryModifyDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(categoryModifyDialog.getDialogFlag()) {
							refreshCategoryListAdapter();
						}
					}
				});

				return true;
			case DELETE_CATEGORY:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "카테고리 삭제 클릭");
				}
				
				menuInfo = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
				index = menuInfo.position;
				
				final SConfirmDialog confirmDialog = new SConfirmDialog(mContext, getString(R.string.dialog_title_category_remove_text));
				confirmDialog.show();
				
				confirmDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(confirmDialog.getDialogStateFlag()) {
							SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
									null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

							// 카테고리가 삭제되면, 기존에 포함되 있던 타이머의 카테고리를 All 로 이동한다.
							ContentValues updateValues = new ContentValues();
							updateValues.put(Constants.TIMER_CATEGORY, Constants.DEFAULT_ALL_CATEGORY);

							// 삭제할 카테고리의 타이머를 All 로 업데이트
							sdb.update(Constants.TABLE_TIMER, updateValues, 
									Constants.TIMER_CATEGORY + " = '" + mCategoryItems.get(index).getName() + "'", null);
							
							// 카테고리 테이블에서 카테고리 삭제
							sdb.delete(Constants.TABLE_CATEGORY, 
									Constants.CATEGORY_NAME + " = '" + mCategoryItems.get(index).getName() + "'", null);
							
							sdb.close();
							
							refreshCategoryListAdapter();
						}
					}
				});

				return true;
			default:
				return false;
		}
	}
	
	private void refreshCategoryListAdapter() {
		if(mCategoryListAdapter != null) {
			mCategoryListAdapter.clear();
		}
		
		mLinearDelArea.setVisibility(View.GONE);
		
		mCategoryItems = Utils.getCategoryList(mContext);
		mCategoryListAdapter = new AdapterCategoryList(mContext, R.layout.timer_category_list_row, mCategoryItems);
		setListAdapter(mCategoryListAdapter);
	}
	
	private void refreshCategoryDelAdapter() {
		if(mCategoryDelAdapter != null) {
			mCategoryDelAdapter.clear();
		}
		
		mLinearDelArea.setVisibility(View.VISIBLE);
		
		mCategoryItems = Utils.getCategoryList(mContext);
		mCategoryDelAdapter = new AdapterCategoryDel(mContext, R.layout.timer_category_list_del_row, mCategoryItems);
		setListAdapter(mCategoryDelAdapter);
	}

	// 버튼 클릭 이벤트 처리 함수
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.timer_schedule_list_btn_menu_view:
				Intent timerIntent = null;
				
				if(mManageTimer.isHashMapEmpty()) {
					timerIntent = new Intent(STimerCategoryList.this, STimerStopWatch.class);
					timerIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_SINGLE_MODE);			//	싱글 모드 전달
				} else {
					timerIntent = new Intent(STimerCategoryList.this, STimerList.class);
				}
				
				startActivity(timerIntent);
				overridePendingTransition(0, 0);
				finish();
				
				break;
			case R.id.timer_schedule_list_btn_menu_config:
				Intent timerConfigIntent = new Intent(STimerCategoryList.this, STimerConfig.class);
				startActivity(timerConfigIntent);
				overridePendingTransition(0, 0);
				finish();
				break;
			
			case R.id.timer_schedule_list_quick_action:
				mQuickAction.show(v);
				break;
				
			case R.id.timer_schedule_list_btn_del_ok:			//	카테고리 삭제 확인 버튼 클릭
				
				final ArrayList<CategoryTimer> checkedItems = mCategoryDelAdapter.getCheckedList();
				
				// CheckList 를 읽어와서 database 에서 삭제한다.
				if(!checkedItems.isEmpty()) {
					final SConfirmDialog confirmDialog = new SConfirmDialog(mContext, getString(R.string.dialog_title_category_remove_text));
					confirmDialog.show();
					
					confirmDialog.setOnDismissListener(new OnDismissListener() {
						public void onDismiss(DialogInterface dialog) {
							if(confirmDialog.getDialogStateFlag()) {
								SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
										null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

								// 카테고리가 삭제되면, 기존에 포함되 있던 타이머의 카테고리를 All 로 이동한다.
								ContentValues updateValues = new ContentValues();
								updateValues.put(Constants.TIMER_CATEGORY, Constants.DEFAULT_ALL_CATEGORY);

								int checkedCategorySize = checkedItems.size();
								
								if(DEVELOPE_MODE) {
									Log.d(TAG, "체크 된 카테고리 개수 : " + checkedCategorySize);
									
									for(int i=0; i < checkedCategorySize; i++) {
										Log.d(TAG, "체크 한 카테고리 명 : " + checkedItems.get(i).getName());
									}
								}
								
								for(int i=0; i < checkedCategorySize; i++) {
									
									// 삭제할 카테고리의 타이머를 All 로 업데이트
									sdb.update(Constants.TABLE_TIMER, updateValues, 
											Constants.TIMER_CATEGORY + " = '" + checkedItems.get(i).getName() + "'", null);
									
									// 카테고리 테이블에서 카테고리 삭제
									sdb.delete(Constants.TABLE_CATEGORY, 
											Constants.CATEGORY_NAME + " = '" + checkedItems.get(i).getName() + "'", null);
								}
								
								sdb.close();
								
								refreshCategoryListAdapter();
							}
						}
					});
				} 
				
				break;
				
			case R.id.timer_schedule_list_btn_del_cancel:		//	카테고리 삭제 취소 버튼 클릭
				refreshCategoryListAdapter();
				break;
				
				
			// 검색 버튼
			case R.id.timer_schedule_list_btn_find:
				String searchName = mEditSearch.getText().toString().trim();
				
				if(searchName.length() != 0) {
					if(searchName.length() >= 2) {
						ArrayList<ScheduleTimer> items = Utils.getSearchTimerList(mContext, searchName);
	
						if(!items.isEmpty()) {
							Intent searchTimerIntent = new Intent(STimerCategoryList.this, STimerScheduleTimerList.class);
							searchTimerIntent.putExtra(Constants.PUT_TIMER_LIST_MODE, STimerScheduleTimerList.MODE_IN_SEARCH);
							searchTimerIntent.putExtra(Constants.PUT_TIMER_SEARCH_TIMER_NAME, searchName);
							searchTimerIntent.putParcelableArrayListExtra(Constants.PUT_TIMER_SEARCH_TIMER_ARRAY, items);
							
							startActivity(searchTimerIntent);
							overridePendingTransition(0, 0);
						} else {
							Toast.makeText(mContext, getString(R.string.toast_not_search_result), Toast.LENGTH_SHORT).show();
						}
					} else {
						Toast.makeText(mContext, getString(R.string.toast_need_two_search_character), Toast.LENGTH_SHORT).show();
					}
					
					mEditSearch.setText("");
				}
				
				break;
		}
	}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, position + "번 째 클릭");
		}
		
		if(mCategoryItems.get(position).getTimerSize() != 0) {
			Intent sTimerListIntent = new Intent(STimerCategoryList.this, STimerScheduleTimerList.class);
			sTimerListIntent.putExtra(Constants.PUT_TIMER_LIST_MODE, STimerScheduleTimerList.MODE_IN_CATEGORY);
			sTimerListIntent.putExtra(Constants.PUT_TIMER_CATEGORY, mCategoryItems.get(position).getName());
			startActivity(sTimerListIntent);
		}
		
		super.onListItemClick(l, v, position, id);
	}

	// 퀵 액션 클릭 이벤트 처리 함수
	public void onItemClick(QuickAction source, int pos, int actionId) {
		switch(actionId) {
			case QUICK_ADD_CATEGORY:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "카테고리 추가 퀵 액션 클릭");
				}
				
				final SCategoryDialog addCategoryDialog = new SCategoryDialog(mContext, SCategoryDialog.MODE_ADD_CATEGORY);
				addCategoryDialog.show();
				
				addCategoryDialog.setOnDismissListener(new OnDismissListener() { 
					public void onDismiss(DialogInterface dialog) {
						if(addCategoryDialog.getDialogFlag()) {
							refreshCategoryListAdapter();
						}
					}
				});
				
				
				
				break;
				
			case QUICK_REMOVE_CATEGORY:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "카테고리 삭제 퀵 액션 클릭");
				}
				
				refreshCategoryDelAdapter();
				
				break;
			case QUICK_ADD_TIMER:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 추가 퀵 액션 클릭");
				}
				
				Intent addScheduleTimer = new Intent(STimerCategoryList.this, STimerScheduleDialog.class);
				addScheduleTimer.putExtra(Constants.PUT_TIMER_DIALOG_MODE, STimerScheduleDialog.ADD_TIMER);
				startActivity(addScheduleTimer);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
		}
	}
}
