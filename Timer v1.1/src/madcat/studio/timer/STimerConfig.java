package madcat.studio.timer;

import java.io.IOException;

import madcat.studio.constants.Constants;
import madcat.studio.dialog.SChooseThemeDialog;
import madcat.studio.manage.timer.STimerManage;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class STimerConfig extends PreferenceActivity implements OnClickListener, OnPreferenceClickListener {
	
	private final String TAG										=	"TimerConfig";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int DISPLAY_ON								=	0;
	public static final int DISPLAY_OFF								=	1;

	private Context mContext;
	private SharedPreferences mConfigPref;
	private STimerManage mManageTimer;
	
	// 메뉴 관련 변수
	private Button mBtnTimerMenuView, mBtnTimerMenuList;
	
	// 환경설정 관련 변수
	private Preference mPrefManageTheme, mPrefManageAlarmTone, mPrefManageScheduleAlarmTone, mPrefManageDisplay;
	
	// 알람 설정 관련 변수
	private MediaPlayer mMediaPlayer;
	private String mSelectAlarmTonePath;
	private int mSelectAlarmToneIndex;
	
	// 스케쥴 알람 설정 관련 변수
	private MediaPlayer mMediaSchedulePlayer;
	private int mSelectScheduleAlarmIndex, mTempScheduleAlarmIndex;
	
	// 화면 설정 관련 변수
	private int mSelectDisplayIndex, mSelectDisplayTempIndex;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preference);
		setContentView(R.layout.timer_config);
		this.mContext = this;
		this.mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.mManageTimer = STimerManage.getInstance();
		
		// Resource Id 설정
		mBtnTimerMenuView = (Button)findViewById(R.id.timer_config_btn_menu_view);
		mBtnTimerMenuList = (Button)findViewById(R.id.timer_config_btn_menu_list);
		
		// Preference Id 설정
		mPrefManageTheme = (Preference)findPreference("pref_manage_theme");
		mPrefManageAlarmTone = (Preference)findPreference("pref_manage_alarm_tone");
		mPrefManageScheduleAlarmTone = (Preference)findPreference("pref_manage_schedule_alarm_tone");
		mPrefManageDisplay = (Preference)findPreference("pref_manage_display");
		
		// Event Listener 설정
		mBtnTimerMenuView.setOnClickListener(this);
		mBtnTimerMenuList.setOnClickListener(this);
		
		mPrefManageTheme.setOnPreferenceClickListener(this);
		mPrefManageAlarmTone.setOnPreferenceClickListener(this);
		mPrefManageScheduleAlarmTone.setOnPreferenceClickListener(this);
		mPrefManageDisplay.setOnPreferenceClickListener(this);
		
		// 설정 값 불러오기
		
		// 스케쥴 알람 설정 값 불러오기
		mSelectScheduleAlarmIndex = mConfigPref.getInt(Constants.PREf_SCHEDULE_ALARM_INDEX, 0);
		
		// 화면 설정 값 불러오기
		mSelectDisplayIndex = mConfigPref.getInt(Constants.PREF_DISPLAY_MODE, 1);
		
		setDisplayMode(mSelectDisplayIndex);
		
	}
	
	@Override
	protected void onPause() {
		finish();
		overridePendingTransition(0, 0);
		
		super.onPause();
	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.timer_config_btn_menu_view:
			Intent timerIntent = null;
			
			if(mManageTimer.isHashMapEmpty()) {
				timerIntent = new Intent(STimerConfig.this, STimerStopWatch.class);
				timerIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_SINGLE_MODE);
			} else {
				timerIntent = new Intent(STimerConfig.this, STimerList.class);
			}
			
			startActivity(timerIntent);
			overridePendingTransition(0, 0);
			finish();
			break;
		case R.id.timer_config_btn_menu_list:
			Intent timerListIntent = new Intent(STimerConfig.this, STimerCategoryList.class);
			startActivity(timerListIntent);
			overridePendingTransition(0, 0);
			finish();
			break;
		}
	}

	public boolean onPreferenceClick(Preference preference) {
		if(preference.getKey().equals("pref_manage_theme")) {
			SChooseThemeDialog chooseThemeDialog = new SChooseThemeDialog(mContext);
			chooseThemeDialog.show();
			
			
			
			
		} else if(preference.getKey().equals("pref_manage_alarm_tone")) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "알림음 설정 다이얼로그 실행");
			}
			
			RingtoneManager ringtoneManager = new RingtoneManager(mContext);
			ringtoneManager.setType(RingtoneManager.TYPE_RINGTONE);
			Cursor ringCursor = ringtoneManager.getCursor();
			
			String defaultRingUri = mConfigPref.getString(Constants.PREF_ALARM_INDEX_PATH, 
					ringtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE).toString());
			
			
			int countIndex = 0;
			
			final String[] ringItems = new String[ringCursor.getCount()];				//	다이얼로그에 표시해 줄 알람 제목
			final String[] ringUri = new String[ringCursor.getCount()];
			
			while(ringCursor.moveToNext()) {
				ringItems[countIndex] = ringCursor.getString(1);
				ringUri[countIndex] = ringCursor.getString(2) + "/" + ringCursor.getInt(0);
				
				if(defaultRingUri.equals(ringUri[countIndex])) {
					mSelectAlarmToneIndex = countIndex;
				}
				
				countIndex++;
			}
			

			// 다이얼로그 설정 부분
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(getString(R.string.dialog_title_select_alarm_tone));
			builder.setSingleChoiceItems(ringItems, mSelectAlarmToneIndex, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					startMediaPlayer(ringUri[which]);
					mSelectAlarmTonePath = ringUri[which];
				}
			});
			builder.setPositiveButton(mContext.getString(R.string.btn_text_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					initMediaPlayer();
					
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putString(Constants.PREF_ALARM_INDEX_PATH, mSelectAlarmTonePath);
					editor.commit();
				}
			});
			
			builder.setNegativeButton(mContext.getString(R.string.btn_text_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					initMediaPlayer();
					
					dialog.dismiss();
				}
			});
			
			builder.show();
			startMediaPlayer(defaultRingUri);
		} else if(preference.getKey().equals("pref_manage_schedule_alarm_tone")) {
			String[] items = {mContext.getString(R.string.alarm_tone_theme_drrrrr), mContext.getString(R.string.alarm_tone_theme_watch),
					mContext.getString(R.string.alarm_tone_theme_clocks), mContext.getString(R.string.alarm_tone_theme_train_light),
					mContext.getString(R.string.alarm_tone_theme_old_telephone)};
			
			mTempScheduleAlarmIndex = mSelectScheduleAlarmIndex;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			
			builder.setTitle(getString(R.string.dialog_title_select_schedule_alarm_tone));
			builder.setSingleChoiceItems(items, mTempScheduleAlarmIndex, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					try {
						startMediaPlayer(getAssets().openFd("alarm/schedule_alarm_" + which + ".mp3"));
						mTempScheduleAlarmIndex = which;
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
			
			builder.setPositiveButton(getString(R.string.btn_text_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					initMediaPlayer();

					mSelectScheduleAlarmIndex = mTempScheduleAlarmIndex;
					
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putInt(Constants.PREf_SCHEDULE_ALARM_INDEX, mSelectScheduleAlarmIndex);
					editor.commit();
				}
			});
			
			builder.setNegativeButton(getString(R.string.btn_text_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					initMediaPlayer();
					
					dialog.dismiss();
				}
			});
			
			builder.show();
			
			try {
				startMediaPlayer(getAssets().openFd("alarm/schedule_alarm_" + mTempScheduleAlarmIndex + ".mp3"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		} else if(preference.getKey().equals("pref_manage_display")) {
			
			// 다이얼로그 설정 부분
			String[] items = {mContext.getString(R.string.pref_manage_display_on), mContext.getString(R.string.pref_manage_display_off)};
			mSelectDisplayTempIndex = mSelectDisplayIndex;
			
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(mContext.getString(R.string.dialog_title_manage_display));
			builder.setSingleChoiceItems(items, mSelectDisplayIndex, new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					mSelectDisplayTempIndex = which;
				}
			});
			builder.setPositiveButton(mContext.getString(R.string.btn_text_ok), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					SharedPreferences.Editor editor = mConfigPref.edit();
					editor.putInt(Constants.PREF_DISPLAY_MODE, mSelectDisplayTempIndex);
					editor.commit();
					
					mSelectDisplayIndex = mSelectDisplayTempIndex;
					
					setDisplayMode(mSelectDisplayIndex);
					
					dialog.dismiss();
				}
			});
			builder.setNegativeButton(mContext.getString(R.string.btn_text_cancel), new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			});
			
			builder.show();
			
		}
		
		return false;
	}
	
	private void initMediaPlayer() {
		if(mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
				mMediaPlayer.release();
				mMediaPlayer = null;
			}
		}
		
		if(mMediaSchedulePlayer != null) {
			if(mMediaSchedulePlayer.isPlaying()) {
				mMediaSchedulePlayer.stop();
				mMediaSchedulePlayer.release();
				mMediaSchedulePlayer = null;
			}
		}
	}
	
	private void startMediaPlayer(String path) {
		initMediaPlayer();
		
		if(mMediaPlayer == null) {
			mMediaPlayer = new MediaPlayer();
			mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
			
			try {
				mMediaPlayer.setDataSource(path);
				mMediaPlayer.setLooping(true);
				mMediaPlayer.prepare();
				mMediaPlayer.start();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void startMediaPlayer(AssetFileDescriptor afd) {
		initMediaPlayer();
		
		if(mMediaSchedulePlayer == null) {
			mMediaSchedulePlayer = new MediaPlayer();
			mMediaSchedulePlayer.setAudioStreamType(AudioManager.STREAM_RING);
			
			try {
				mMediaSchedulePlayer.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
				mMediaSchedulePlayer.setLooping(true);
				mMediaSchedulePlayer.prepare();
				mMediaSchedulePlayer.start();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void setDisplayMode(int mode) {
		switch(mode) {
			case DISPLAY_ON:
				mPrefManageDisplay.setTitle(mContext.getString(R.string.pref_manage_display_on));
				break;
			case DISPLAY_OFF:
				mPrefManageDisplay.setTitle(mContext.getString(R.string.pref_manage_display_off));
				break;
		}
	}
}
