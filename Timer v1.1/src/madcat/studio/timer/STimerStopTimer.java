package madcat.studio.timer;

import java.util.ArrayList;

import madcat.studio.adapter.AdapterStopTimerScheduleView;
import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.data.StopTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.dialog.STimePickDialog;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class STimerStopTimer extends ListActivity implements OnClickListener, OnItemSelectedListener {

	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	private final String TAG											=	"TimerStopTimer";
	
	private final int DEFAULT_LOAD_KEY									=	1;
	
	public static final int TIMER_SINGLE_MODE							=	0;
	public static final int TIMER_LOAD_MODE								=	1;
	
	private final int TIMER_STATE_INIT_MODE								=	100;
	private final int TIMER_STATE_PLAYING_MODE							=	200;
	private final int TIMER_STATE_PAUSE_MODE							=	300;
	
	private Context mContext;
	private STimerManage mManageTimer;
	private SharedPreferences mConfigPref;
	
	private TextView mTextHour, mTextMin, mTextSec, mTextMilSec;
	private Button mBtnSetNReset, mBtnStartNPause;
	private Button mBtnSelectStopWatch;
	private Button mBtnTimerMenuList, mBtnTimerMenuScheduleList, mBtnTimerMenuConfig;
	
	private LinearLayout mLinearAlarmLayout;
	private Spinner mSpinAlarmType;
	
	// 알람 타입 관련 변수
	private int mAlarmTypeIndex											=	ScheduleTimer.MODE_ALARM;
	
	// 스케쥴 어댑터 관련 변수
	private AdapterStopTimerScheduleView mScheduleAdapter;
	private ArrayList<TimerScheduleList> mArrayTimerScheduleList;
	private int mCurrentIndex;
	private boolean mDynamicCurrentFlag;
	
	// 타이머 타입 관련 변수 (일반 타이머, 스케쥴 타이머)
	private int mTimerType												=	STimer.TIMER_TYPE_GENERAL_TIMER;
	
	// 타이머 시간 설정 관련 변수
	private int mSetHour, mSetMin, mSetSec, mSetMilSec;
	
	// 타이머 제어 관련 변수
	private int mTimerMode, mTimerState, mTimerLoadKey;
	private boolean mTimerViewUpdateFlag;
	private boolean mTimerSetFlag										=	false;
	
	// 쓰레드 관련 변수
	private StopTimer mSTimer;
	private Thread mTimerViewUpdateThread;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.timer_stop_timer);
		
		// 기본 설정
		this.mContext = this;
		this.mConfigPref = getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
		this.getIntenter();
		this.mManageTimer = STimerManage.getInstance();
		
		// 화면 설정
		switch(mConfigPref.getInt(Constants.PREF_DISPLAY_MODE, STimerConfig.DISPLAY_OFF)) {
			case STimerConfig.DISPLAY_ON:
				getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
				break;
			case STimerConfig.DISPLAY_OFF:
				break;
		}
		
		// Resource 및 Event 설정
		mLinearAlarmLayout = (LinearLayout)findViewById(R.id.timer_stoptimer_alarm_type_layout);
		mSpinAlarmType = (Spinner)findViewById(R.id.timer_stoptimer_alarm_spinner);
		
		mTextHour = (TextView)findViewById(R.id.timer_stoptimer_text_hour);
		mTextMin = (TextView)findViewById(R.id.timer_stoptimer_text_min);
		mTextSec = (TextView)findViewById(R.id.timer_stoptimer_text_sec);
		mTextMilSec = (TextView)findViewById(R.id.timer_stoptimer_text_msec);
		
		mBtnSelectStopWatch = (Button)findViewById(R.id.timer_stoptimer_btn_stopwatch);
		
		mBtnSetNReset = (Button)findViewById(R.id.timer_stoptimer_btn_set_n_reset);
		mBtnStartNPause = (Button)findViewById(R.id.timer_stoptimer_btn_start_n_pause);
		
		mBtnTimerMenuList = (Button)findViewById(R.id.timer_stoptimer_btn_menu_list);
		mBtnTimerMenuScheduleList = (Button)findViewById(R.id.timer_stoptimer_btn_menu_schedule_list);
		mBtnTimerMenuConfig = (Button)findViewById(R.id.timer_stoptimer_btn_menu_config);
		
		// 화면 테마 설정
		int themeIndex = mConfigPref.getInt(Constants.PREF_CHOOSE_THEME_INDEX, 0);
		Utils.setDisplayTheme(themeIndex, mTextHour, mTextMin, mTextSec, mTextMilSec);
		
		// Spinner 설정
		ArrayAdapter alarmAdapter = ArrayAdapter.createFromResource(mContext, R.array.spin_alarm_type, android.R.layout.simple_spinner_item);
		alarmAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinAlarmType.setAdapter(alarmAdapter);
		
		
		// Event Listener 설정
		
		mBtnSelectStopWatch.setOnClickListener(this);
		mBtnSetNReset.setOnClickListener(this);
		mBtnStartNPause.setOnClickListener(this);
		mBtnTimerMenuList.setOnClickListener(this);
		mBtnTimerMenuScheduleList.setOnClickListener(this);
		mBtnTimerMenuConfig.setOnClickListener(this);
		
		mSpinAlarmType.setOnItemSelectedListener(this);
		
		
		// 싱글 모드, 로드 모드에 따른 초기화 설정
		initTimerState();
		
	}
	
	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}
		
		// 화면 갱신 쓰레드 종료
		stopTimerViewUpdateThread();
		finish();
		overridePendingTransition(0, 0);
		
		super.onPause();
	}

	private void initTimerState() {
		switch(mTimerMode) {
			case TIMER_SINGLE_MODE:
				ctrlTimerState(TIMER_STATE_INIT_MODE);
				break;
			case TIMER_LOAD_MODE:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머를 로드 합니다.");
				}
				
				// 타이머를 로드
				mSTimer = (StopTimer)mManageTimer.getItem(mTimerLoadKey).getTimer();
				mTimerType = mSTimer.getType();
				mAlarmTypeIndex = mSTimer.getAlarmType();
				mTimerSetFlag = mSTimer.getTimerSetFlag();
				
				// 사용자가 이전에 설정한 타이머 시간 값을 가지고 와 저장한다.
				mSetHour = mSTimer.getTimerHour();
				mSetMin = mSTimer.getTimerMin();
				mSetSec = mSTimer.getTimerSec();
				mSetMilSec = mSTimer.getTimerMilSec();
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 타이머 작동 상태 : " + mSTimer.getState());
					Log.d(TAG, "현재 타이머 타입 : " + mTimerType);
					
					Log.d(TAG, "설정된 시간 값 : " + mSetHour + ", " + mSetMin + ", " + mSetSec + ", " + mSetMilSec);
					Log.d(TAG, "로드한 스케쥴 Array : " + mArrayTimerScheduleList);
					Log.d(TAG, "현재 Adapter 상태 : " + mScheduleAdapter);
				}
				
				switch(mTimerType) {
					case STimer.TIMER_TYPE_GENERAL_TIMER:
						mSpinAlarmType.setSelection(mAlarmTypeIndex);
						
						break;
					case STimer.TIMER_TYPE_SCHEDULE_TIMER:
						mArrayTimerScheduleList = mSTimer.getScheduleTimerList();
						
						if(mArrayTimerScheduleList != null) {
							// 현재 시간에 맞는 색상 설정을 위한 currentIndex 를 로드
							mCurrentIndex = mSTimer.getScheduleIndex();
							
							if(DEVELOPE_MODE) {
								Log.d(TAG, "복구된 mCurrentIndex : " + mCurrentIndex);
							}
							
							
							if(mScheduleAdapter == null) {
								mScheduleAdapter = new AdapterStopTimerScheduleView(mContext, R.layout.timer_stop_timer_schedule_list_row, mArrayTimerScheduleList);
								mScheduleAdapter.setCurrentListIndex(mCurrentIndex);
								setListAdapter(mScheduleAdapter);
							}
						}
						
						break;
				}
				
				// 화면에 표시할 타이머의 값을 재설정
				changeTimerViewText(mSTimer.getHour(), mSTimer.getMin(), mSTimer.getSec(), mSTimer.getMilsec());
				
				switch(mSTimer.getState()) {
					case STimer.TIMER_INIT:
						
						mBtnSelectStopWatch.setVisibility(View.VISIBLE);
						
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);
						
						break;
					case STimer.TIMER_PLAYING:
						
						// 로드 뒤에는 스탑 워치 모드 선택 부분을 감춘다.
						mBtnSelectStopWatch.setVisibility(View.INVISIBLE);
						
						// 쓰레드 객체 생성
						updateTimerView();
						
						// 타이머 모드를 '실행' 모드로 변경
						ctrlTimerState(TIMER_STATE_PLAYING_MODE);
						
						break;
					case STimer.TIMER_PAUSE:
						
						// 로드 뒤에는 스탑 워치 모드 선택 부분을 감춘다.
						mBtnSelectStopWatch.setVisibility(View.INVISIBLE);
						
						// 타이머 모드를 '정지' 모드로 변경 
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);
						
						break;
				}
				
				break;
				
		}
	}
	
	private void ctrlTimerState(int state) {
		mTimerState = state;
		
		switch(state) {
			case TIMER_STATE_INIT_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_start));
				mBtnSetNReset.setText(getString(R.string.btn_text_timer_set_time));
				
				break;
			case TIMER_STATE_PAUSE_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_start));
				mBtnSetNReset.setText(getString(R.string.btn_text_timer_set_time));
				
				break;
			case TIMER_STATE_PLAYING_MODE:
				mBtnStartNPause.setText(getString(R.string.btn_text_timer_stop));
				mBtnSetNReset.setText(getString(R.string.btn_text_timer_reset));
				break;
		}
		
		switch(mTimerType) {
			case STimer.TIMER_TYPE_GENERAL_TIMER:
				mLinearAlarmLayout.setVisibility(View.VISIBLE);
				break;
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
				mLinearAlarmLayout.setVisibility(View.GONE);
				break;
		}
	}
	
	private void updateTimerView() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "타이머 뷰 갱신 쓰레드 실행");
		}
		
		// 타이머 뷰 갱신 쓰레드 실행
		if(mTimerViewUpdateThread == null) {
		
			mTimerViewUpdateFlag = true;
			
			mTimerViewUpdateThread = new Thread(new Runnable() {
				
				private void updateUI() {
					if(mTimerViewUpdateThread.isInterrupted()) {
						return;
					}
					
					
					
					runOnUiThread(new Runnable() {
						public void run() {
							changeTimerViewText(mSTimer.getHour(), mSTimer.getMin(), mSTimer.getSec(), mSTimer.getMilsec());
							
							// 타이머의 정해진 시간에 도달하였다면
							if(!mSTimer.getTimerSetFlag()) {
								if(DEVELOPE_MODE) {
									Log.d(TAG, "타이머가 정해진 시간에 도달하였습니다");
								}
								
								
								mTimerSetFlag = false;
								
								mSTimer = (StopTimer)mManageTimer.getItem(mTimerLoadKey).getTimer();		//	정지된 타이머의 상태를 다시 로드
								mBtnStartNPause.performClick();
							}
							
							switch(mTimerType) {
								case STimer.TIMER_TYPE_GENERAL_TIMER:
									
									
									break;
								case STimer.TIMER_TYPE_SCHEDULE_TIMER:
									if(mScheduleAdapter != null) {
										if(mCurrentIndex <= mArrayTimerScheduleList.size()) {
											mScheduleAdapter.notifyDataSetChanged();
										}
									}
									
									break;
							}
						}
					});
				}
				
				public void run() {
					while(mTimerViewUpdateFlag) {
						
						switch(mTimerType) {
							case STimer.TIMER_TYPE_GENERAL_TIMER:
								break;
								
							case STimer.TIMER_TYPE_SCHEDULE_TIMER:
								if(mScheduleAdapter != null) {
									mCurrentIndex = mSTimer.getScheduleIndex();
									mScheduleAdapter.setCurrentListIndex(mCurrentIndex);
								}
							
								break;
						}
						
						updateUI();
						
						SystemClock.sleep(80);
					}
				}
			});
			
			mTimerViewUpdateThread.start();
		}
	}
	
	private void changeTimerViewText(int hour, int min, int sec, long milSec) {
		String strHour = null, strMin = null, strSec = null, strMilsec = null;
		
		strHour = String.format("%02d", hour);
		strMin = String.format("%02d", min);
		strSec = String.format("%02d", sec);
		strMilsec = String.valueOf(milSec);
		
		mTextHour.setText(strHour);
		mTextMin.setText(strMin);
		mTextSec.setText(strSec);
		mTextMilSec.setText(strMilsec);
	}
	
	private void stopTimerViewUpdateThread() {
		// 화면 갱신 쓰레드 종료
		mTimerViewUpdateFlag = false;
		mTimerViewUpdateThread = null;
	} 
	
	private void startTimerListActivity() {
		Intent timerStopIntent = new Intent(STimerStopTimer.this, STimerList.class);
		startActivity(timerStopIntent);
		overridePendingTransition(0, 0);
		finish();
	}
	
	private void getIntenter() {
		Intent intent = getIntent();
		
		if(intent != null) {
			mTimerMode = intent.getExtras().getInt(Constants.PUT_TIMER_MODE);
			
			switch(mTimerMode) {
				case TIMER_SINGLE_MODE:
//					mTimerLoadKey = DEFAULT_LOAD_KEY;
					break;
				case TIMER_LOAD_MODE:
					mTimerLoadKey = intent.getExtras().getInt(Constants.PUT_TIMER_LOAD_KEY);
					break;
			}
		}
	}
	

	public void onClick(View v) {
		switch(v.getId()) {
		
			// 타이머 제어 버튼
			case R.id.timer_stoptimer_btn_start_n_pause:			//	시작 및 정지 버튼
				switch(mTimerState) {
					case TIMER_STATE_INIT_MODE:							//	처음 시작 버튼을 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "처음 시작 버튼을 클릭");
							Log.d(TAG, "현재 타이머 타입 : " + mTimerType);
							Log.d(TAG, "현재 설정된 알람 타입 : " + mAlarmTypeIndex);
						}
						
						if(mTimerSetFlag) {		//	설정된 시간이 존재한다면
							
							// 다이얼로그에서 설정된 값을 읽어 오든, 화면에 설정된 값을 읽어오든 해서 처리
							mManageTimer.addStopTimerThread(mContext, mTimerType, "", mSetHour, mSetMin, mSetSec, mAlarmTypeIndex, null);
							mManageTimer.startTimerThread(mManageTimer.getLastKey());
							
							startTimerListActivity();
							
							
						} else {				//	설정된 시간이 존재하지 않는다면
							Toast.makeText(mContext, getString(R.string.toast_caution_want_time_set), Toast.LENGTH_SHORT).show();
						}
						
						
						break;
						
					case TIMER_STATE_PLAYING_MODE:						//	정지 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "정지 버튼 클릭");
						}
						
						// 화면 갱신 쓰레드 종료
						stopTimerViewUpdateThread();
						
						// 타이머 정지
						mManageTimer.pauseTimerThread(mTimerLoadKey);
						
						// 정지 모드로 변환
						ctrlTimerState(TIMER_STATE_PAUSE_MODE);
						
						break;
						
					case TIMER_STATE_PAUSE_MODE:						//	시작 버튼 클릭
						
						if(DEVELOPE_MODE) {
							Log.d(TAG, "시작 버튼 클릭");
						}
						
						if(mTimerSetFlag) {
							if(mSTimer.getState() == STimer.TIMER_INIT) {				//	타이머를 추가한 뒤, 처음 실행하는 것이라면 Start 를 호출
								mManageTimer.startTimerThread(mTimerLoadKey);
								startTimerListActivity();
							} else {
								// 화면 갱신 쓰레드 재생성
								updateTimerView();
								mManageTimer.resumeTimerThread(mTimerLoadKey);			//	생성 된 타이머를 재실행 하는 것이라면 resume 을 호출
							}
							
							// 실행 모드로 변환
							ctrlTimerState(TIMER_STATE_PLAYING_MODE);
						} else {
							Toast.makeText(mContext, getString(R.string.toast_caution_want_time_set), Toast.LENGTH_SHORT).show();
						}
						
						break;
				}
				
				break;
				
				
			case R.id.timer_stoptimer_btn_set_n_reset:				//	시간 설정 및 재설정 버튼
				switch(mTimerState) {
				
					case TIMER_STATE_INIT_MODE:
						if(DEVELOPE_MODE) {
							Log.d(TAG, "처음 시간 설정 버튼을 클릭");			//	처음 시간 설정 버튼 클릭
						}

						// 다이얼로그에서 값을 가져와서 타이머 TextView에 설정
						final STimePickDialog timePickDialog = new STimePickDialog(mContext);
						timePickDialog.setTimePickHour(mSetHour);
						timePickDialog.setTimePickMin(mSetMin);
						timePickDialog.setTimePickSec(mSetSec);
						
						timePickDialog.show();
						
						timePickDialog.setOnDismissListener(new OnDismissListener() {
							public void onDismiss(DialogInterface dialog) {
								if(timePickDialog.getDialogState()) {
									mSetHour = timePickDialog.getTimePickHour();
									mSetMin = timePickDialog.getTimePickMin();
									mSetSec = timePickDialog.getTimePickSec();
									mSetMilSec = 0;
									
									changeTimerViewText(mSetHour, mSetMin, mSetSec, mSetMilSec);
									
									if(mSetHour == 0 && mSetMin == 0 && mSetSec == 0 && mSetMilSec ==0) {
										mTimerSetFlag = false;
									} else {
										mTimerSetFlag = true;
									}
								}
							}
						});
						
						break;
						
					
					case TIMER_STATE_PAUSE_MODE:
						if(DEVELOPE_MODE) {
							Log.d(TAG, "시간 설정 버튼 클릭");				//	시간 설정 버튼 클릭
							Log.d(TAG, "mSTimer : " + mSTimer);
						}
						
						
						// 일단, 사용자가 이전에 설정한 값을 타이머 다이얼로그에 로드하여 표시
						final STimePickDialog reTimePickDialog = new STimePickDialog(mContext);
						reTimePickDialog.setTimePickHour(mSTimer.getTimerHour());
						reTimePickDialog.setTimePickMin(mSTimer.getTimerMin());
						reTimePickDialog.setTimePickSec(mSTimer.getTimerSec());
						
						reTimePickDialog.show();
						
						// 사용자가 타이머 값을 재설정하였다면
						reTimePickDialog.setOnDismissListener(new OnDismissListener() {
							public void onDismiss(DialogInterface dialog) {
								if(reTimePickDialog.getDialogState()) {
									// 시간 값 재설정
									mSetHour = reTimePickDialog.getTimePickHour();
									mSetMin = reTimePickDialog.getTimePickMin();
									mSetSec = reTimePickDialog.getTimePickSec();
									mSetMilSec = 0;
									
									// 이미 로드된 STimer 객체의 시간 값을 수정
									mSTimer.setTimerHour(mSetHour);
									mSTimer.setTimerMin(mSetMin);
									mSTimer.setTimerSec(mSetSec);
									mSTimer.setTimerMilSec(mSetMilSec);
									
									switch(mTimerType) {
										case STimer.TIMER_TYPE_GENERAL_TIMER:
											mAlarmTypeIndex = ScheduleTimer.MODE_ALARM;
											
											break;
										case STimer.TIMER_TYPE_SCHEDULE_TIMER:
											// 사용자가 설정한 시간에 맞게 인덱스를 설정
											mCurrentIndex = caculateScheduleIndex(mSetHour, mSetMin, mSetSec);
											
											// 스케쥴을 표시할 인덱스 초기화
											mSTimer.setScheduleIndex(mCurrentIndex);
											
											if(mScheduleAdapter != null) {
												mScheduleAdapter.setCurrentListIndex(mCurrentIndex);
												mScheduleAdapter.notifyDataSetChanged();
											}
											
											break;
									}
									
									
									// 화면 타이머 표시 수정
									changeTimerViewText(mSetHour, mSetMin, mSetSec, mSetMilSec);
									
									if(mSetHour == 0 && mSetMin == 0 && mSetSec == 0 && mSetMilSec ==0) {
										mTimerSetFlag = false;
									} else {
										mTimerSetFlag = true;
									}
									
									mSTimer.setTimerSetFlag(mTimerSetFlag);
								}
							}
						});
						
						
						break;
				
				
					case TIMER_STATE_PLAYING_MODE:						//	재설정 버튼 클릭
						if(DEVELOPE_MODE) {
							Log.d(TAG, "재설정 버튼 클릭");
						}
						
						mBtnStartNPause.performClick();
						
						switch(mTimerType) {
							case STimer.TIMER_TYPE_GENERAL_TIMER:
								mAlarmTypeIndex = ScheduleTimer.MODE_ALARM;
								
								break;
							case STimer.TIMER_TYPE_SCHEDULE_TIMER:
								// 타이머 스케쥴을 재설정
								mCurrentIndex = caculateScheduleIndex(mSetHour, mSetMin, mSetSec);
								
								mSTimer.setScheduleIndex(mCurrentIndex);
								
								if(mScheduleAdapter != null) {
									mScheduleAdapter.setCurrentListIndex(mCurrentIndex);
									mScheduleAdapter.notifyDataSetChanged();
								}
								
								break;
						}
						
						
						// 타이머의 값을 재설정
						mSTimer.setTimerHour(mSetHour);
						mSTimer.setTimerMin(mSetMin);
						mSTimer.setTimerSec(mSetSec);
						mSTimer.setTimerMilSec(mSetMilSec);
						
						// 쓰레드의 값을 재설정
						mManageTimer.getItem(mTimerLoadKey).getTimerThread().setTimerToThread();
						
						// 화면에 표시되는 값을 재설정
						changeTimerViewText(mSTimer.getHour(), mSTimer.getMin(), mSTimer.getSec(), mSTimer.getMilsec());
						
						break;
						
					
				}
				
				break;
				
				
				
			// 스탑워치 모드 버튼	
			case R.id.timer_stoptimer_btn_stopwatch:
				Intent timerModeIntent = new Intent(STimerStopTimer.this, STimerStopWatch.class);
				timerModeIntent.putExtra(Constants.PUT_TIMER_MODE, mTimerMode);
				startActivity(timerModeIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
				
				
			// 메뉴 버튼 부분
			case R.id.timer_stoptimer_btn_menu_list:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 리스트 메뉴 버튼 클릭");
				}
				
				
				if(!mManageTimer.isHashMapEmpty()) {
					// 화면 갱신 쓰레드 종료
					stopTimerViewUpdateThread();
					
					Intent timerListIntent = new Intent(STimerStopTimer.this, STimerList.class);
					startActivity(timerListIntent);
					overridePendingTransition(0, 0);
					
					finish();
				}
				
				break;
				
			case R.id.timer_stoptimer_btn_menu_schedule_list:
				
				// 화면 갱신 쓰레드를 종료
				stopTimerViewUpdateThread();
				
				// 스케쥴 리스트 Activity 호출
				Intent scheduleIntent = new Intent(STimerStopTimer.this, STimerCategoryList.class);
				startActivity(scheduleIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
				
			case R.id.timer_stoptimer_btn_menu_config:

				// 화면 갱신 쓰레드 종료
				stopTimerViewUpdateThread();
				
				// 타이머 설정 Activity 호출
				Intent configIntent = new Intent(STimerStopTimer.this, STimerConfig.class);
				startActivity(configIntent);
				overridePendingTransition(0, 0);
				
				finish();
				
				break;
		}
	}
	
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		mAlarmTypeIndex = position;
		
		switch(mTimerMode) {
			case TIMER_LOAD_MODE:
				mSTimer.setAlarmType(mAlarmTypeIndex);
				
				break;
		}
		
	}
	
	private int caculateScheduleIndex(int setHour, int setMin, int setSec) {
		// 사용자가 설정한 시간에 맞게 인덱스를 설정
		int currentIndex = 0;
		
		ArrayList<TimerScheduleList> checkScheduleItems = mSTimer.getScheduleTimerList();
		int checkScheduleSize = checkScheduleItems.size();
		
		int pickTotalSec = (setHour * 3600) + (setMin * 60) + setSec;
		
		for(int i=0; i < checkScheduleSize; i++) {
			int timerTotalSec = (checkScheduleItems.get(i).getHour() * 3600) 
					+ (checkScheduleItems.get(i).getMin() * 60) + checkScheduleItems.get(i).getSec();
			
			if(timerTotalSec >= pickTotalSec) {
				currentIndex++;
			}
		}
		
		return currentIndex;
	}
	
	public void onNothingSelected(AdapterView<?> arg0) {}

	// 뒤로가기 버튼을 누르면, Timer 화면 갱신을 중지하고, Activity 를 종료한다.
	@Override
	public void onBackPressed() {
		// 화면 갱신 쓰레드 종료
		stopTimerViewUpdateThread();
		finish();
	}
}
