package madcat.studio.timer;

import madcat.studio.constants.Constants;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.service.LoadDatabase;
import madcat.studio.utils.ContextPool;
import madcat.studio.utils.MessagePool;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Toast;

public class SLoading extends Activity {
	
	private Context mContext;
	private ContextPool mContextPool;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.loading);
        this.mContext = this;

        // 앱에서 사용할 Context 를 저장
        this.mContextPool = ContextPool.getInstance();
        this.mContextPool.setContext(mContext);
        
        InitLoading initLoading = new InitLoading(mContext);
        initLoading.execute();
    }
    
    class InitLoading extends AsyncTask<Void, Void, Void> {
    	
    	boolean firstAppFlag;
    	boolean succeedLoadDbFlag;
    	
    	SharedPreferences configPref;
    	ProgressDialog progressDig;
    	Context context;
    	
    	public InitLoading(Context context) {
    		this.context = context;
		}
    	
    	@Override
    	protected void onPreExecute() {
    		configPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
    		
    		firstAppFlag = configPref.getBoolean(Constants.PREF_APP_FIRST, true);
    		
    		if(firstAppFlag) {
    			progressDig = ProgressDialog.show(context, "", getString(R.string.prg_init_loading));
    		}
    	}
    	
		@Override
		protected Void doInBackground(Void... arg0) {
			if(firstAppFlag) {
				succeedLoadDbFlag = LoadDatabase.loadDataBase(context.getResources().getAssets(), context);
			}
			
			SystemClock.sleep(Constants.LOADING_DELAY * 1000);
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			if(firstAppFlag) {
				if(progressDig.isShowing()) {
					progressDig.dismiss();
				}
			
				if(!succeedLoadDbFlag) {
					Toast.makeText(context, context.getString(R.string.toast_fail_create_database), Toast.LENGTH_SHORT).show();
					finish();
				}
				
				SharedPreferences.Editor editor = configPref.edit();
				editor.putBoolean(Constants.PREF_APP_FIRST, false);
				editor.commit();
			}
			
			startTimerActivity();
		}
    }
    
    /**
     * 타이머 뷰를 실행하는 함수입니다.
     */
    private void startTimerActivity() {
    	STimerManage manageTimer = STimerManage.getInstance();
    	
    	Intent startIntent = null;

    	if(!manageTimer.isHashMapEmpty()) {
    		startIntent = new Intent(SLoading.this, STimerList.class);
    	} else {
    		startIntent = new Intent(SLoading.this, STimerStopWatch.class);
    		startIntent.putExtra(Constants.PUT_TIMER_MODE, STimerStopWatch.TIMER_SINGLE_MODE);
    	}
    	
    	startActivity(startIntent);
    	overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    	finish();
    }
}




