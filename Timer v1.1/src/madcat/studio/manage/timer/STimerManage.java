package madcat.studio.manage.timer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.STotal;
import madcat.studio.data.StopTimer;
import madcat.studio.data.StopWatch;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.service.AlarmWakeLockService;
import madcat.studio.service.NotificationTimer;
import madcat.studio.service.ScheduleAlarmService;
import madcat.studio.thread.STimerThread;
import madcat.studio.utils.ContextPool;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class STimerManage {
	
	private final String TAG										=	"ManageTimer";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private static STimerManage mInstance							=	new STimerManage();
	
	private LinkedHashMap<Integer, STotal> mTotalHashMap;
	
	private int mKeyNumber;
	
	private NotificationTimer mNotiTimer;
	private AlarmWakeLockService mAlarmWakeLockService;
	
	private Context mContext;
	private ContextPool mContextPool;

	private AlarmManager mAlarmManager;
	private Intent mDummyIntent;
	private PendingIntent mDummyPending;
	
	private STimerManage() {
		mTotalHashMap = new LinkedHashMap<Integer, STotal>();
		mKeyNumber = 0;
		mNotiTimer = NotificationTimer.getInstance();
		mContextPool = ContextPool.getInstance();
	}
	
	public void addStopWatchThread(Context context, String name) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "스탑워치를 추가합니다");
		}
		
		this.mContext = context;
		
		if(isHashMapEmpty()) {
			mDummyIntent = new Intent("dummy.action");
			mDummyPending = PendingIntent.getBroadcast(mContext, 0, mDummyIntent, 0);
			
			mAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 500, mDummyPending);
		}
		
		
		// 타이머를 HashMap 에 추가합니다.
		mKeyNumber++;
		
		StopWatch stopWatch = new StopWatch(name, STimer.TIMER_TYPE_STOPWATCH);
		STimerThread timerThread = new STimerThread(stopWatch);					//	타이머가 사용할 쓰레드 생성 한 뒤 데이터를 넘겨 준다.
		STotal sTotal = new STotal(mKeyNumber, timerThread, stopWatch);		//	쓰레드와 데이터를 묶어서 관리할 Total 객체 생성
		
		mTotalHashMap.put(mKeyNumber, sTotal);									//	Toatal 객체 HahsMap에 추가
		
		mNotiTimer.registeNotification(context);
	}
	
	public void addStopTimerThread(Context context, int type, 
			String name, int hour, int min, int sec, int alarmType, ArrayList<TimerScheduleList> scheduleArray) {

		this.mContext = context;
		
		if(isHashMapEmpty()) {
			mDummyIntent = new Intent("dummy.action");
			mDummyPending = PendingIntent.getBroadcast(mContext, 0, mDummyIntent, 0);
			
			mAlarmManager = (AlarmManager) mContext.getSystemService(Context.ALARM_SERVICE);
			mAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000, mDummyPending);
			
		}

		// 스케쥴 알람 서비스를 설정합니다.
		switch(type) {
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
				ScheduleAlarmService scheduleAlarm = ScheduleAlarmService.getInstance();
				scheduleAlarm.initAlarm(context);
				break;
		}
		
		// 타이머가 시간이 도달하였을 경우 알람을 울리면서 핸드폰을 깨우기 위한 서비스 설정
		mAlarmWakeLockService.wakeLock(context);
		
		mContextPool.setContext(context);
		
		
		// 타이머를 HashMap 에 추가합니다.
		mKeyNumber++;
		
		StopTimer stopTimer = new StopTimer(name, type);
		
		stopTimer.setIndexKey(mKeyNumber);
		stopTimer.setTimerHour(hour);
		stopTimer.setTimerMin(min);
		stopTimer.setTimerSec(sec);
		stopTimer.setTimerMilSec(0);
		stopTimer.setTimerSetFlag(true);
		stopTimer.setAlarmType(alarmType);
		
		
		stopTimer.setScheduleTimerList(scheduleArray);
		
		STimerThread timerThread = new STimerThread(stopTimer);					//	타이머가 사용할 쓰레드 생성 한 뒤 데이터를 넘겨 준다.
		STotal sTotal = new STotal(mKeyNumber, timerThread, stopTimer);		//	쓰레드와 데이터를 묶어서 관리할 Total 객체 생성
		
		mTotalHashMap.put(mKeyNumber, sTotal);									//	Toatal 객체 HahsMap에 추가
		
		mNotiTimer.registeNotification(context);
	}
	
	public void startTimerThread(int key) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "등록 된 타이머를 시작");
		}
		
		STimerThread timerThread = mTotalHashMap.get(key).getTimerThread();
		timerThread.setThreadFlag(true);
		timerThread.start();
		
		mNotiTimer.updateAddNotification(mTotalHashMap.get(key).getTimer().getType());
	}
	
	public void pauseTimerThread(int key) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "등록 된 타이머를 정지");
		}
		
		if(!mTotalHashMap.isEmpty()) {
			mTotalHashMap.get(key).getTimerThread().setThreadFlag(false);
			mNotiTimer.updateRemoveNotification(mTotalHashMap.get(key).getTimer().getType());
		}
	}
	
	public void resumeTimerThread(int key) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "등록 된 타이머를 재시작");
		}
		
		
		if(!mTotalHashMap.isEmpty()) {
			STimer loadTimer = mTotalHashMap.get(key).getTimer();
			STimerThread timerThread = new STimerThread(loadTimer);
	
			mTotalHashMap.get(key).setTimerThread(timerThread);			//	타이머 쓰레드 객체를 새롭게 갱신
			
			startTimerThread(key);
		}
	}
	
	public void removeTimerThread(int key) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, key + " 값의 등록 된 타이머를 삭제");
		}
		
		if(!mTotalHashMap.isEmpty()) {
			// SmartTimer 가 현재 동작 중이면 정지를 시키고 그렇지 않다면, 삭제를 바로 한다.
			if(mTotalHashMap.get(key).getTimerThread().getThreadFlag()) {
				pauseTimerThread(key);					//	먼저, 해당 키 값의 타이머를 정지
			}
			
			mTotalHashMap.remove(key);					//	HashMap 에서 STotal 를 삭제
			
			if(mTotalHashMap.isEmpty()) {				//	만일, 키 삭제 후 HashMap 의 값이 비어있다면, KeyNumber 값을 0 으로 초기화
				mKeyNumber = 0;
				
				mNotiTimer.removeNotification();		//	키 삭제 후 HashMap 이 비어있다면, Notification 을 제거한다.

				if(mAlarmManager != null) {
					mAlarmManager.cancel(mDummyPending);
				}
				
			}
		}
		
	}
	
	public HashMap<Integer, STotal> getHashMap() {
		return mTotalHashMap;
	}
	
	public int getLastKey() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "마지막 키 값 : " + mKeyNumber);
		}
		
		
		return mKeyNumber;
	}
	
	public STotal getItem(int key) {
		return mTotalHashMap.get(key);
	}
	
	public boolean isHashMapEmpty() {
		if(mTotalHashMap != null) {
			if(mTotalHashMap.isEmpty()) {
				return true;
			}
		}
		
		return false;
	}
	
	public int getTimerHashMapSize() {
		return mTotalHashMap.size();
	}
	
	public static STimerManage getInstance() {
		return mInstance;
	}

}
