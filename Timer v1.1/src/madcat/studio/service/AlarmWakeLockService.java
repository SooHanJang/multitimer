package madcat.studio.service;

import android.content.Context;
import android.os.PowerManager;

public class AlarmWakeLockService {

	private static final String TAG										=	"AlarmWakeLockService";
	private static PowerManager.WakeLock mWakeLock;
	
	public static void wakeLock(Context context) {
		if(mWakeLock != null) {
			return;
		}
		
		PowerManager powerManager = (PowerManager)context.getSystemService(Context.POWER_SERVICE);
		mWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK, TAG);
		mWakeLock.acquire();
	}
	
	public static void releaseWakeLock() {
		if(mWakeLock != null) {
			mWakeLock.release();
			mWakeLock = null;
		}
	}
}
