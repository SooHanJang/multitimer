package madcat.studio.service;

import java.io.IOException;

import madcat.studio.constants.Constants;
import madcat.studio.data.ScheduleTimer;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Vibrator;
import android.util.Log;

public class ScheduleAlarmService {
	
	private final String TAG										=	"ScheduleAlarmService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private final int VIBRATOR_DELAY								=	1000;
	
	public static ScheduleAlarmService mTimerAlarmService			=	new ScheduleAlarmService();
	
	// 환경설정 관련 변수
	private SharedPreferences mConfigPref;
	
	// 벨소리 관련 변수
	private AssetFileDescriptor mAfd;
	private MediaPlayer mMediaPlayer;
	
	// 진동 관련 변수
	private Vibrator mVibrate;
	
	private ScheduleAlarmService() {
		mMediaPlayer = new MediaPlayer();
	}
	
	public static ScheduleAlarmService getInstance() {
		return mTimerAlarmService;
	}
	
	public void initAlarm(Context context) {
		// 알람과 진동이 작동 중이라면 먼저 중지한다.
		if(context != null) {
			// 벨소리 설정
			try {
				mConfigPref = context.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
				
				mAfd = context.getAssets().openFd("alarm/schedule_alarm_" + 
						mConfigPref.getInt(Constants.PREf_SCHEDULE_ALARM_INDEX, 0) + ".mp3");
				
				mMediaPlayer = new MediaPlayer();
				
				if(!mMediaPlayer.isPlaying()) {
					mMediaPlayer.setDataSource(mAfd.getFileDescriptor(), mAfd.getStartOffset(), mAfd.getLength());
					
					mMediaPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
					mMediaPlayer.prepare();
					
				}
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			// 진동 설정
			mVibrate = (Vibrator)context.getSystemService(Context.VIBRATOR_SERVICE);

		}
	}
	
	public void playAlarm(int type) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "playAlarm 호출");
		}
		
		switch(type) {
			case ScheduleTimer.MODE_ALARM:
				if(mMediaPlayer != null) {
					mMediaPlayer.start();
				}
				
				
				break;
			case ScheduleTimer.MODE_SHAKE:
				if(mVibrate != null) {
					mVibrate.vibrate(VIBRATOR_DELAY);
				}
				
				break;
			case ScheduleTimer.MODE_MUTE:
				
				break;
			case ScheduleTimer.MODE_ALARM_SHAKE:
				if(mMediaPlayer != null && mVibrate != null) {
					mMediaPlayer.start();
					mVibrate.vibrate(VIBRATOR_DELAY);
				}
				
				break;
		}
	}
	
}
