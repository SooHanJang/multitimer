package madcat.studio.service;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.timer.R;
import madcat.studio.timer.STimerList;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

public class NotificationTimer {

	private final String TAG										=	"NotificationTimer";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private static NotificationTimer mInstance						=	new NotificationTimer();
	
	private Context mContext;
	
	private NotificationManager mNotificationManager;
	private Notification mNotification;
	private Intent mIntent;
	private PendingIntent mPendingIntent;
	
	private int mStopTimerSize, mStopWatchSize;
	
	private NotificationTimer() {}
	
	public void registeNotification(Context context) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Notification 을 등록합니다.");
		}
		
		mContext = context;
		
		mNotificationManager = (NotificationManager)mContext.getSystemService(Context.NOTIFICATION_SERVICE);

		mIntent = new Intent(mContext, STimerList.class);
		mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
								 Intent.FLAG_ACTIVITY_CLEAR_TOP |
								 Intent.FLAG_ACTIVITY_SINGLE_TOP);
		
		// PendingIntent Flag 처리를 해야한다.
		mPendingIntent = PendingIntent.getActivity(mContext, 0, mIntent, 0);

		mNotification = new Notification();
		mNotification.flags = Notification.FLAG_ONGOING_EVENT;
		mNotification.icon = R.drawable.timer_icon_notification;
		mNotification.when = System.currentTimeMillis();
		
	}
	
	public void updateAddNotification(int type) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Notification 추가 상황을 update 합니다");
		}
		
		switch(type) {
			case STimer.TIMER_TYPE_STOPWATCH:
				mNotification.tickerText = mContext.getString(R.string.noti_text_add_stopwatch);
				mStopWatchSize++;
				
				break;
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
			case STimer.TIMER_TYPE_GENERAL_TIMER:
				mNotification.tickerText = mContext.getString(R.string.noti_text_add_timer);
				mStopTimerSize++;
				
				break;
		}
		
		mNotification.setLatestEventInfo(mContext, mContext.getString(R.string.noti_text_postfix_title), 
				mContext.getString(R.string.noti_text_prefix_timer_text) + "(" + mStopTimerSize + "), " +
				mContext.getString(R.string.noti_text_prefix_stopwatch_text) + "(" + mStopWatchSize + ") " + 
				mContext.getString(R.string.noti_text_postfix_text), mPendingIntent);
		
		mNotificationManager.notify(Constants.APP_NUMBER, mNotification);
	}
	
	public void updateRemoveNotification(int type) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Notification 삭제 상황을 update 합니다.");
		}
		
		switch(type) {
			case STimer.TIMER_TYPE_STOPWATCH:
				mStopWatchSize--;
				
				break;
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
			case STimer.TIMER_TYPE_GENERAL_TIMER:
				mStopTimerSize--;
				break;
		}
		
		mNotification.setLatestEventInfo(mContext, mContext.getString(R.string.noti_text_postfix_title), 
				mContext.getString(R.string.noti_text_prefix_timer_text) + "(" + mStopTimerSize + "), " +
				mContext.getString(R.string.noti_text_prefix_stopwatch_text) + "(" + mStopWatchSize + ") " + 
				mContext.getString(R.string.noti_text_postfix_text), mPendingIntent);
		
		mNotificationManager.notify(Constants.APP_NUMBER, mNotification);
	}
	
	public void removeNotification() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Notification 을 삭제합니다");
		}
		
		
		if(mNotificationManager != null) {
			mStopTimerSize = 0;
			mStopWatchSize = 0;
			
			mNotificationManager.cancel(Constants.APP_NUMBER);
		}
	}
	
	public static NotificationTimer getInstance() {
		return mInstance;
	}
	
}











