package madcat.studio.service;

import madcat.studio.timer.R;
import madcat.studio.voice.VoiceInput;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

public class VoiceWidgetService extends AppWidgetProvider {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		
		int idSize = appWidgetIds.length;
		
		for(int i=0; i < idSize; i++) {
			int appWidgetId = appWidgetIds[i];
			updateWidget(context, appWidgetManager, appWidgetId);
			
		}
	}

	public void updateWidget(Context context, AppWidgetManager appWidgetManager, int appWidgetId) {
		RemoteViews updateView = new RemoteViews(context.getPackageName(), R.layout.widget_voice_timer);
		
		// 음성 인식 버튼 설정
		Intent voiceIntent = new Intent(context, VoiceInput.class);
		updateView.setOnClickPendingIntent(R.id.widget_timer_voice, 
				PendingIntent.getActivity(context, 0, voiceIntent, PendingIntent.FLAG_UPDATE_CURRENT));
		
		appWidgetManager.updateAppWidget(appWidgetId, updateView);
	}

}
