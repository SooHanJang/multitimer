package madcat.studio.service;

import java.io.IOException;

import madcat.studio.constants.Constants;
import madcat.studio.data.ScheduleTimer;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.SystemClock;
import android.os.Vibrator;
import android.util.Log;

public class TimerAlarmService {
	
	private final String TAG										=	"TimerAlarmService";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private final int VIBRATOR_DELAY								=	1500;
	
	public static TimerAlarmService mTimerAlarmService				=	new TimerAlarmService();
	
	private Context mContext;
	private SharedPreferences mConfigPref;

	// 벨소리 관련 변수
	private Uri mRingtoneUri;
	private AudioManager mAudioManager;
	private MediaPlayer mMediaPlayer;
	private RingtoneManager mRingToneManager;
	
	// 진동 관련 변수
	private Thread mVibratorThread;
	private boolean mVibratorFlag									=	false;
	
	private TimerAlarmService() {
		mMediaPlayer = new MediaPlayer();
	}
	
	public static TimerAlarmService getInstance() {
		return mTimerAlarmService;
	}
	
	public void setContext(Context context) {	this.mContext = context;	}
	
	public void initAlarm() {
		// 알람과 진동이 작동 중이라면 먼저 중지한다.
		this.stopAlarm();
		this.stopVibration();
		
		if(mContext != null) {
			mAudioManager = (AudioManager)mContext.getSystemService(Context.AUDIO_SERVICE);
			mRingToneManager = new RingtoneManager(mContext);
			mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
			
			// 벨소리 설정
			mRingtoneUri = RingtoneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE);
	        
	        try {
	        	mMediaPlayer = new MediaPlayer();
	        	
	        	if(mRingtoneUri != null) {
	             	mMediaPlayer.setDataSource(mConfigPref.getString(Constants.PREF_ALARM_INDEX_PATH, 
	             			mRingToneManager.getActualDefaultRingtoneUri(mContext, RingtoneManager.TYPE_RINGTONE).toString()));
	             	
	             	mMediaPlayer.setAudioStreamType(AudioManager.STREAM_RING);
	    			mMediaPlayer.setLooping(true);
	    			mMediaPlayer.prepare();
	            } 
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			// 진동 설정
			mVibratorThread = new Thread(new Runnable() {
				public void run() {
					Vibrator vi = (Vibrator)mContext.getSystemService(Context.VIBRATOR_SERVICE);

					while(mVibratorFlag) {
						vi.vibrate(VIBRATOR_DELAY);
						SystemClock.sleep(VIBRATOR_DELAY + 1000);
					}
				}
			});
		}
	}
	
	public void playAlarm(int type) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "playAlarm 호출");
		}
		
		switch(type) {
			case ScheduleTimer.MODE_ALARM:
//				mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
				
				if(mRingtoneUri != null) {
					if(mMediaPlayer != null) {
						mMediaPlayer.start();
					}
				}
				
				
				break;
			case ScheduleTimer.MODE_SHAKE:
				
				if(mVibratorThread != null) {
					mVibratorFlag = true;
					mVibratorThread.start();
				}
				
//				mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
				
				
				break;
			case ScheduleTimer.MODE_MUTE:
//				mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
				
				break;
			case ScheduleTimer.MODE_ALARM_SHAKE:
				if(mRingtoneUri != null && mVibratorThread != null) {
					mVibratorFlag = true;
					
					mMediaPlayer.start();
					mVibratorThread.start();
				}
				
//				mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
				
				
				break;
		}
	}
	
	public void stopAlarm() {
		if(mMediaPlayer != null) {
			if(mMediaPlayer.isPlaying()) {
				mMediaPlayer.stop();
			}
		}
	}
	
	public void stopVibration() {
		if(mVibratorThread != null) {
			if(mVibratorThread.isAlive()) {
				mVibratorFlag = false;
				mVibratorThread = null;
			}
		}
	}
	
	public void stopAllAlarm() {
		stopAlarm();
		stopVibration();
	}
	
	private void setVibratorFlag(boolean flag) {	this.mVibratorFlag = flag;	}
	private boolean getVibratorFlag() {	return mVibratorFlag;	}
	
}
