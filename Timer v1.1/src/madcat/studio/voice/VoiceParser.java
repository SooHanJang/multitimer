package madcat.studio.voice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.StopTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class VoiceParser {
	
	private final String TAG										=	"VoiceParser";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private final String DELIMETER									=	"|";
	public static final int TIMER_TYPE_IS_NEW_TIMER					=	1;
	public static final int TIMER_TYPE_IS_SCHEDULE_TIMER			=	2;
	private static VoiceParser mInstance							=	new VoiceParser();
	
	private int mTimerType											=	TIMER_TYPE_IS_NEW_TIMER;
	
	private VoiceParser() {}
	
	public static VoiceParser getInstance() {
		return mInstance;
	}
	
	/**
	 * Raw 음성에서 타이머를 뽑아내어 중복을 제거한 뒤 ArrayList<TimerObject> 에 추가하여 반환합니다.
	 * 
	 * @param items
	 * @return
	 */
	
	public int getTimerType() {
		return mTimerType;
	}

	public ArrayList<StopTimer> voiceParsing(Context context, ArrayList<String> items) {

		ArrayList<StopTimer> results = new ArrayList<StopTimer>();
		ArrayList<String> voiceItems = new ArrayList<String>();
		StringBuffer strBuf = new StringBuffer();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);

		int itemSize = items.size();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "DB 체크 시작");
		}
		
		for(int i=0; i < itemSize; i++) {
			initStringBuffer(strBuf);
			String str = items.get(i).replaceAll(" ", "").replaceAll("'", "");
			
			if(Constants.DEVELOPE_MODE) {
				Log.d(TAG, "입력 값 : " + str);
			}
			
			// DB 값을 체크
			String selectTimerSql = "SELECT * FROM " + Constants.TABLE_TIMER + 
								 	" WHERE " + Constants.TIMER_NAME + " LIKE '%" + str.trim() + "%'";
			Cursor selectTimerCursor = sdb.rawQuery(selectTimerSql, null);

			if(selectTimerCursor.getCount() != 0) {
				mTimerType = TIMER_TYPE_IS_SCHEDULE_TIMER;
				
				while(selectTimerCursor.moveToNext()) {
					if(DEVELOPE_MODE) {
						Log.d(TAG, "db value : " + selectTimerCursor.getString(2));
					}
					
					StopTimer timerItem = new StopTimer(selectTimerCursor.getString(2), STimer.TIMER_TYPE_SCHEDULE_TIMER);
					timerItem.setTimerHour(selectTimerCursor.getInt(3));
					timerItem.setTimerMin(selectTimerCursor.getInt(4));
					timerItem.setTimerSec(selectTimerCursor.getInt(5));
					timerItem.setTimerMilSec(0);
					timerItem.setTimerSetFlag(true);
					timerItem.setAlarmType(selectTimerCursor.getInt(6));
					
					ArrayList<TimerScheduleList> loadScheduleArray = 
						(ArrayList<TimerScheduleList>) Utils.deSerializeObject(selectTimerCursor.getBlob(8));
					timerItem.setScheduleTimerList(loadScheduleArray);
					
					results.add(timerItem);
					
				}
				
				selectTimerCursor.close();
				sdb.close();
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "DB 체크 종료, 값이 존재하므로 리턴합니다.");
				}
				
				return results;
			} else {
				selectTimerCursor.close();
			}
		}
		
		sdb.close();
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "DB 에 값이 존재하지 않습니다. 타이머 체크 시작");
		}
		
		for(int i=0; i < itemSize; i++) {
			initStringBuffer(strBuf);
			String str = items.get(i).replaceAll(" ", "");
			
			if(Constants.DEVELOPE_MODE) {
				Log.d(TAG, "입력 값 : " + str);
			}
			
			// DB 값이 아닌, 타이머라면
			mTimerType = TIMER_TYPE_IS_NEW_TIMER;
			String splitResult = splitVoiceTimer(str, strBuf);
			
			if(splitResult != null) {
				voiceItems.add(splitResult);
			}
		}
		
		if(voiceItems.size() != 0) {
			// 정렬 뒤 중복 제거
//			sortDescArray(voiceItems);
			
			ArrayList<String> distinctArray = new ArrayList<String>();
			distinctArray = distinctTimer(voiceItems);
			
			// 구분자 제거 뒤 객체로 생성한 다음, ArrayList에 넣어 리턴
			for(int i=0; i < distinctArray.size(); i++) {
				results.add(getSplitDelimeter(distinctArray.get(i)));
			}
			
			return results;
		} else {
			Log.d(TAG, "다시 한번 입력해 주세요");
			return null;
		}
	}
	
	private ArrayList<String> sortDescArray(ArrayList<String> items) {
		Collections.sort(items);
		return items;
	}
	
	/**
	 * ArrayList<String> 중복을 제거한 뒤, 반환합니다.
	 * 
	 * @param items
	 * @return
	 */
	
	private ArrayList<String> distinctTimer(ArrayList<String> items) {
		ArrayList<String> strItems = items;
		HashSet<String> hs = new HashSet<String>(strItems);
		
		Iterator<String> it = hs.iterator();
		strItems.clear();
		
		while(it.hasNext()) {
			strItems.add(it.next());
		}
		
		return strItems;
	}
	
	/**
	 * 최종 결과 값인 시|분|초 의 Delimeter 값인 '|' 을 제거한 뒤, TimerObject 객체 형태로 반환합니다. 
	 * 
	 * @param item
	 * @return
	 */
	
	private StopTimer getSplitDelimeter(String item) {
		char[] charArray = item.toCharArray();
		int charLength = charArray.length;
		int tempIndex = 0;
		int count = 0;
		
		StringBuffer strBuf = new StringBuffer();
		
		StopTimer timer = new StopTimer("", STimer.TIMER_TYPE_GENERAL_TIMER);
		
		// 구분자 제거
		for(int i=0; i < charLength; i++) {
			if(charArray[i] == DELIMETER.charAt(0)) {
				for(int j=tempIndex; j < i; j++) {
					strBuf.append(charArray[j]);
				}
				
				switch(count) {
					case 0:
						timer.setHour(Integer.parseInt(strBuf.toString()));						
						break;
					case 1:
						timer.setMin(Integer.parseInt(strBuf.toString()));
						break;
					case 2:
						timer.setSec(Integer.parseInt(strBuf.toString()));
						break;
				}
				
				count++;
				tempIndex = i+1;
				initStringBuffer(strBuf);
			}
		}
		
		return timer;
	}
	
	/**
	 * Raw 음성을 시간, 분, 초로 자른 뒤 시|분|초 의 String 형태로 반환합니다.
	 * 
	 * @param voice
	 * @param strBuf
	 * @return
	 */
	
	private String splitVoiceTimer(String voice, StringBuffer strBuf) {
		char[] charArray = voice.toCharArray();
		int charLength = charArray.length;
		int tempIndex = 0;
		
		String strHour = null, strMin = null, strSec = null;
		boolean isHourDigit = false, isMinDigit = false, isSecDigit = false;
		
		for(int i=0; i < charLength; i++) {
			if(charArray[i] == '시' && charArray[i+1] == '간') {
				int hourDigit = 0;
				
				for(int j=0; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						hourDigit += 1;
					}
					
					strBuf.append(charArray[j]);
				}
				
				if(hourDigit == 0) {
					isHourDigit = true;
				} else {
					strBuf.append(DELIMETER);
				}
				
				strHour = strBuf.toString();
				initStringBuffer(strBuf);
				
				tempIndex = (i+1)+1;
			}
			
			if(charArray[i] == '분') {
				int minDigit = 0;
				
				for(int j=tempIndex; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						minDigit += 1;
					}
					
					strBuf.append(charArray[j]);
				}
				
				if(minDigit == 0) {
					isMinDigit = true;
				} else {
					strBuf.append(DELIMETER);
				}
				
				strMin = strBuf.toString();
				initStringBuffer(strBuf);
				
				tempIndex = i+1;
			}
			
			if(charArray[i] == '초') {
				int secDigit = 0;
				
				for(int j=tempIndex; j < i; j++) {
					if(!Character.isDigit(charArray[j])) {
						secDigit += 1;
					}
					
					strBuf.append(charArray[j]);
				}
				
				if(secDigit == 0) {
					isSecDigit = true;
				} else {
					strBuf.append(DELIMETER);
				}
				
				strSec = strBuf.toString();
				initStringBuffer(strBuf);

				tempIndex = i+1;
			}
		}
		
		if(Constants.DEVELOPE_MODE) {
			Log.d(TAG, "String value : " + strHour + "시간 " + strMin + "분 " + strSec + "초");
//			Log.d(TAG, "Digit Type : " + isHourDigit + ", " + isMinDigit + ", " + isMinDigit);
		}
		
		// 플래그와 값을 넘겨서 숫자 타입으로 전부 바꾼다.
		int hour = 0, min = 0, sec = 0;
		
		if(strHour != null) {
			if(!isHourDigit) {
				hour = hourToInteger(strHour);
			} else {
				hour = Integer.parseInt(strHour);
			}
		}
		
		if(strMin != null) {
			if(!isMinDigit) {
				min = minToInteger(strMin);
			} else {
				min = Integer.parseInt(strMin);
			}
		}
	
		if(strSec != null) {
			if(!isSecDigit) {
				sec = secToInteger(strSec);
			} else {
				sec = Integer.parseInt(strSec);
			}
		}

		if(Constants.DEVELOPE_MODE) {
			Log.d(TAG, "Integer value : " + hour + "시간 " + min + "분 " + sec + "초");
		}

		// 조건 검사
		if(hour != 0 || min != 0 || sec != 0) {
			if((hour >= 0) && (min >= 0 && min <= 59) && (sec >= 0 && sec <= 59)) {
				return hour + DELIMETER + min + DELIMETER + sec + DELIMETER;
			}
		}
		
		return null;
	}
	
	/**
	 * String 형태의 시간을 Integer 로 변환하여 반환합니다.
	 * 
	 * @param strHour
	 * @return
	 */
	
	public int hourToInteger(String strHour) {
		int hour = 0;
		char[] charArray = strHour.toCharArray();

		int charSize = charArray.length;
		
		for(int i=0; i < charSize; i++) {
			if((charArray[i] == '한' || charArray[i] == '일') 
					&& (charArray[i+1] != '흔' && charArray[i+1] != '곱')) {
				hour += 1;
			}
			
			if((charArray[i] == '두' || charArray[i] == '이') && 
					(charArray[i+1] != '른' && charArray[i+1] != '은' && charArray[i+1] != '런' && charArray[i+1] != '든')) {
				hour += 2;
			}
			
			if(charArray[i] == '세' || charArray[i] == '삼') {
				hour += 3;
			}
			
			if(charArray[i] == '네' || charArray[i] == '사') {
				hour += 4;
			} 
			
			if((charArray[i] == '다' && charArray[i+1] == '섯') || (charArray[i] == '오')) {
				hour += 5;
			} 
			
			if((charArray[i] == '여' && charArray[i+1] == '섯') || (charArray[i] == '육')) {
				hour += 6;
			} 
			
			if((charArray[i] == '일' && charArray[i+1] == '곱') || (charArray[i] == '칠')) {
				hour += 7;
			} 
			
			if((charArray[i] == '여' && (charArray[i+1] == '덟' || charArray[i+1] == '덜') 
					&& (charArray[i+1] != '든')) || (charArray[i] == '팔')) { 
				hour += 8;
			} 
			
			if((charArray[i] == '아' && charArray[i+1] == '홉') || charArray[i] == '구') {
				hour += 9;
			} 
			
			if(charArray[i] == '열' || charArray[i] == '십') {
				if(hour != 0) {
					hour *= 10;
				} else {
					hour += 10;
				}
			} 
			
			if(charArray[i] == '스' && charArray[i+1] == '물') {
				hour += 20;
			} 
			
			if(charArray[i] == '서' && charArray[i+1] == '른') {
				hour += 30;
			} 
			
			if(charArray[i] == '마' && charArray[i+1] == '흔') {
				hour += 40;
			} 
			
			if(charArray[i] == '쉰' || charArray[i] == '신') {
				hour += 50;
			} 
			
			if((charArray[i] == '예' || charArray[i] == '애') && 
					(charArray[i+1] == '순' || charArray[i+1] == '상' || charArray[i+1] == '산' ||
					 charArray[i+1] == '손' || charArray[i+1] == '순' || charArray[i+1] == '슨')) {
				hour += 60;
			} 
			
			if((charArray[i] == '일' || charArray[i] == '이') && 
					(charArray[i+1] == '른' || charArray[i+1] == '은' || 
							charArray[i+1] == '런' || charArray[i+1] == '흔' || charArray[i+1] == '든')) {
				hour += 70;
			} 
			
			if(charArray[i] == '여' && charArray[i+1] == '든') {
				hour += 80;
			} 
			
			if(charArray[i] == '아' && charArray[i+1] == '흔') {
				hour += 90;
			}
		}
		
		return hour;
	}
	
	/**
	 * String 형태의 분을 Integer 로 변환하여 반환합니다.
	 * 
	 * @param strMin
	 * @return
	 */
	
	private int minToInteger(String strMin) {
		int min = 0;
		char[] charArray = strMin.toCharArray();
		
		int charSize = charArray.length;
		for(int i=0; i < charSize; i++) {
			if(charArray[i] == '일') {
				min += 1;
			} else if(charArray[i] == '이') {
				min += 2;
			} else if(charArray[i] == '삼') {
				min += 3;
			} else if(charArray[i] == '사') {
				min += 4;
			} else if(charArray[i] == '오') {
				min += 5;
			} else if(charArray[i] == '육') {
				min += 6;
			} else if(charArray[i] == '칠') {
				min += 7;
			} else if(charArray[i] == '팔') {
				min += 8;
			} else if(charArray[i] == '구') {
				min += 9;
			} else if(charArray[i] == '십') {
				if(min != 0) {
					min *= 10;
				} else {
					min += 10;
				}
			}			
		}
		
		return min;
	}
	
	
	/**
	 * String 형태의 초를 Integer 로 변환하여 반환합니다.
	 * 
	 * @param strSec
	 * @return
	 */
	
	private int secToInteger(String strSec) {
		return minToInteger(strSec);
	}
	
	
	private void initStringBuffer(StringBuffer strBuf) {
		strBuf.delete(0, strBuf.length());
		strBuf.setLength(0);
	}
}




