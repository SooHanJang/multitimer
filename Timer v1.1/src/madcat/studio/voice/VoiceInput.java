package madcat.studio.voice;

import java.util.ArrayList;
import java.util.Locale;

import madcat.studio.constants.Constants;
import madcat.studio.data.StopTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.timer.R;
import madcat.studio.timer.STimerList;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

public class VoiceInput extends Activity {
	
	private final String TAG												=	"VoiceInput";
	private final boolean DEVELOPE_MODE										=	Constants.DEVELOPE_MODE;
	
	private final int MAX_VOICE_RESULT										=	10;
	private final int VOICE_RESULT_REQUEST_CODE								=	15;
	
	
	private Context mContext;
	private ArrayList<StopTimer> mStopTimerArray;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.mContext = this;
		
		inputVoice();
	}
	
	private void inputVoice() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "inputVoice 호출");
		}
		
		Intent voiceIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
		voiceIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
		voiceIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.dialog_title_voice_input_prompt));
		voiceIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, MAX_VOICE_RESULT);
		voiceIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
		
		startActivityForResult(voiceIntent, VOICE_RESULT_REQUEST_CODE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "requestCode : " + requestCode + ", resultCode : " + resultCode + ", data : " + data);
		}
		
		if(requestCode == VOICE_RESULT_REQUEST_CODE) {
			if (resultCode == RESULT_OK) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, "onActivityResult 호출");
				}
				
				// 출력된 결과 값을 타이머 음성 인식 파싱 프로세서로 보내 처리하도록 한다.
				
				ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

				VoiceParser voiceParser = VoiceParser.getInstance();
				mStopTimerArray = voiceParser.voiceParsing(mContext, results);
				
				if(mStopTimerArray != null) {
					if(mStopTimerArray.size() != 0) {
						CharSequence[] items = new CharSequence[mStopTimerArray.size()];
						
						int timerArraySize = mStopTimerArray.size();
						switch(voiceParser.getTimerType()) {
							case VoiceParser.TIMER_TYPE_IS_NEW_TIMER:
								for(int i=0; i < timerArraySize; i++) {
					    			String convertFormat = Utils.timeToString(mStopTimerArray.get(i));
					    			items[i] = convertFormat;
					    		}
								
								break;
							case VoiceParser.TIMER_TYPE_IS_SCHEDULE_TIMER:
								for(int i=0; i < timerArraySize; i++) {
									String convertFormat = mStopTimerArray.get(i).getName() + " (" + Utils.timeToString(mStopTimerArray.get(i)) + ")";
									items[i] = convertFormat;
								}
								
								
								break;
						}
						
			    		
						
			    		AlertDialog.Builder builder = new Builder(mContext);
			    		builder.setTitle(getString(R.string.dialog_title_select_timer));
						builder.setItems(items, new OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								// Click 후 이벤트 처리
								
								if(DEVELOPE_MODE) {
									Log.d(TAG, "click value : " + mStopTimerArray.get(which).getHour() + "시간 " + 
											mStopTimerArray.get(which).getMin() + "분 " + mStopTimerArray.get(which).getSec() + "초");
								}

								StopTimer clickTimer = mStopTimerArray.get(which);
								
								// 쓰레드에 등록할 값을 로드
								ArrayList<TimerScheduleList> scheduleArray = clickTimer.getScheduleTimerList();
								String timerName = clickTimer.getName();
								
								if(timerName == null) {
									timerName = "";
								} 
								
								// 쓰레드에 등록
								STimerManage timerManage = STimerManage.getInstance();
								timerManage.addStopTimerThread(mContext, clickTimer.getType(), timerName, clickTimer.getHour(), clickTimer.getMin(), clickTimer.getSec(), 
										clickTimer.getAlarmType(), scheduleArray);
								timerManage.startTimerThread(timerManage.getLastKey());
								
								Intent timerListIntent = new Intent(VoiceInput.this, STimerList.class);
								startActivity(timerListIntent);
								overridePendingTransition(0, 0);
								
							}
						});
			    		
			    		AlertDialog dialogShowTimer = builder.create();
			    		dialogShowTimer.show();
			    		dialogShowTimer.setOnDismissListener(new OnDismissListener() {
							public void onDismiss(DialogInterface dialog) {
								finish();
							}
						});
			    		
					} else {
						Toast.makeText(mContext, getString(R.string.toast_wrong_input_voice), Toast.LENGTH_SHORT).show();
						finish();
					}
				} else {
					Toast.makeText(mContext, getString(R.string.toast_wrong_input_voice), Toast.LENGTH_SHORT).show();
					finish();
				}
			} else if(resultCode == RESULT_CANCELED) {
				finish();
			}
		}
		
		super.onActivityResult(requestCode, resultCode, data);
	}
}











