package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.timer.R;
import madcat.studio.utils.Utils;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class SChooseThemeDialog extends Dialog implements OnClickListener, OnItemClickListener{

	private final String TAG										=	"SChooseTheme";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private SharedPreferences mConfigPref;
	
	private ListView mThemeList;
	private Button mBtnOk, mBtnCancel;
	private TextView mTextHour, mTextMin, mTextSec, mTextMSec;
	
	private int mThemeIndex;
	
	
	public SChooseThemeDialog(Context context) {
		super(context);
		this.mContext = context;
		this.mConfigPref = mContext.getSharedPreferences(Constants.CONFIG_PREF_NAME, Activity.MODE_PRIVATE);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(madcat.studio.timer.R.layout.dialog_choose_theme);
		
		// 다이얼로그 제목 설정
		setTitle(mContext.getString(R.string.dialog_title_manage_theme));
		
		// Resource Id 설정
		mThemeList = (ListView)findViewById(android.R.id.list);
		mBtnOk = (Button)findViewById(R.id.dialog_choose_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_choose_btn_cancle);
		mTextHour = (TextView)findViewById(R.id.dialog_choose_text_hour);
		mTextMin = (TextView)findViewById(R.id.dialog_choose_text_min);
		mTextSec = (TextView)findViewById(R.id.dialog_choose_text_sec);
		mTextMSec = (TextView)findViewById(R.id.dialog_choose_text_msec);
		
		// Adapter 설정
		String[] themeItems = { mContext.getString(R.string.timer_theme_black), mContext.getString(R.string.timer_theme_blue),
				mContext.getString(R.string.timer_theme_brown), mContext.getString(R.string.timer_theme_white),
				mContext.getString(R.string.timer_theme_yellow_green), mContext.getString(R.string.timer_theme_purple),
				mContext.getString(R.string.timer_theme_pink)};
		
		mThemeList.setAdapter(new ArrayAdapter<String>(mContext, R.layout.dialog_choose_theme_list_item, android.R.id.text1, themeItems));
		mThemeList.setItemsCanFocus(true);
		mThemeList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		mThemeList.setOnItemClickListener(this);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 기본 값 불러오기
		mThemeIndex = mConfigPref.getInt(Constants.PREF_CHOOSE_THEME_INDEX, 0);
		mThemeList.setItemChecked(mThemeIndex, true);
		Utils.setDisplayTheme(mThemeIndex, mTextHour, mTextMin, mTextSec, mTextMSec);
		
	}
	
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Utils.setDisplayTheme(position, mTextHour, mTextMin, mTextSec, mTextMSec);
		mThemeIndex = position;
	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.dialog_choose_btn_ok:
			SharedPreferences.Editor editor = mConfigPref.edit();
			editor.putInt(Constants.PREF_CHOOSE_THEME_INDEX, mThemeIndex);
			editor.commit();
			
			dismiss();
			break;
		
		case R.id.dialog_choose_btn_cancle:
			dismiss();
			break;
		}
	}
	

}
