package madcat.studio.dialog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import madcat.studio.adapter.AdapterTimerScheduleList;
import madcat.studio.constants.Constants;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.timer.R;
import madcat.studio.timer.STimerCategoryList;
import madcat.studio.timer.STimerScheduleTimerList;
import madcat.studio.utils.SlideButton;
import madcat.studio.utils.SlideButton.OnCheckChangedListner;
import madcat.studio.utils.Utils;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class STimerScheduleDialog extends ListActivity implements OnClickListener, OnCheckChangedListner, OnItemSelectedListener {

	private final String TAG										=	"TimerDialogActivity";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int SCHEDULE_OFF							=	0;
	public static final int SCHEDULE_ON								=	1;
	
	public static final int ADD_TIMER								=	0;
	public static final int MODIFY_TIMER							=	1;
	
	private Context mContext;
	
	private int mTimerDialogMode;
	private String mGetModifyTimerName;
	
	private Button mBtnOk, mBtnCancel, mBtnTimeSet;
	private Button mBtnAddScheduleTimer;
	
	private EditText mEditTimerName;
	private TextView mTextTitle, mTextSetTime, mTextHour, mTextMin, mTextSec;
	
	private Spinner mSpinCategory, mSpinAlarmType;
	
	private SlideButton mSlideTimerSchedule;
	
	private AdapterTimerScheduleList mAdapter;
	private ArrayList<TimerScheduleList> mItems;
	private ArrayList<TimerScheduleList> mCopyItems;
	
	// 스케쥴 관련 변수
	private int mScheduleFlag										=	SCHEDULE_OFF;
	
	// 시간 설정 관련 변수
	private int mHour, mMin, mSec;
	private int mAlarmType;
	
	// 정렬 관련 변수
	private AscendingComparator mAscendingComparator;
	
	// 카테고리 관련 변수
	private String[] mCategory;
	private String mSelectedCategory;
	
	// 타이머 로드 관련 변수
	private String mLoadTimerTitle;
	private String mLoadTimerCategory;
	
	// 보존 전달 변수
	private String mSaveSearchName;
	private int mSaveMode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_schedule_timer);
		this.mContext = this; 
		this.getIntenter();
		
		mItems = new ArrayList<TimerScheduleList>();
		mCopyItems = new ArrayList<TimerScheduleList>();
		mAdapter = new AdapterTimerScheduleList(mContext, R.layout.timer_schedule_list_row, mItems);
		
		LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View header = inflater.inflate(R.layout.dialog_schedule_timer_header, null);
		
		this.getListView().addHeaderView(header);
		setListAdapter(mAdapter);
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "현재 아이템 상태 : " + mItems.size());
		}
		
		// Resource Id 설정
		mBtnOk = (Button)findViewById(R.id.dialog_schedule_timer_btn_ok);
		mBtnCancel= (Button)findViewById(R.id.dialog_schedule_timer_btn_cancel);
		
		mBtnAddScheduleTimer = (Button)header.findViewById(R.id.dialog_schedule_timer_add_btn);
		mBtnTimeSet = (Button)header.findViewById(R.id.dialog_schedule_timer_btn_set_time);
		
		mEditTimerName = (EditText)header.findViewById(R.id.dialog_schedule_timer_input_timer_name);
		mTextTitle = (TextView)header.findViewById(R.id.dialog_schedule_timer_title);
		mTextSetTime = (TextView)header.findViewById(R.id.dialog_schedule_timer_set_text_time);
		mSpinCategory = (Spinner)header.findViewById(R.id.dialog_schedule_timer_spin_select_category);
		mSpinAlarmType = (Spinner)header.findViewById(R.id.dialog_schedule_timer_spin_alarm_type);
		mSlideTimerSchedule = (SlideButton)header.findViewById(R.id.dialog_schedule_timer_slide_btn);
		
		// 카테고리 Spinner 설정
		mCategory = Utils.getCategoryListToString(mContext);
		ArrayAdapter<String> spinCategoryAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, mCategory);
		spinCategoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinCategory.setAdapter(spinCategoryAdapter);
		
		// 알람 타입 Spinner 설정
		ArrayAdapter alarmSpinnerApapter = ArrayAdapter.createFromResource(mContext, R.array.spin_alarm_type, android.R.layout.simple_spinner_item);
		alarmSpinnerApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinAlarmType.setAdapter(alarmSpinnerApapter);
		
		// Array Compare 초기화
		mAscendingComparator = new AscendingComparator();
		
		// Event Listener 설정
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		mBtnAddScheduleTimer.setOnClickListener(this);
		mBtnTimeSet.setOnClickListener(this);
		
		mSpinCategory.setOnItemSelectedListener(this);
		mSpinAlarmType.setOnItemSelectedListener(this);
		
		mSlideTimerSchedule.setOnCheckChangedListner(this);
		
		switch(mTimerDialogMode) {
			case ADD_TIMER:					//	타이머를 추가하려는 경우의 호출 상태
				mTextTitle.setText(mContext.getString(R.string.dialog_title_timer_add));
				
				mSlideTimerSchedule.setChecked(true);
				mScheduleFlag = SCHEDULE_ON;
				
			
				break;
			case MODIFY_TIMER:				//	타이머를 수정하려고 하는 경우의 호출 상태
				mTextTitle.setText(mContext.getString(R.string.dialog_title_timer_modify));
				
				// 타이머 값 로드
				
				SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
						null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
				
				String timerSql = "SELECT * FROM " + Constants.TABLE_TIMER + " WHERE " + Constants.TIMER_NAME + "='" + mGetModifyTimerName + "'";
				Cursor timerCursor = sdb.rawQuery(timerSql, null);
				
				timerCursor.moveToFirst();
				
				mLoadTimerTitle = timerCursor.getString(2);
				
				mEditTimerName.setText(mLoadTimerTitle);
				mHour = timerCursor.getInt(3);
				mMin = timerCursor.getInt(4);
				mSec = timerCursor.getInt(5);

				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));

				int categorySize = mCategory.length;
				for(int i=0; i < categorySize; i++) {
					if(mCategory[i].equals(timerCursor.getString(1))) {
						mLoadTimerCategory = mCategory[i];
						mSpinCategory.setSelection(i);
						break;
					}
				}
				
				mSpinAlarmType.setSelection(timerCursor.getInt(6));
				
				// On Off 가 거꿀로 저장된 문제 해결 할 것, Adapter 조금 이상하지 않나? 확인할것
				
				mScheduleFlag = timerCursor.getInt(7);
				switch(mScheduleFlag) {
					case SCHEDULE_OFF:
						mSlideTimerSchedule.setChecked(false);
						break;
					case SCHEDULE_ON:
						mSlideTimerSchedule.setChecked(true);
						break;
				}
				
				mItems = (ArrayList<TimerScheduleList>) Utils.deSerializeObject(timerCursor.getBlob(8));
				
				mAdapter = new AdapterTimerScheduleList(mContext, R.layout.timer_schedule_list_row, mItems);
				setListAdapter(mAdapter);
				
				
				if(DEVELOPE_MODE) {
					if(mItems != null) {
						Log.d(TAG, "복구한 스케쥴 사이즈 : " + mItems.size());
					}
				}
				
				timerCursor.close();
				sdb.close();
			
				break;
		}
	}
	
	private void startScheduleTimerList() {
		Intent intent = null;
		
		switch(mTimerDialogMode) {
			case ADD_TIMER:
				intent = new Intent(STimerScheduleDialog.this, STimerCategoryList.class);
				break;
			case MODIFY_TIMER:
				intent = new Intent(STimerScheduleDialog.this, STimerScheduleTimerList.class);
				intent.putExtra(Constants.PUT_TIMER_LIST_MODE, mSaveMode);
				intent.putExtra(Constants.PUT_TIMER_CATEGORY, mLoadTimerCategory);
				intent.putExtra(Constants.PUT_TIMER_SEARCH_TIMER_NAME, mSaveSearchName);
				break;
		}
		
		startActivity(intent);
		finish();
		overridePendingTransition(0, 0);
	}
	
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_schedule_timer_btn_set_time:
				final STimePickDialog timePickDialog = new STimePickDialog(mContext);
				timePickDialog.setTimePickHour(mHour);
				timePickDialog.setTimePickMin(mMin);
				timePickDialog.setTimePickSec(mSec);
				timePickDialog.show();
				
				timePickDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(timePickDialog.getDialogState()) {
							mHour = timePickDialog.getTimePickHour();
							mMin = timePickDialog.getTimePickMin();
							mSec = timePickDialog.getTimePickSec();
							
							mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
									+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
									+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
						}
					}
				});
				
				
				break;
		
		
		
			// 스케쥴 타이머 확인, 취소 관련 변수	
			case R.id.dialog_schedule_timer_btn_ok:
				
				// 데이터베이스에 값을 저장한 뒤, TimerScheduleList 호출
				String timerTitle = mEditTimerName.getText().toString().trim();

				// 무결성 검사
				if(timerTitle.length() != 0) {
					int scheduleSize = mItems.size();
					int totalTime = (mHour * 3600) + (mMin * 60) + mSec;
					boolean exceedFlag = false;
					
					for(int i=0; i < scheduleSize; i++) {
						if(totalTime < mItems.get(i).getTotalSecTime()) {
							exceedFlag = true;
							break;
						}
					}
					
					if(!exceedFlag) {
						// DB 에 삽입
						if(DEVELOPE_MODE) {
							Log.d(TAG, "삽입 데이터 : " + mSelectedCategory + ", " + timerTitle + ", " + 
									mHour + ", " + mMin + ", " + mSec + ", " + mAlarmType + ", " + mScheduleFlag);
							Log.d(TAG, "스케쥴 리스트가 비어있는가? : " + mItems.isEmpty());
						}
						
						SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
								null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
						
						byte[] objectBytes = Utils.serializeObject(mItems);
						
						if(mItems.isEmpty()) {
							mScheduleFlag = SCHEDULE_OFF;
						}
						
						// Timer 테이블에 삽입
						ContentValues values = new ContentValues();
						values.put(Constants.TIMER_CATEGORY, mSelectedCategory);
						values.put(Constants.TIMER_NAME, timerTitle);
						values.put(Constants.TIMER_HOUR, mHour);
						values.put(Constants.TIMER_MIN, mMin);
						values.put(Constants.TIMER_SEC, mSec);
						values.put(Constants.TIMER_ALARM_TYPE, mAlarmType);
						values.put(Constants.TIMER_ARRAY_FLAG, mScheduleFlag);
						values.put(Constants.TIMER_ARRAY, objectBytes);
						
						switch(mTimerDialogMode) {
							case ADD_TIMER:
								if(!Utils.duplicationTimerCheck(sdb, timerTitle)) {
									sdb.insert(Constants.TABLE_TIMER, null, values);
									
									// Category 테이블에 카테고리 사이즈 값을 증가시켜 삽입
//									Utils.modifyCategorySize(sdb, values, Utils.ADD_CATEGORY_SIZE, mSelectedCategory);
									
									startScheduleTimerList();
								} else {
									Toast.makeText(mContext, getString(R.string.toast_duplicate_timer), Toast.LENGTH_SHORT).show();
								}
								
								break;
							
							case MODIFY_TIMER:
								if(!timerTitle.equals(mLoadTimerTitle)) {
									if(!Utils.duplicationTimerCheck(sdb, timerTitle)) {
										sdb.update(Constants.TABLE_TIMER, values, Constants.TIMER_NAME + "='" + mLoadTimerTitle + "'", null);
										
										startScheduleTimerList();
									} else {
										Toast.makeText(mContext, getString(R.string.toast_duplicate_timer), Toast.LENGTH_SHORT).show();
									}
								} else {
									sdb.update(Constants.TABLE_TIMER, values, Constants.TIMER_NAME + "='" + mLoadTimerTitle + "'", null);
									
									startScheduleTimerList();
								}
								
								break;
						}
						
						sdb.close();	
						
					} else {
						Toast.makeText(mContext, getString(R.string.toast_integrity_exceed_time), Toast.LENGTH_SHORT).show();
					}
				}
				break;
			case R.id.dialog_schedule_timer_btn_cancel:
				
				// TimerScheduleList 호출
				
				startScheduleTimerList();
				break;
				
			// 스케쥴 추가 버튼
			case R.id.dialog_schedule_timer_add_btn:
				final STimerScheduleSubDialog subDialog = new STimerScheduleSubDialog(
						mContext, mItems, mHour, mMin, mSec, STimerScheduleSubDialog.MODE_ADD_SCHEDULE);
				subDialog.show();
				
				subDialog.setOnDismissListener(new OnDismissListener() {
					public void onDismiss(DialogInterface dialog) {
						if(subDialog.getDialogStateFlag()) {
							// ArrayAdapter 의 item 에 값을 추가 한 뒤, Adapter 갱신
							
							mItems.add(subDialog.getScheduleTimer());

							Collections.sort(mItems, mAscendingComparator);
							
							
							mAdapter.notifyDataSetChanged();
							
							
						}
					}
				});
				
				break;
		}
	}
	
	// 스케쥴 타이머 이벤트 함수
	public void onCheckChanged(View v, boolean isChecked) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "스케쥴 타이머가 " + isChecked);
		}
		
		if(isChecked) {			//	스케쥴 설정이 On 인 경우
			mScheduleFlag = SCHEDULE_ON;
			
			mItems.clear();
			mItems.addAll(mCopyItems);
			mAdapter.notifyDataSetChanged();
			
			
			mBtnAddScheduleTimer.setVisibility(View.VISIBLE);

			
		} else {				//	스케쥴 설정이 Off 인 경우
			mScheduleFlag = SCHEDULE_OFF;
			
			mCopyItems.clear();
			mCopyItems.addAll(mItems);
			
			mItems.clear();
			
			mAdapter.notifyDataSetChanged();
			mBtnAddScheduleTimer.setVisibility(View.GONE);
		}
	}
	
	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		if(parent == mSpinAlarmType) {
			switch(position) {
				case TimerScheduleList.MODE_ALARM:
					mAlarmType = TimerScheduleList.MODE_ALARM;
					break;
				case TimerScheduleList.MODE_SHAKE:
					mAlarmType = TimerScheduleList.MODE_SHAKE;
					break;
				case TimerScheduleList.MODE_MUTE:
					mAlarmType = TimerScheduleList.MODE_MUTE;
					break;
				case TimerScheduleList.MODE_ALARM_SHAKE:
					mAlarmType = TimerScheduleList.MODE_ALARM_SHAKE;
					break;
			}
		} else if(parent == mSpinCategory) {
			mSelectedCategory = mCategory[position];
		}
	}


	public void onNothingSelected(AdapterView<?> arg0) {}
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, position + "번째 아이템이 클릭되었습니다.");
		}
		
		final int itemIndex = position -1;
		
		// 스케쥴 수정 다이얼로그 출력
		final STimerScheduleSubDialog subDialog = new STimerScheduleSubDialog(mContext, mItems, mHour, mMin, mSec, STimerScheduleSubDialog.MODE_MODIFY_SCHEDULE);
		subDialog.loadScheduleTimer(mItems.get(itemIndex));
		subDialog.show();
		
		subDialog.setOnDismissListener(new OnDismissListener() {
			public void onDismiss(DialogInterface dialog) {
				if(subDialog.getDialogStateFlag()) {
					mItems.set(itemIndex, subDialog.getScheduleTimer());
					Collections.sort(mItems, mAscendingComparator);
					
					mAdapter.notifyDataSetChanged();
				}
			}
		});
		
		
		super.onListItemClick(l, v, position, id);
	}
	
	
	private void getIntenter() {
		Intent intent = this.getIntent();
		
		if(intent != null) {
			mTimerDialogMode = intent.getExtras().getInt(Constants.PUT_TIMER_DIALOG_MODE);

			switch(mTimerDialogMode) {
				case MODIFY_TIMER:
					mGetModifyTimerName = intent.getExtras().getString(Constants.PUT_TIMER_DIALOG_MODIFY_NAME);
					mSaveMode = intent.getExtras().getInt(Constants.PUT_TIMER_LIST_MODE);
					mSaveSearchName = intent.getExtras().getString(Constants.PUT_TIMER_SEARCH_TIMER_NAME);
					break;
			}
		}
	}
	
	@Override
	public void onBackPressed() {}
	
	
	class AscendingComparator implements Comparator {

		public int compare(Object obj1, Object obj2) {
			int by1 = ((TimerScheduleList)obj1).getTotalSecTime();
			int by2 = ((TimerScheduleList)obj2).getTotalSecTime(); 
			
			return by1 > by2 ? -1 : (by1 == by2 ? 0 : 1);
		}
		
	}

}
