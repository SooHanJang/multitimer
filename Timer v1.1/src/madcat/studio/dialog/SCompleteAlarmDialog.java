package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.service.AlarmWakeLockService;
import madcat.studio.service.TimerAlarmService;
import madcat.studio.timer.R;
import madcat.studio.utils.MessagePool;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SCompleteAlarmDialog extends Activity implements OnClickListener {
	
	private final String TAG										=	"SCompleteAlarmDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private Button mBtnOk;
	private TextView mTextTitle;
	
	private AlarmWakeLockService mAlarmWakeLockService;
	
	private TimerAlarmService mTimerAlarmService;
	private int mAlarmType											=	ScheduleTimer.MODE_ALARM;
	private String mAlarmName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onCreate 호출");
		}
		
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
		
		setContentView(R.layout.dialog_complete_alarm);
		this.mContext = this;
		this.getIntenter();
		this.mTimerAlarmService = TimerAlarmService.getInstance();
		this.mTimerAlarmService.setContext(mContext);
		this.mTimerAlarmService.initAlarm();
		
		mBtnOk = (Button)findViewById(R.id.dialog_complete_alarm_btn_ok);
		mTextTitle = (TextView)findViewById(R.id.dialog_complete_alarm_title);
		
		mBtnOk.setOnClickListener(this);

		if(mAlarmName.length() != 0) {
			mTextTitle.setText(mAlarmName);
			mTextTitle.setVisibility(View.VISIBLE);
		} else {
			mTextTitle.setVisibility(View.GONE);
		}
		
		mTimerAlarmService.playAlarm(mAlarmType);
	}
	
	@Override
	protected void onResume() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onResume 호출");
		}
		
		overridePendingTransition(android.R.anim.fade_in, R.anim.hold);
		
		super.onResume();
	}

	@Override
	protected void onPause() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onPause 호출");
		}

//		mTimerAlarmService.stopAllAlarm();
//		mAlarmWakeLockService.releaseWakeLock();
//		finish();
		
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onStop 호출");
		}
		
		mTimerAlarmService.stopAllAlarm();
		finish();
		
		super.onStop();
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_complete_alarm_btn_ok:
				mTimerAlarmService.stopAllAlarm();
				mAlarmWakeLockService.releaseWakeLock();
				finish();
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				break;
		}
	}
	
	private void getIntenter() {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "getIntenter 호출");
		}
		
		Intent intent = this.getIntent();
		
		if(intent != null) {
			mAlarmName = intent.getExtras().getString(Constants.PUT_TIMER_COMPELETE_ALARM_NAME);
			mAlarmType = intent.getExtras().getInt(Constants.PUT_TIMER_COMPELETE_ALARM_TYPE);
		}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		
		mTimerAlarmService.initAlarm();
		
		if(intent != null) {
			mAlarmName = intent.getExtras().getString(Constants.PUT_TIMER_COMPELETE_ALARM_NAME);
			mAlarmType = intent.getExtras().getInt(Constants.PUT_TIMER_COMPELETE_ALARM_TYPE);
			
			if(mAlarmName.length() != 0) {
				mTextTitle.setText(mAlarmName);
				mTextTitle.setVisibility(View.VISIBLE);
			} else {
				mTextTitle.setVisibility(View.GONE);
			}
			
			mTimerAlarmService.playAlarm(mAlarmType);
		}
		
		if(DEVELOPE_MODE) {
			Log.d(TAG, "onNewIntent 호출");
			Log.d(TAG, "새로 넘어온 alarmType");
		}
	}
	
	@Override
	public void onBackPressed() {}
}
