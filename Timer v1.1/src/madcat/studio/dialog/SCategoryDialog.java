package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.timer.R;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class SCategoryDialog extends Dialog implements OnClickListener {
	
	private final String TAG										=	"CategoryDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int MODE_ADD_CATEGORY								=	1;
	public static final int MODE_MODIFY_CATEGORY							=	2;
	
	private Context mContext;
	
	private EditText mEditCategory;
	private Button mBtnOk, mBtnCancel;
	
	private int mCtrlMode;
	private String mLoadCategory;
	private boolean mDialogFlag												=	false;
	
	
	public SCategoryDialog(Context context) {
		super(context);
		this.mContext = context;
	}
	
	public SCategoryDialog(Context context, int mode) {
		super(context);
		this.mContext = context;
		this.mCtrlMode = mode;
	}
	
	public SCategoryDialog(Context context, int mode, String category) {
		super(context);
		this.mContext = context;
		this.mCtrlMode = mode;
		this.mLoadCategory = category;
	}
	
	public boolean getDialogFlag() {
		return mDialogFlag;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		switch(mCtrlMode) {
			case MODE_ADD_CATEGORY:
				setTitle(mContext.getString(R.string.dialog_title_category_add));
				break;
			case MODE_MODIFY_CATEGORY:
				setTitle(mContext.getString(R.string.dialog_title_category_remove));
				break;
		}
		
		
		setContentView(R.layout.dialog_category);
		
		mEditCategory = (EditText)findViewById(R.id.dialog_category_input);
		mBtnOk = (Button)findViewById(R.id.dialog_category_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_category_btn_cancel);
		
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		// 카테고리 수정 모드일 경우, 선택된 카테고리 값을 입력창에 표시해준다.
		if(mCtrlMode == MODE_MODIFY_CATEGORY) {
			mEditCategory.setText(mLoadCategory);
		}
	}

	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_category_btn_ok:
				
				String insertCategory = mEditCategory.getText().toString().trim();
				SQLiteDatabase sdb = SQLiteDatabase.openDatabase(mContext.getDatabasePath(Constants.DATABASE_NAME).toString(), 
						null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
				
				switch(mCtrlMode) {
					case MODE_ADD_CATEGORY:
						if(mEditCategory.length() != 0) {
							
							// 중복된 카테고리가 존재하지 않으면, 데이터베이스에 추가한다.
							if(!Utils.duplicationCategoryCheck(sdb, insertCategory)) {
								ContentValues insertValues = new ContentValues();
								insertValues.put(Constants.CATEGORY_NAME, insertCategory);
								sdb.insert(Constants.TABLE_CATEGORY, null, insertValues);
								
								// 카테고리가 추가 되었음을 설정
								mDialogFlag = true;
								
								Utils.hideSoftKeyboard(mContext, mEditCategory);
								
								dismiss();
							} else {
								Toast.makeText(mContext, mContext.getString(R.string.toast_duplicate_category), Toast.LENGTH_SHORT).show();
							}
						} 
						
						break;
						
						
					case MODE_MODIFY_CATEGORY:
						if(mEditCategory.length() != 0) {
							if(!mLoadCategory.equals(insertCategory)) {
								
								// 중복된 카테고리가 존재하지 않으면, 데이터베이스에 추가한다.
								if(!Utils.duplicationCategoryCheck(sdb, insertCategory)) {
									// 카테고리 테이블 변경
									ContentValues updateValues = new ContentValues();
									updateValues.put(Constants.CATEGORY_NAME, insertCategory);
									sdb.update(Constants.TABLE_CATEGORY, updateValues, 
											Constants.CATEGORY_NAME + " = '" + mLoadCategory + "'", null);

									// 타이머 테이블 변경
									updateValues.clear();
									updateValues.put(Constants.TIMER_CATEGORY, insertCategory);
									sdb.update(Constants.TABLE_TIMER, updateValues, 
											Constants.TIMER_CATEGORY + " = '" + mLoadCategory + "'", null);

									// 카테고리가 변경 수정 되었음을 설정
									mDialogFlag = true;
									
									Utils.hideSoftKeyboard(mContext, mEditCategory);
									
									dismiss();
									
								} else {
									Toast.makeText(mContext, mContext.getString(R.string.toast_duplicate_timer), Toast.LENGTH_SHORT).show();
								}
							} else {
								mDialogFlag = false;
								dismiss();
							}
						}
						
						break;
				}
				
				sdb.close();
				
				break;
			case R.id.dialog_category_btn_cancel:
				Utils.hideSoftKeyboard(mContext, mEditCategory);
				dismiss();
				break;
		}
	}

}
