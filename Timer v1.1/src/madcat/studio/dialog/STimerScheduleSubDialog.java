package madcat.studio.dialog;

import java.lang.reflect.Modifier;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.timer.R;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class STimerScheduleSubDialog extends Dialog implements OnClickListener, OnItemSelectedListener, OnFocusChangeListener {

	private final String TAG										=	"TimerScheduleDialog";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	public static final int MODE_ADD_SCHEDULE						=	1;
	public static final int MODE_MODIFY_SCHEDULE					=	2;
	
	private Context mContext;

	private EditText mEditScheduleName, mEditHour, mEditMin, mEditSec;
	private TextView mTextSetTime;
	private Button mBtnPlusHour, mBtnMinusHour, mBtnPlusMin, mBtnMinusMin, mBtnPlusSec, mBtnMinusSec;
	private Button mBtnOk, mBtnCancel;
	
	private Spinner mSpinAlarmType;
	
	private String mTitle;
	private int mMode, mAlarmType;
	private int mHour, mMin, mSec, mTotalSecTime;
	private int mMaxHour, mMaxMin, mMaxSec;
	private TimerScheduleList mLoadScheduleTimer;
	
	private boolean mDialogStateFlag								=	false;
	
	// 무결성 체크 관련 변수
	private ArrayList<TimerScheduleList> mTimerScheduleItems;
	
	// TimerWatcher
	private TextWatcher mWatcherHour, mWatcherMin, mWatcherSec;
	
	public STimerScheduleSubDialog(Context context, ArrayList<TimerScheduleList> items, int maxHour, int maxMin, int maxSec, int mode) {
		super(context);
		this.mContext = context;
		this.mTimerScheduleItems = items;
		this.mMaxHour = maxHour;
		this.mMaxMin = maxMin;
		this.mMaxSec = maxSec;
		this.mMode = mode;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_schedule_sub_timer);
		
		switch(mMode) {
			case MODE_ADD_SCHEDULE:
				setTitle(mContext.getString(R.string.dialog_title_timer_schedule_registe));
				break;
			case MODE_MODIFY_SCHEDULE:
				setTitle(mContext.getString(R.string.dialog_title_timer_schedule_modify));
				break;
		}
		

		mEditScheduleName = (EditText)findViewById(R.id.dialog_schedule_sub_timer_name);
		mTextSetTime = (TextView)findViewById(R.id.dialog_schedule_sub_timer_set_text_time);
		mEditHour = (EditText)findViewById(R.id.dialog_schedule_sub_timer_pick_tv_hour);
		mEditMin = (EditText)findViewById(R.id.dialog_schedule_sub_timer_pick_tv_min);
		mEditSec = (EditText)findViewById(R.id.dialog_schedule_sub_timer_pick_tv_sec);
		
		mBtnPlusHour = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_hour_plus);
		mBtnMinusHour = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_hour_minus);
		mBtnPlusMin = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_min_plus);
		mBtnMinusMin = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_min_minus);
		mBtnPlusSec = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_sec_plus);
		mBtnMinusSec = (Button)findViewById(R.id.dialog_schedule_sub_timer_pick_btn_sec_minus);
		
		mBtnOk = (Button)findViewById(R.id.dialog_schedule_sub_timer_btn_ok);
		mBtnCancel = (Button)findViewById(R.id.dialog_schedule_sub_timer_btn_cancel);
		
		mSpinAlarmType = (Spinner)findViewById(R.id.dialog_schedule_sub_timer_spin_alarm_type);
		
		// 알람 타입 Spinner 설정
		ArrayAdapter alarmSpinnerApapter = ArrayAdapter.createFromResource(mContext, R.array.spin_alarm_type, android.R.layout.simple_spinner_item);
		alarmSpinnerApapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		mSpinAlarmType.setAdapter(alarmSpinnerApapter);
		
		// 스케쥴 타이머가 수정 모드일 경우, 데이터를 복원
		switch(mMode) {
			case MODE_MODIFY_SCHEDULE:
				mEditScheduleName.setText(mTitle);
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				mEditHour.setText(String.format("%02d", mHour));
				mEditMin.setText(String.format("%02d", mMin));
				mEditSec.setText(String.format("%02d", mSec));
				mSpinAlarmType.setSelection(mAlarmType);
				break;
		}
		
		
		// Event Listener 설정
		mBtnPlusHour.setOnClickListener(this);
		mBtnMinusHour.setOnClickListener(this);
		mBtnPlusMin.setOnClickListener(this);
		mBtnMinusMin.setOnClickListener(this);
		mBtnPlusSec.setOnClickListener(this);
		mBtnMinusSec.setOnClickListener(this);
		mBtnOk.setOnClickListener(this);
		mBtnCancel.setOnClickListener(this);
		
		mEditHour.setOnFocusChangeListener(this);
		mEditMin.setOnFocusChangeListener(this);
		mEditSec.setOnFocusChangeListener(this);
		
		// TextWatcher 설정
		setTimeWatcherListener();
		
		mSpinAlarmType.setOnItemSelectedListener(this);
		
	}
	
	private void setTimeWatcherListener() {
		mWatcherHour = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int hour = Integer.parseInt(s.toString());
					
					if(hour > 99) {
						mHour = 99;
						mEditHour.setText(String.format("%02d", mHour));
					} else {
						mHour = hour;
					}
				} else {
					mHour = 0;
				}
			}
		};
		
		mWatcherMin = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int min = Integer.parseInt(s.toString());
					
					if(min > 59) {
						mMin = 59;
						mEditMin.setText(String.format("%02d", mMin));
					} else {
						mMin = min;
					}
				} else {
					mMin = 0;
				}
			}
		};
		
		mWatcherSec = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int sec = Integer.parseInt(s.toString());
					
					if(sec > 59) {
						mSec = 59;
						mEditSec.setText(String.format("%02d", mSec));
					} else {
						mSec = sec;
					}
				} else {
					mSec = 0;
				}
			}
		};
		
		mEditHour.addTextChangedListener(mWatcherHour);
		mEditMin.addTextChangedListener(mWatcherMin);
		mEditSec.addTextChangedListener(mWatcherSec);
		
	}
	
	public void loadScheduleTimer(TimerScheduleList scheduleTimer) {
		if(mMode == MODE_MODIFY_SCHEDULE) {
			this.mLoadScheduleTimer = scheduleTimer;
			
			this.mTitle = scheduleTimer.getTitle();
			this.mHour = scheduleTimer.getHour();
			this.mMin = scheduleTimer.getMin();
			this.mSec = scheduleTimer.getSec();
			this.mTotalSecTime = scheduleTimer.getTotalSecTime();
			this.mAlarmType = scheduleTimer.getAlarmType();
		}
	}

	public void onClick(View v) {
		switch(v.getId()) {
		
			// 시간 설정 부분
			case R.id.dialog_schedule_sub_timer_pick_btn_hour_plus:
				mEditHour.requestFocus();
				
				mHour++;
				
				if(mHour > 99) {
					mHour = 0;
				}
				
				mEditHour.setText(String.format("%02d", mHour));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				break;
				
			case R.id.dialog_schedule_sub_timer_pick_btn_hour_minus:
				mEditHour.requestFocus();
				
				mHour--;
				
				if(mHour < 0) {
					mHour = 99;
				}

				mEditHour.setText(String.format("%02d", mHour));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				
				break;
				
			case R.id.dialog_schedule_sub_timer_pick_btn_min_plus:
				mEditMin.requestFocus();
				
				mMin++;
				
				if(mMin >= 60) {
					mMin = 0;
				}
				
				mEditMin.setText(String.format("%02d", mMin));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				break;
				
			case R.id.dialog_schedule_sub_timer_pick_btn_min_minus:
				mEditMin.requestFocus();
				
				mMin--;
				
				if(mMin < 0) {
					mMin = 59;
				}
				
				mEditMin.setText(String.format("%02d", mMin));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				break;
				
			case R.id.dialog_schedule_sub_timer_pick_btn_sec_plus:
				mEditSec.requestFocus();
				
				mSec++;
				
				if(mSec >= 60) {
					mSec = 0;
				}
				
				mEditSec.setText(String.format("%02d", mSec));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				break;
				
			case R.id.dialog_schedule_sub_timer_pick_btn_sec_minus:
				mEditSec.requestFocus();
				
				mSec--;
				
				if(mSec < 0) {
					mSec = 59;
				}
				
				mEditSec.setText(String.format("%02d", mSec));
				mTextSetTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
						+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
				
				break;
		
				
			// 확인, 취소 버튼 부분	
		
			case R.id.dialog_schedule_sub_timer_btn_ok:
				
				if(DEVELOPE_MODE) {
					Log.d(TAG, "설정된 타이머 시간 값 : " + mMaxHour + "시간 " + mMaxMin + "분 " + mMaxSec + "초");
					Log.d(TAG, "스케쥴 시간 값 : " + mHour + "시간 " + mMin + "분 " + mSec + "초");
				}
				
				int maxTime = (mMaxHour * 3600) + (mMaxMin * 60) + mMaxSec;				//	타이머의 총 시간
				mTotalSecTime = (mHour * 3600) + (mMin * 60) + mSec;					//	스케쥴의 총 시간
				
				// 무결성 체크
				
				
				if(mTotalSecTime < maxTime && mEditScheduleName.getText().toString().trim().length() != 0) {
					boolean duplicateCheck = false;
					int scheduleSize = mTimerScheduleItems.size();
					
					switch(mMode) {
						case MODE_ADD_SCHEDULE:
							for(int i=0; i < scheduleSize; i++) {
								if(mTotalSecTime == mTimerScheduleItems.get(i).getTotalSecTime()) {
									duplicateCheck = true;
									break;
								}
							}
							
							break;
						case MODE_MODIFY_SCHEDULE:
							if(mTotalSecTime == mLoadScheduleTimer.getTotalSecTime()) {
								duplicateCheck = false;
								break;
							} else {
								for(int i=0; i < scheduleSize; i++) {
									if(mTotalSecTime == mTimerScheduleItems.get(i).getTotalSecTime()) {
										duplicateCheck = true;
										break;
									}
								}
							}
							
							
							break;
					}
					
					if(!duplicateCheck) {
						mDialogStateFlag = true;
						dismiss();
					} else {
						Toast.makeText(mContext, mContext.getString(R.string.toast_wrong_duplicate_schedule), Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(mContext, mContext.getString(R.string.toast_integrity_schedule_time), Toast.LENGTH_SHORT).show();
				}
				
				
				break;
			case R.id.dialog_schedule_sub_timer_btn_cancel:
				dismiss();
				break;
		}
	}
	
	public TimerScheduleList getScheduleTimer() {
		if(mDialogStateFlag) {
			if(mMode == MODE_ADD_SCHEDULE) {
				mLoadScheduleTimer = new TimerScheduleList();
			}
			
			mLoadScheduleTimer.setTitle(mEditScheduleName.getText().toString());
			mLoadScheduleTimer.setTime(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
					+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
					+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), mHour, mMin, mSec));
			mLoadScheduleTimer.setAlarmType(mAlarmType);
			mLoadScheduleTimer.setHour(mHour);
			mLoadScheduleTimer.setMin(mMin);
			mLoadScheduleTimer.setSec(mSec);
			mLoadScheduleTimer.setTotalSecTime(mTotalSecTime);
			
			return mLoadScheduleTimer;
		} else {
			return null;
		}
	}

	public boolean getDialogStateFlag() {
		return mDialogStateFlag;
	}
	
	@Override
	public void onBackPressed() {}

	public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
		if(parent == mSpinAlarmType) {
			switch(position) {
				case TimerScheduleList.MODE_ALARM:
					mAlarmType = TimerScheduleList.MODE_ALARM;
					break;
				case TimerScheduleList.MODE_SHAKE:
					mAlarmType = TimerScheduleList.MODE_SHAKE;
					break;
				case TimerScheduleList.MODE_MUTE:
					mAlarmType = TimerScheduleList.MODE_MUTE;
					break;
				case TimerScheduleList.MODE_ALARM_SHAKE:
					mAlarmType = TimerScheduleList.MODE_ALARM_SHAKE;
					break;
			}
		}
	}

	public void onNothingSelected(AdapterView<?> arg0) {}

	public void onFocusChange(View v, boolean hasFocus) {
		switch(v.getId()) {
			case R.id.dialog_schedule_sub_timer_pick_tv_hour:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 시간 포커스 : " + hasFocus);
				}
				
				if(!hasFocus) {
					if(mEditHour.getText().length() == 0) {
						mEditHour.setText(String.format("%02d", mHour));
					} else if(mEditHour.getText().length() == 1) {
						mEditHour.setText(String.format("%02d", mHour));
					}
				}
				
				
				break;
			case R.id.dialog_schedule_sub_timer_pick_tv_min:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 분 포커스 : " + hasFocus);
				}
				
				if(!hasFocus) {
					if(mEditMin.getText().length() == 0) {
						mEditMin.setText(String.format("%02d", mMin));
					} else if(mEditHour.getText().length() == 1) {
						mEditMin.setText(String.format("%02d", mMin));
					}
				}
				
				
				break;
			case R.id.dialog_schedule_sub_timer_pick_tv_sec:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "현재 초 포커스 : " + hasFocus);
				}
				
				if(mEditSec.getText().length() == 0) {
					mEditSec.setText(String.format("%02d", mSec));
				} else if(mEditHour.getText().length() == 1) {
					mEditSec.setText(String.format("%02d", mSec));
				}
				
				
				break;
		}
	}
}












