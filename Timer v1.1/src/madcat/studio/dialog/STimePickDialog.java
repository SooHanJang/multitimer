package madcat.studio.dialog;

import madcat.studio.constants.Constants;
import madcat.studio.timer.R;
import madcat.studio.utils.Utils;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;


public class STimePickDialog extends Dialog implements OnClickListener, OnFocusChangeListener {

	private final String TAG											=	"TimerPickDialog";
	private final boolean DEVELOPE_MODE									=	Constants.DEVELOPE_MODE;
	private final float DIALOG_SIZE										=	1.2f;
	
	private Context mContext;
	
	private EditText mEditHour, mEditMin, mEditSec;
	private Button mBtnHourPlus, mBtnHourMinus, mBtnMinPlus, mBtnMinMinus, mBtnSecPlus, mBtnSecMinus, mBtnSetTimeOk;

	private TextWatcher mWatcherHour, mWatcherMin, mWatcherSec;
	
	private int mHour, mMin, mSec;
	
	private boolean mDialogState										=	false;
	
	public STimePickDialog(Context context) {
		super(context);
		mContext = context;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_time_pick);
		setTitle(mContext.getString(R.string.dialog_title_time_pick));
		
		// Resource Id 설정
		mEditHour = (EditText)findViewById(R.id.dialog_timer_pick_edit_hour);
		mEditMin = (EditText)findViewById(R.id.dialog_timer_pick_edit_min);
		mEditSec = (EditText)findViewById(R.id.dialog_timer_pick_edit_sec);
		
		mBtnHourPlus = (Button)findViewById(R.id.dialog_timer_pick_btn_hour_plus);
		mBtnHourMinus = (Button)findViewById(R.id.dialog_timer_pick_btn_hour_minus);
		
		mBtnMinPlus = (Button)findViewById(R.id.dialog_timer_pick_btn_min_plus);
		mBtnMinMinus = (Button)findViewById(R.id.dialog_timer_pick_btn_min_minus);
		
		mBtnSecPlus = (Button)findViewById(R.id.dialog_timer_pick_btn_sec_plus);
		mBtnSecMinus = (Button)findViewById(R.id.dialog_timer_pick_btn_sec_minus);
		
		mBtnSetTimeOk = (Button)findViewById(R.id.dialog_timer_pick_btn_set_ok);
		
		// 리스너 설정
		mBtnHourPlus.setOnClickListener(this);
		mBtnHourMinus.setOnClickListener(this);
		
		mBtnMinPlus.setOnClickListener(this);
		mBtnMinMinus.setOnClickListener(this);
		
		mBtnSecPlus.setOnClickListener(this);
		mBtnSecMinus.setOnClickListener(this);
		
		mBtnSetTimeOk.setOnClickListener(this);
		
		mEditHour.setOnFocusChangeListener(this);
		mEditMin.setOnFocusChangeListener(this);
		mEditSec.setOnFocusChangeListener(this);
		
		// TextWatcher 설정
		setTimeWatcherListener();
		
		// TextView 시간 값 설정
		mEditHour.setText(String.format("%02d", mHour));
		mEditMin.setText(String.format("%02d", mMin));
		mEditSec.setText(String.format("%02d", mSec));
		
	}
	
	public boolean getDialogState() {	return mDialogState;	}
	public int getTimePickHour() {	return mHour;	}
	public int getTimePickMin() {	return mMin;	}
	public int getTimePickSec() {	return mSec;	}
	
	public void setTimePickHour(int hour) {	this.mHour = hour;	}
	public void setTimePickMin(int min) {	this.mMin = min;	}
	public void setTimePickSec(int sec) {	this.mSec = sec;	}
	
	private void setTimeWatcherListener() {
		mWatcherHour = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int hour = Integer.parseInt(s.toString());
					
					if(hour > 99) {
						mHour = 99;
						mEditHour.setText(String.format("%02d", mHour));
					} else {
						mHour = hour;
					}
				} else {
					mHour = 0;
				}
			}
		};
		
		mWatcherMin = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int min = Integer.parseInt(s.toString());
					
					if(min > 59) {
						mMin = 59;
						mEditMin.setText(String.format("%02d", mMin));
					} else {
						mMin = min;
					}
				} else {
					mMin = 0;
				}
			}
		};
		
		mWatcherSec = new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			public void beforeTextChanged(CharSequence s, int start, int count,	int after) {}
			public void afterTextChanged(Editable s) {
				if(s.toString().length() > 0) {
					int sec = Integer.parseInt(s.toString());
					
					if(sec > 59) {
						mSec = 59;
						mEditSec.setText(String.format("%02d", mSec));
					} else {
						mSec = sec;
					}
				} else {
					mSec = 0;
				}
			}
		};
		
		mEditHour.addTextChangedListener(mWatcherHour);
		mEditMin.addTextChangedListener(mWatcherMin);
		mEditSec.addTextChangedListener(mWatcherSec);
		
	}
	
	public void onClick(View v) {
		switch(v.getId()) {
			case R.id.dialog_timer_pick_btn_hour_plus:
				mEditHour.requestFocus();
				
				mHour++;
				
				if(mHour > 99) {
					mHour = 0;
				}
				
				mEditHour.setText(String.format("%02d", mHour));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_hour_minus:
				mEditHour.requestFocus();
				
				mHour--;
				
				if(mHour < 0) {
					mHour = 99;
				}

				mEditHour.setText(String.format("%02d", mHour));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_min_plus:
				mEditMin.requestFocus();
				
				mMin++;
				
				if(mMin >= 60) {
					mMin = 0;
				}
				
				mEditMin.setText(String.format("%02d", mMin));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_min_minus:
				mEditMin.requestFocus();
				
				mMin--;
				
				if(mMin < 0) {
					mMin = 59;
				}
				
				mEditMin.setText(String.format("%02d", mMin));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_sec_plus:
				mEditSec.requestFocus();
				
				mSec++;
				
				if(mSec >= 60) {
					mSec = 0;
				}
				
				mEditSec.setText(String.format("%02d", mSec));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_sec_minus:
				mEditSec.requestFocus();
				
				mSec--;
				
				if(mSec < 0) {
					mSec = 59;
				}
				
				mEditSec.setText(String.format("%02d", mSec));
				
				break;
				
				
			case R.id.dialog_timer_pick_btn_set_ok:
				if(DEVELOPE_MODE) {
					Log.d(TAG, "전달할 시간 : " + mHour + "시 " + mMin + "분 " + mSec + "초");
				}
				
				Utils.hideSoftKeyboard(mContext, mEditHour);
				
				mDialogState = true;
				dismiss();
				break;
		}
	}

	public void onFocusChange(View v, boolean hasFocus) {
		switch(v.getId()) {
			case R.id.dialog_timer_pick_edit_hour:
				if(!hasFocus) {
					if(mEditHour.getText().length() == 0) {
						mEditHour.setText(String.format("%02d", mHour));
					} else if(mEditHour.getText().length() == 1) {
						mEditHour.setText(String.format("%02d", mHour));
					}
				}
				
				
				break;
			case R.id.dialog_timer_pick_edit_min:
				if(!hasFocus) {
					if(mEditMin.getText().length() == 0) {
						mEditMin.setText(String.format("%02d", mMin));
					} else if(mEditHour.getText().length() == 1) {
						mEditMin.setText(String.format("%02d", mMin));
					}
				}
				
				
				break;
			case R.id.dialog_timer_pick_edit_sec:
				if(mEditSec.getText().length() == 0) {
					mEditSec.setText(String.format("%02d", mSec));
				} else if(mEditHour.getText().length() == 1) {
					mEditSec.setText(String.format("%02d", mSec));
				}
				
				
				break;
		}
	}
}









