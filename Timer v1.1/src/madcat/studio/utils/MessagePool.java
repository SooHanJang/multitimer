package madcat.studio.utils;

import android.app.Application;
import android.content.Context;

public class MessagePool extends Application {
	
	private boolean mTimerListDelFlag										=	false;

	public boolean isTimerListDelFlag() {	return mTimerListDelFlag;	}
	public void setTimerListDelFlag(boolean timerListDelFlag) {	this.mTimerListDelFlag = timerListDelFlag;	}
}
