package madcat.studio.utils;

import android.content.Context;

public class ContextPool {
	
	private static ContextPool mInstance = new ContextPool();
	
	private Context mContext;

	private ContextPool() {}
	
	
	public void setContext(Context context) {	this.mContext = context;	}
	public Context getContext() {	return mContext;	}
	
	
	public static ContextPool getInstance() {
		return mInstance;
	}

}
