package madcat.studio.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryTimer;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.data.StopTimer;
import madcat.studio.timer.R;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class Utils {
	
	private static final String TAG										=	"Utils";
	private static boolean DEVELOPE_MODE								=	true;
	
	public static final int ADD_CATEGORY_SIZE							=	1;
	public static final int REMOVE_CATEGORY_SIZE						=	2;
	
	/**
	 * 핸드폰 화면의 가로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 가로 크기 dip
	 */
	
	public static int getScreenDipWidthSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipWidth = (int) (displayMetrics.widthPixels / displayMetrics.density);
		
		return dipWidth;
	}

	/**
	 * 핸드폰 화면의 세로 크기를 dip 단위로 가져옵니다.
	 * 
	 * @param context
	 * @return int형의 핸드폰 화면의 세로 크기 dip
	 */
	
	public static int getScreenDipHeightSize(Context context) {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		windowManager.getDefaultDisplay().getMetrics(displayMetrics);
		
		int dipHeight = (int) (displayMetrics.heightPixels / displayMetrics.density);
		
		return dipHeight;
	}
	
	/**
	 * dip 를 pixel 로 변환합니다.
	 * 
	 * @param context
	 * @param dip
	 * @return int형의 pixel 값
	 */
	
	public static int pixelFromDip(Context context, int dip) {
		Resources resources = context.getResources();
		int pixel = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, resources.getDisplayMetrics());
		return pixel;
	}
	
	/**
	 * 해당 카테고리가 데이터베이스에 중복이 존재하는지 여부를 체크합니다.
	 * (반드시, 호출 하는 곳에서 db.close() 를 호출해야 합니다.)
	 * 
	 * @param sdb - 데이터베이스 객체
	 * @param category - 중복 검사할 카테고리
	 * @return
	 */
	public static boolean duplicationCategoryCheck(SQLiteDatabase sdb, String category) {
		String compareSql = "SELECT * FROM " + Constants.TABLE_CATEGORY +
							" WHERE " + Constants.CATEGORY_NAME + " = '" + category + "';";
		Cursor cursor = sdb.rawQuery(compareSql, null);
		
		if(cursor.getCount() == 0) {
			cursor.close();
			return false;
		} else {
			cursor.close();
			return true;
		}
	}
	
	public static boolean duplicationTimerCheck(SQLiteDatabase sdb, String timer) {
		String compareSql = "SELECT * FROM " + Constants.TABLE_TIMER +
							" WHERE " + Constants.TIMER_NAME + " = '" + timer + "';";
		Cursor cursor = sdb.rawQuery(compareSql, null);
		
		if(cursor.getCount() == 0) {
			cursor.close();
			return false;
		} else {
			cursor.close();
			return true;
		}
	}
	
	/**
	 * Category Table 에 저장된 카테고리를 String 배열 타입으로 리턴합니다.
	 * @return items;
	 */
	
	public static String[] getCategoryListToString(Context context) {
		String[] items;

		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String categorySql = "SELECT * FROM " + Constants.TABLE_CATEGORY;
		
		Cursor cursor = sdb.rawQuery(categorySql, null);
		items = new String[cursor.getCount()];
		int i=0;

		while(cursor.moveToNext()) {
			items[i] = cursor.getString(1);
			i++;
		}
		
		cursor.close();
		sdb.close();
		
		return items;
	}
	
	public static ArrayList<CategoryTimer> getCategoryList(Context context) {
		ArrayList<CategoryTimer> items = new ArrayList<CategoryTimer>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String categoryListSql = "SELECT * FROM " + Constants.TABLE_CATEGORY;
		Cursor categoryCursor = sdb.rawQuery(categoryListSql, null);
		
		while(categoryCursor.moveToNext()) {
			CategoryTimer categoryTimer = new CategoryTimer();
			categoryTimer.setName(categoryCursor.getString(1));
			
			String categorySizeSql = null;
			
			if(categoryCursor.getString(1).equals(Constants.DEFAULT_ALL_CATEGORY)) {
				categorySizeSql = "SELECT * FROM " + Constants.TABLE_TIMER;
				
			} else {
				categorySizeSql = "SELECT * FROM " + Constants.TABLE_TIMER +
								  " WHERE " + Constants.TIMER_CATEGORY + "='" + categoryCursor.getString(1) + "'";
			}
				
			Cursor sizeCursor = sdb.rawQuery(categorySizeSql, null); 
			categoryTimer.setTimerSize(sizeCursor.getCount());
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "categorySize : " + sizeCursor.getCount());
			}
			
			sizeCursor.close();
			
			items.add(categoryTimer);
		}
		
		categoryCursor.close();
		sdb.close();
		
		return items;
	}
	
	public static ArrayList<ScheduleTimer> getTimerAllList(Context context) {
		ArrayList<ScheduleTimer> items = new ArrayList<ScheduleTimer>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String timerListSql = "SELECT * FROM " + Constants.TABLE_TIMER;
		Cursor timerCurosr = sdb.rawQuery(timerListSql, null);
		
		while(timerCurosr.moveToNext()) {
			ScheduleTimer scheduleTimer = new ScheduleTimer();
			scheduleTimer.setTitle(timerCurosr.getString(2));
			scheduleTimer.setHour(timerCurosr.getInt(3));
			scheduleTimer.setMin(timerCurosr.getInt(4));
			scheduleTimer.setSec(timerCurosr.getInt(5));
			scheduleTimer.setAlarmType(timerCurosr.getInt(6));
			scheduleTimer.setScheduleFlag(timerCurosr.getInt(7));
			scheduleTimer.setScheduleBytes(timerCurosr.getBlob(8));
			
			items.add(scheduleTimer);
		}
		
		timerCurosr.close();
		sdb.close();
		
		return items;
	}
	
	public static ArrayList<ScheduleTimer> getTimerList(Context context, String category) {
		ArrayList<ScheduleTimer> items = new ArrayList<ScheduleTimer>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String timerListSql = "SELECT * FROM " + Constants.TABLE_TIMER + " WHERE " + Constants.TIMER_CATEGORY + "='" + category + "'";
		Cursor timerCurosr = sdb.rawQuery(timerListSql, null);
		
		while(timerCurosr.moveToNext()) {
			ScheduleTimer scheduleTimer = new ScheduleTimer();
			scheduleTimer.setTitle(timerCurosr.getString(2));
			scheduleTimer.setHour(timerCurosr.getInt(3));
			scheduleTimer.setMin(timerCurosr.getInt(4));
			scheduleTimer.setSec(timerCurosr.getInt(5));
			scheduleTimer.setAlarmType(timerCurosr.getInt(6));
			scheduleTimer.setScheduleFlag(timerCurosr.getInt(7));
			scheduleTimer.setScheduleBytes(timerCurosr.getBlob(8));
			
			items.add(scheduleTimer);
		}
		
		timerCurosr.close();
		sdb.close();
		
		return items;
	}
	
	/**
	 * DB 에서 likeName 이 포함되어 있는 타이머를 ArrayList<ScheduleTimer> 타입으로 가져온다.
	 * 
	 * @param context
	 * @param likeName
	 * @return
	 */
	
	public static ArrayList<ScheduleTimer> getSearchTimerList(Context context, String likeName) {
		ArrayList<ScheduleTimer> result = new ArrayList<ScheduleTimer>();
		
		SQLiteDatabase sdb = SQLiteDatabase.openDatabase(context.getDatabasePath(Constants.DATABASE_NAME).toString(), 
				null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		
		String searchSql = "SELECT * FROM " + Constants.TABLE_TIMER + 
	 					   " WHERE " + Constants.TIMER_NAME + " LIKE '%" + likeName + "%'";
		Cursor searchCursor = sdb.rawQuery(searchSql, null);
		
		while(searchCursor.moveToNext()) {
			ScheduleTimer item = new ScheduleTimer();
			
			item.setCategory(searchCursor.getString(1));
			item.setTitle(searchCursor.getString(2));
			item.setHour(searchCursor.getInt(3));
			item.setMin(searchCursor.getInt(4));
			item.setSec(searchCursor.getInt(5));
			item.setAlarmType(searchCursor.getInt(6));
			item.setScheduleFlag(searchCursor.getInt(7));
			item.setScheduleBytes(searchCursor.getBlob(8));
			
			result.add(item);
		}
		
		searchCursor.close();
		sdb.close();
		
		return result;
	}
	
	/**
	 * 가상 키보드를 숨기는 함수입니다.
	 * 호출 시, 반드시 View 가 Focus 를 가지고 있어야 작동합니다.
	 * 
	 * @param context
	 * @param view
	 */
	
	public static void hideSoftKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	/**
	 * Object 타입을 byte 타입으로 Serialize 합니다.
	 * 
	 * @param object
	 * @return
	 */
	
	public static byte[] serializeObject(Object object) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "직렬화 시작");
		}
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		
		try {
			ObjectOutputStream oos = new ObjectOutputStream(bos);
			oos.writeObject(object);
			oos.close();
			
			byte[] buf = bos.toByteArray();
		
			if(DEVELOPE_MODE) {
				Log.d(TAG, "직렬화 성공");
			}
			
			return buf;
		} catch (IOException e) {
			e.printStackTrace();
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "직렬화 실패");
			}
			
			return null;
		}
	}
	
	/**
	 * byte 타입을 해당 Object 로 deSerialize 합니다. 
	 * 
	 * @param b
	 * @return
	 */
	
	public static Object deSerializeObject(byte[] b) {
		if(DEVELOPE_MODE) {
			Log.d(TAG, "Deserialize 시작");
		}
		
		try {
			ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(b));
			Object object = ois.readObject();
			
			ois.close();
			
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Deserialize 성공");
			}
			
			return object;
		} catch(IOException ie) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Deserialize 실패");
			}
			
			return null;
		} catch (ClassNotFoundException e) {
			if(DEVELOPE_MODE) {
				Log.d(TAG, "Deserialize 실패");
			}
			
			e.printStackTrace();
			return null;
		}
	}
	
	public static String timeToString(int hour, int min, int sec) {
		String result = "";
		
		if(hour != 0) {
			result += hour + "시간" + Constants.WHITE_SPACE;
		}
		
		if(min != 0) {
			result += min + "분" + Constants.WHITE_SPACE;
		}
		
		result += sec + "초";
		
		return result;
	}
	
	public static String timeToString(StopTimer timer) {
		String result = "";
		
		int hour = timer.getHour();
		int min = timer.getMin();
		int sec = timer.getSec();
		
		if(hour != 0) {
			result += hour + "시간" + Constants.WHITE_SPACE;
		}
		
		if(min != 0) {
			result += min + "분" + Constants.WHITE_SPACE;
		}
		
		result += sec + "초";
		
		return result;
	}
	
	public static String totalTimeToString(int totalSec) {
		int hour = totalSec / 3600;
		int min = (totalSec % 3600) / 60;
		int sec = (totalSec % 3600) % 60;
		
		return timeToString(hour, min, sec);
	}
	
	public static void setDisplayTheme(int index, TextView textHour, TextView textMin, TextView textSec, TextView textMSec) {
		switch(index) {
			case 0:				//	블랙(기본)
				textHour.setBackgroundResource(R.drawable.shape_timer_view_black);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_black);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_black);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_black);
				break;
			case 1:				//	블루
				textHour.setBackgroundResource(R.drawable.shape_timer_view_blue);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_blue);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_blue);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_blue);
				break;
			case 2:				//	브라운
				textHour.setBackgroundResource(R.drawable.shape_timer_view_brown);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_brown);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_brown);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_brown);
				break;
			case 3:				//	화이트
				textHour.setBackgroundResource(R.drawable.shape_timer_view_white);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_white);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_white);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_white);
				break;
			case 4:				//	옐로우 그린
				textHour.setBackgroundResource(R.drawable.shape_timer_view_yellow_green);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_yellow_green);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_yellow_green);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_yellow_green);
				break;
			case 5:				//	퍼플
				textHour.setBackgroundResource(R.drawable.shape_timer_view_puple);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_puple);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_puple);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_puple);
				break;
			case 6:				//	핑크
				textHour.setBackgroundResource(R.drawable.shape_timer_view_pink);
				textMin.setBackgroundResource(R.drawable.shape_timer_view_pink);
				textSec.setBackgroundResource(R.drawable.shape_timer_view_pink);
				textMSec.setBackgroundResource(R.drawable.shape_timer_view_pink);
				break;
		}
	}
	
//	public static void modifyCategorySize(SQLiteDatabase sdb, ContentValues values, int mode, String category) {
//		if(values == null) {
//			values = new ContentValues();
//		}
//		
//		values.clear();
//		
//		// 해당 카테고리의 사이즈를 추가하여 업데이트
//		switch(mode) {
//			case ADD_CATEGORY_SIZE:
//				String categorySql = "SELECT COUNT(" + Constants.TIMER_CATEGORY + ")" +
//									 " FROM " + Constants.TABLE_TIMER + 
//									 " WHERE " + Constants.TIMER_CATEGORY + "='" + category + "'";
//	
//				Cursor countCursor = sdb.rawQuery(categorySql, null);
//				countCursor.moveToFirst();
//				
//				int categorySize = countCursor.getInt(0);
//				
//				if(DEVELOPE_MODE) {
//					Log.d(TAG, "category size : " + categorySize);
//				}
//				
//				values.put(Constants.CATEGORY_SIZE, categorySize);
//				sdb.update(Constants.TABLE_CATEGORY, values, Constants.CATEGORY_NAME + "='" + category + "'", null);
//				
//				countCursor.close();
//				
//				values.clear();
//				
//				String allSql = "SELECT COUNT(" + Constants.TIMER_CATEGORY + ")" + 
//							" FROM " + Constants.TABLE_TIMER;
//				
//				Cursor allCursor = sdb.rawQuery(allSql, null);
//				allCursor.moveToFirst();
//				
//				values.put(Constants.CATEGORY_SIZE, allCursor.getInt(0));
//				sdb.update(Constants.TABLE_CATEGORY, values, Constants.CATEGORY_NAME + "='" + Constants.DEFAULT_ALL_CATEGORY + "'", null);
//				
//				allCursor.close();
//				break;
//		}
//	}
}








