package madcat.studio.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.LinearLayout;

public class CheckableLinearLayout extends LinearLayout implements Checkable {
	final String NS = "http://schemas.android.com/apk/res/madcat.studio.timer";
	final String ATTR = "checkable";

	int checkableId;
	Checkable checkable;

	public CheckableLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		checkableId = attrs.getAttributeResourceValue(NS, ATTR, 0);
	}

	public boolean isChecked() {
		checkable = (Checkable) findViewById(checkableId);
		if (checkable == null)
			return false;
		return checkable.isChecked();
	}

	public void setChecked(boolean checked) {
		checkable = (Checkable) findViewById(checkableId);
		if (checkable == null)
			return;
		checkable.setChecked(checked);
	}

	public void toggle() {
		checkable = (Checkable) findViewById(checkableId);
		if (checkable == null)
			return;
		checkable.toggle();
	}
}
