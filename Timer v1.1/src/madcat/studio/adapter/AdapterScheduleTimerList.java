package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.timer.R;
import madcat.studio.timer.STimerList;
import madcat.studio.timer.STimerScheduleTimerList;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterScheduleTimerList extends ArrayAdapter<ScheduleTimer> {
	
	private final String TAG										=	"AdapterScheduleTimerList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId, mMode;
	private ArrayList<ScheduleTimer> mItems;

	public AdapterScheduleTimerList(Context context, int resId, ArrayList<ScheduleTimer> items, int mode) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mMode = mode;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.linearCategory = (LinearLayout)convertView.findViewById(R.id.schedule_timer_list_category_layout);
			holder.textTimerTitle = (TextView)convertView.findViewById(R.id.schedule_timer_list_title);
			holder.textTimerTime = (TextView)convertView.findViewById(R.id.schedule_timer_list_time);
			holder.textCategory = (TextView)convertView.findViewById(R.id.schedule_timer_list_category);
			holder.imageScheduleIcon = (ImageView)convertView.findViewById(R.id.schedule_timer_list_schedule_icon);
			holder.imageAlarmType = (ImageView)convertView.findViewById(R.id.schedule_timer_list_alarm_type_icon);
			holder.imageTimerStart = (ImageView)convertView.findViewById(R.id.schedule_timer_list_start);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		ScheduleTimer item = mItems.get(position);
		
		holder.textTimerTitle.setText(item.getTitle());
		holder.textTimerTitle.setSelected(true);
		
		/*
		holder.textTimerTime.setText(String.format("%02d" + mContext.getString(R.string.adapter_time_format_hour) + " "
				+ "%02d" + mContext.getString(R.string.adapter_time_format_min) + " "
				+ "%02d" + mContext.getString(R.string.adapter_time_format_sec), item.getHour(), item.getMin(), item.getSec())); */
		
		// 스케쥴 별 시간 설정 코드
		if(item.getHour() <= 0 && item.getMin() <= 0) {
			holder.textTimerTime.setText(item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() <= 0 && item.getMin() > 0) {
			holder.textTimerTime.setText(item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() > 0 && item.getMin() > 0) {
			holder.textTimerTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else {
			holder.textTimerTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		}
		

		switch(item.getScheduleFlag()) {
			case ScheduleTimer.SCHEDULE_ON:
				holder.imageScheduleIcon.setVisibility(View.VISIBLE);
				break;
			case ScheduleTimer.SCHEDULE_OFF:
				holder.imageScheduleIcon.setVisibility(View.GONE);
				break;
		}
		
		int alarmResId = mContext.getResources().getIdentifier("timer_icon_alarm_type_" + item.getAlarmType(), "drawable", mContext.getPackageName());
		holder.imageAlarmType.setBackgroundResource(alarmResId);

		switch(mMode) {
			case STimerScheduleTimerList.MODE_IN_CATEGORY:
				holder.linearCategory.setVisibility(View.GONE);
				break;
			case STimerScheduleTimerList.MODE_IN_SEARCH:
				holder.linearCategory.setVisibility(View.VISIBLE);
				holder.textCategory.setText("[" + item.getCategory() + "]");
				break;
		}
		
		holder.imageTimerStart.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, position + "번째 타이머를 시작합니다");
				}
					
				// Thread 에 타이머를 등록한다.
				ScheduleTimer clickItem = mItems.get(position);
				
				// 타이머의 스케쥴이 존재하면 복원하여 같이 등록해준다.
				ArrayList<TimerScheduleList> scheduleItem = null;
				
				switch(clickItem.getScheduleFlag()) {
					case ScheduleTimer.SCHEDULE_ON:
						scheduleItem =	(ArrayList<TimerScheduleList>) Utils.deSerializeObject(clickItem.getScheduleBytes()); 
						break;
					case ScheduleTimer.SCHEDULE_OFF:
						scheduleItem = null;
						break;
				}
				
				if(DEVELOPE_MODE) {
					if(scheduleItem != null) {
						Log.d(TAG, "복원 된 Schedule 사이즈 : " + scheduleItem.size());
					} else {
						Log.d(TAG, "복원 된 값은 Null 입니다.");
					}
				}
				
				STimerManage timerManage = STimerManage.getInstance();
				timerManage.addStopTimerThread(mContext, STimer.TIMER_TYPE_SCHEDULE_TIMER, clickItem.getTitle(), 
						clickItem.getHour(), clickItem.getMin(), clickItem.getSec(), clickItem.getAlarmType(), scheduleItem);
				timerManage.startTimerThread(timerManage.getLastKey());
				
				
				Intent viewIntent = new Intent(mContext, STimerList.class);
				mContext.startActivity(viewIntent);
			}
		});
		
		return convertView;
	}
	
	class ViewHolder {
		public LinearLayout linearCategory;
		
		public TextView textTimerTitle;
		public TextView textTimerTime;
		public TextView textCategory;
		public ImageView imageScheduleIcon;
		public ImageView imageAlarmType;
		public ImageView imageTimerStart;
	}

}
