package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryTimer;
import madcat.studio.timer.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterCategoryDel extends ArrayAdapter<CategoryTimer> {
	
	private Context mContext;
	private ArrayList<CategoryTimer> mItems;
	
	private ArrayList<CategoryTimer> mDelItems;
	private ArrayList<Integer> mListItems;
	
	private LayoutInflater mInflater;
	private int mResId;
	
	public AdapterCategoryDel(Context context, int resId, ArrayList<CategoryTimer> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<CategoryTimer>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.linearRoot = (LinearLayout)convertView.findViewById(R.id.category_list_del_row_root_layout);
			holder.textCategoryName = (TextView)convertView.findViewById(R.id.category_list_del_row_name);
			holder.textCategorySize = (TextView)convertView.findViewById(R.id.category_list_del_row_timer_size);
			holder.checkCategory = (CheckBox)convertView.findViewById(R.id.category_list_del_row_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if(mItems.get(position).getName().equals(Constants.DEFAULT_ALL_CATEGORY)) {
			holder.checkCategory.setVisibility(View.GONE);
		} else {
			holder.checkCategory.setVisibility(View.VISIBLE);
			
			holder.linearRoot.setOnClickListener(new OnClickListener() {
				public void onClick(View v) {
					holder.checkCategory.setChecked(!holder.checkCategory.isChecked());
				}
			});
		}
		
		holder.textCategoryName.setText(mItems.get(position).getName());
		holder.textCategoryName.setSelected(true);
		holder.textCategorySize.setText("(" + mItems.get(position).getTimerSize() + ")");
		
		holder.checkCategory.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							return;
						}
					}
					mListItems.add(position);
					mDelItems.add(mItems.get(position));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
				}
			}
		});
		
		boolean reChecked = false;
		for(int i=0; i < mListItems.size(); i++) {
			if(mListItems.get(i) == position) {
				holder.checkCategory.setChecked(true);
				reChecked = true;
				break;
			}
		}
		
		if(!reChecked) {
			holder.checkCategory.setChecked(false);
		}
		
		return convertView;
		
	}
	
	public class ViewHolder {
		LinearLayout linearRoot;
		TextView textCategoryName;
		TextView textCategorySize;
		CheckBox checkCategory;
	}
	
	public ArrayList<CategoryTimer> getCheckedList() {
		return mDelItems;
	}

}











