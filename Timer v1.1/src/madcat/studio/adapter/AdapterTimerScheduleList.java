package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.timer.R;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AdapterTimerScheduleList extends ArrayAdapter<TimerScheduleList> {
	
	private final String TAG										=	"AdapterTimerSchedule";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	
	private ArrayList<TimerScheduleList> mItems;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	
	private String[] mAlarmType;
	
	public AdapterTimerScheduleList(Context context, int resId, ArrayList<TimerScheduleList> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mAlarmType = context.getResources().getStringArray(R.array.spin_alarm_type);
	}
	
	public void setItems(ArrayList<TimerScheduleList> items) {
		this.mItems = items;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			holder.imageRemove = (ImageView)convertView.findViewById(R.id.timer_schedule_list_row_remove);
			holder.textTime = (TextView)convertView.findViewById(R.id.timer_schedule_list_row_time);
			holder.textTitle = (TextView)convertView.findViewById(R.id.timer_schedule_list_row_title);
			holder.textAlarmType = (TextView)convertView.findViewById(R.id.timer_schedule_list_row_alarm_type);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder)convertView.getTag();
		}
		
		holder.imageRemove.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, position + "번째 삭제 버튼이 클릭");
				}
					
				mItems.remove(position);
				notifyDataSetChanged();
			}
		});
		
//		holder.textTime.setText(mItems.get(position).getTime());
		
		// 스케쥴 별 시간 설정 코드
		if(mItems.get(position).getHour() <= 0 && mItems.get(position).getMin() <= 0) {
			holder.textTime.setText(mItems.get(position).getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(mItems.get(position).getHour() <= 0 && mItems.get(position).getMin() > 0) {
			holder.textTime.setText(mItems.get(position).getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + mItems.get(position).getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(mItems.get(position).getHour() > 0 && mItems.get(position).getMin() > 0) {
			holder.textTime.setText(mItems.get(position).getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + mItems.get(position).getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + mItems.get(position).getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else {
			holder.textTime.setText(mItems.get(position).getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + mItems.get(position).getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + mItems.get(position).getSec() + mContext.getString(R.string.adapter_time_format_sec));
		}
		
		
		holder.textTitle.setText(mItems.get(position).getTitle());
		holder.textTitle.setSelected(true);
		holder.textAlarmType.setText("[" + mAlarmType[mItems.get(position).getAlarmType()] + "]");
		
		return convertView;
	}
	
	public class ViewHolder {
		ImageView imageRemove;
		TextView textTime;
		TextView textTitle;
		TextView textAlarmType;
	}

}
