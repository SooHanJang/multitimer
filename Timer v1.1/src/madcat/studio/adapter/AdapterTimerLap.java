package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.data.StopWatchLap;
import madcat.studio.timer.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterTimerLap extends ArrayAdapter<StopWatchLap> {
	
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	private ArrayList<StopWatchLap> mItems;

	public AdapterTimerLap(Context context, int resId, ArrayList<StopWatchLap> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setItems(ArrayList<StopWatchLap> items) {
		this.mItems = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		TimerLapViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new TimerLapViewHolder();
			
			holder.textIndex = (TextView)convertView.findViewById(R.id.timer_view_lab_index);
			holder.textInterval = (TextView)convertView.findViewById(R.id.timer_view_lap_interval_time);
			holder.textLap = (TextView)convertView.findViewById(R.id.timer_view_lap_time);
			
			convertView.setTag(holder);
		} else {
			holder = (TimerLapViewHolder) convertView.getTag();
		}
		
		setAdapterText(mItems.get(position), holder);
		
		return convertView;
	}
	
	private void setAdapterText(StopWatchLap timerLap, TimerLapViewHolder holder) {
		String lapMin, lapSec, ivMin, ivSec;
		
		holder.textIndex.setText(mContext.getString(R.string.adapter_time_lap_text) + " " + (timerLap.getIndex()+1));		//	�� �ε��� ����
		
		// Interval ����
		
		if(timerLap.getIvMin() < 10) {
			ivMin = "0" + timerLap.getIvMin();
		} else {
			ivMin = String.valueOf(timerLap.getIvMin());
		}
		
		if(timerLap.getIvSec() < 10) {
			ivSec = "0" + timerLap.getIvSec();
		} else {
			ivSec = String.valueOf(timerLap.getIvSec());
		}
		
		holder.textInterval.setText(timerLap.getIvHour() + ":" + ivMin + ":" + ivSec + "." + timerLap.getIvMilSec());
		
		// Ÿ�̸� �� ����
		
		if(timerLap.getLapMin() < 10) {
			lapMin = "0" + timerLap.getLapMin();
		} else {
			lapMin = String.valueOf(timerLap.getLapMin());
		}
		
		if(timerLap.getLapSec() < 10) {
			lapSec = "0" + timerLap.getLapSec();
		} else {
			lapSec = String.valueOf(timerLap.getLapSec());
		}
		
		holder.textLap.setText(timerLap.getLapHour() + ":" + lapMin + ":" + lapSec + "." + timerLap.getLapMilSec());
		
		
	}
	
	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	class TimerLapViewHolder {
		public TextView textIndex;
		public TextView textInterval;
		public TextView textLap;
	}

}
