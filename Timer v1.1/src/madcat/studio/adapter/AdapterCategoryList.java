package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.data.CategoryTimer;
import madcat.studio.timer.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterCategoryList extends ArrayAdapter<CategoryTimer> {
	
	private ArrayList<CategoryTimer> mItems;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;

	public AdapterCategoryList(Context context, int resId, ArrayList<CategoryTimer> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public void setCategoryItems(ArrayList<CategoryTimer> items) {
		this.mItems = items;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.textCategoryName = (TextView)convertView.findViewById(R.id.category_list_row_name);
			holder.textCategorySize = (TextView)convertView.findViewById(R.id.category_list_row_timer_size);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.textCategoryName.setText(mItems.get(position).getName());
		holder.textCategoryName.setSelected(true);
		holder.textCategorySize.setText("(" + mItems.get(position).getTimerSize() + ")");
		
		return convertView;
	}
	
	
	class ViewHolder {
		public TextView textCategoryName;
		public TextView textCategorySize;
	}
	

}
