package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.CategoryTimer;
import madcat.studio.data.ScheduleTimer;
import madcat.studio.timer.R;
import madcat.studio.timer.STimerScheduleTimerList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class AdapterScheduleTimerDel extends ArrayAdapter<ScheduleTimer> {
	
	private final String TAG										=	"AdapterScheduleTimerList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;
	
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId, mMode;
	
	private ArrayList<ScheduleTimer> mItems;
	private ArrayList<ScheduleTimer> mDelItems;
	private ArrayList<Integer> mListItems;
	
	public AdapterScheduleTimerDel(Context context, int resId, ArrayList<ScheduleTimer> items, int mode) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mMode = mode;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		this.mDelItems = new ArrayList<ScheduleTimer>();
		this.mListItems = new ArrayList<Integer>();
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.linearRoot = (LinearLayout)convertView.findViewById(R.id.schedule_timer_list_del_linear_root);
			holder.linearCategory = (LinearLayout)convertView.findViewById(R.id.schedule_timer_list_del_category_layout);
			
			holder.textTimerTitle = (TextView)convertView.findViewById(R.id.schedule_timer_list_del_title);
			holder.textTimerTime = (TextView)convertView.findViewById(R.id.schedule_timer_list_del_time);
			holder.textCategory = (TextView)convertView.findViewById(R.id.schedule_timer_list_del_category);
			holder.imageScheduleIcon = (ImageView)convertView.findViewById(R.id.schedule_timer_list_del_schedule_icon);
			holder.imageAlarmType = (ImageView)convertView.findViewById(R.id.schedule_timer_list_del_alarm_type_icon);
			holder.checkDelTimer = (CheckBox)convertView.findViewById(R.id.schedule_timer_list_del_check);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		ScheduleTimer item = mItems.get(position);
		
		holder.linearRoot.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				holder.checkDelTimer.setChecked(!holder.checkDelTimer.isChecked());
			}
		});
		
		holder.textTimerTitle.setText(item.getTitle());
		holder.textTimerTitle.setSelected(true);
		
		if(item.getHour() <= 0 && item.getMin() <= 0) {
			holder.textTimerTime.setText(item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() <= 0 && item.getMin() > 0) {
			holder.textTimerTime.setText(item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() > 0 && item.getMin() > 0) {
			holder.textTimerTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else {
			holder.textTimerTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		}
		

		switch(item.getScheduleFlag()) {
			case ScheduleTimer.SCHEDULE_ON:
				holder.imageScheduleIcon.setVisibility(View.VISIBLE);
				break;
			case ScheduleTimer.SCHEDULE_OFF:
				holder.imageScheduleIcon.setVisibility(View.GONE);
				break;
		}
		
		int alarmResId = mContext.getResources().getIdentifier("timer_icon_alarm_type_" + item.getAlarmType(), "drawable", mContext.getPackageName());
		holder.imageAlarmType.setBackgroundResource(alarmResId);
		
		switch(mMode) {
			case STimerScheduleTimerList.MODE_IN_CATEGORY:
				holder.linearCategory.setVisibility(View.GONE);
				break;
			case STimerScheduleTimerList.MODE_IN_SEARCH:
				holder.linearCategory.setVisibility(View.VISIBLE);
				holder.textCategory.setText("[" + item.getCategory() + "]");
				break;
		}
		
		holder.checkDelTimer.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							return;
						}
					}
					mListItems.add(position);
					mDelItems.add(mItems.get(position));
				} else {
					for(int i=0; i < mListItems.size(); i++) {
						if(mListItems.get(i) == position) {
							mListItems.remove(i);
							mDelItems.remove(mItems.get(position));
							break;
						}
					}
				}
			}
		});
		
		boolean reChecked = false;
		for(int i=0; i < mListItems.size(); i++) {
			if(mListItems.get(i) == position) {
				holder.checkDelTimer.setChecked(true);
				reChecked = true;
				break;
			}
		}
		
		if(!reChecked) {
			holder.checkDelTimer.setChecked(false);
		}
		
		
		return convertView;
	}
	
	public ArrayList<ScheduleTimer> getCheckedList() {
		return mDelItems;
	}
	
	class ViewHolder {
		public LinearLayout linearRoot;
		public LinearLayout linearCategory;
		
		public TextView textTimerTitle;
		public TextView textTimerTime;
		public TextView textCategory;
		public ImageView imageScheduleIcon;
		public ImageView imageAlarmType;
		public CheckBox checkDelTimer;
	}

}
