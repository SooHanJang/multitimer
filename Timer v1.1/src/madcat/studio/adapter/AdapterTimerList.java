package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.STimer;
import madcat.studio.data.STotal;
import madcat.studio.data.StopTimer;
import madcat.studio.manage.timer.STimerManage;
import madcat.studio.timer.R;
import madcat.studio.utils.MessagePool;
import madcat.studio.utils.Utils;
import android.content.Context;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


public class AdapterTimerList extends ArrayAdapter<STotal> {
	
	private final String TAG										=	"AdapterTimerList";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	private ArrayList<STotal> mItems;
	private MessagePool mMessagePool;
	
	private Thread mDelThread;
	
	public AdapterTimerList(Context context, int resId, ArrayList<STotal> items) {
		super(context, resId, items);
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mMessagePool = (MessagePool)mContext.getApplicationContext();
	}
	
	public void setItems(ArrayList<STotal> items) {
		this.mItems = items;
	}
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		TimerViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new TimerViewHolder();
			holder.linearTimerSchedule = (LinearLayout)convertView.findViewById(R.id.timer_list_row_schedule_layout);
			holder.textTimerScheduleName = (TextView)convertView.findViewById(R.id.timer_list_row_schedule_name);
			holder.textTimerScheduleTime = (TextView)convertView.findViewById(R.id.timer_list_row_schedule_time);
			
			holder.textTimerNumber = (TextView)convertView.findViewById(R.id.timer_list_row_number);
			holder.textTimerTime = (TextView)convertView.findViewById(R.id.timer_list_row_time);
			holder.imageTimerIcon = (ImageView)convertView.findViewById(R.id.timer_list_row_icon);
			holder.imageTimerDel = (ImageView)convertView.findViewById(R.id.timer_list_row_del);
			
			convertView.setTag(holder);
		} else {
			holder = (TimerViewHolder)convertView.getTag();
		}
		
		STimer timer = mItems.get(position).getTimer();
		
		int nowHour = timer.getHour();
		int nowMin = timer.getMin();
		int nowSec = timer.getSec();
		int nowTSecTime = (nowHour * 3600) + (nowMin * 60) + nowSec;
		
		holder.textTimerNumber.setText(String.valueOf(mItems.get(position).getKey()));
		holder.textTimerTime.setText(nowHour + mContext.getString(R.string.adapter_time_format_hour) + " " + 
				nowMin + mContext.getString(R.string.adapter_time_format_min) + " " + 
				nowSec + mContext.getString(R.string.adapter_time_format_sec));

		switch(timer.getType()) {
			case STimer.TIMER_TYPE_STOPWATCH:
				holder.imageTimerIcon.setBackgroundResource(R.drawable.timer_icon_stopwatch);
				holder.linearTimerSchedule.setVisibility(View.GONE);
				
				break;
			case STimer.TIMER_TYPE_SCHEDULE_TIMER:
				holder.imageTimerIcon.setBackgroundResource(R.drawable.timer_icon_stoptimer);

				// 스케쥴 설정 부분
				StopTimer stopTimer = (StopTimer)timer;		//	형 변환
				
				if(stopTimer.getScheduleTimerList() != null) {			//	스케쥴이 존재한다면
					int currentIndex = stopTimer.getScheduleIndex();
					
					// 스케쥴 갱신 부분
					if(currentIndex < stopTimer.getScheduleTimerList().size()) {
						// 스케쥴러의 시간을 가져온다.
						int sTSecTime = stopTimer.getScheduleTimerList().get(currentIndex).getTotalSecTime();
						int gapTime = nowTSecTime - sTSecTime;
						
						String result = Utils.totalTimeToString(gapTime);
						
						// 스케쥴러를 리스트에 표시하는 부분
						holder.linearTimerSchedule.setVisibility(View.VISIBLE);
						
						String currentTitle = stopTimer.getScheduleTimerList().get(currentIndex).getTitle();
						holder.textTimerScheduleTime.setText("[ " + result + mContext.getString(R.string.adapter_time_remaining) + " ]");
						holder.textTimerScheduleName.setText(currentTitle);
						
						
					} else {
						holder.textTimerScheduleName.setText(mContext.getString(R.string.adapter_time_not_schedule));
						holder.textTimerScheduleTime.setVisibility(View.GONE);
					}
				} else {												//	스케쥴이 존재하지 않는다면
					holder.linearTimerSchedule.setVisibility(View.GONE);
				}
				
				break;
			case STimer.TIMER_TYPE_GENERAL_TIMER:
				holder.imageTimerIcon.setBackgroundResource(R.drawable.timer_icon_stoptimer);
				holder.linearTimerSchedule.setVisibility(View.GONE);
				break;
		}
		
		// 삭제 버튼
		holder.imageTimerDel.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(DEVELOPE_MODE) {
					Log.d(TAG, "타이머 삭제 버튼을 클릭");
					Log.d(TAG, "처음 Thread 상태 : " + mDelThread);
				}

				if(mDelThread == null) {
					mDelThread = new Thread(new Runnable() {
						public void run() {
							if(!mMessagePool.isTimerListDelFlag()) {
								mMessagePool.setTimerListDelFlag(true);
								STimerManage manageTimer = STimerManage.getInstance();
								manageTimer.removeTimerThread(mItems.get(position).getKey());
								
								SystemClock.sleep(1000);
								
								mMessagePool.setTimerListDelFlag(false);
								
								mDelThread.interrupt();
								mDelThread = null;
							}
						}
					});
				}

				if(!mMessagePool.isTimerListDelFlag()) {
					mDelThread.start();
				}
			}
		});
		
		return convertView;
	}
	
	public class TimerViewHolder {
		public TextView textTimerNumber;
		public TextView textTimerTime;
		
		public LinearLayout linearTimerSchedule;
		public TextView textTimerScheduleName;
		public TextView textTimerScheduleTime;
		
		public ImageView imageTimerIcon;
		public ImageView imageTimerDel;
	}
	
}







