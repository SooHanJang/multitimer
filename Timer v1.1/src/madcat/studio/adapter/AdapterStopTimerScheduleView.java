package madcat.studio.adapter;

import java.util.ArrayList;

import madcat.studio.constants.Constants;
import madcat.studio.data.TimerScheduleList;
import madcat.studio.timer.R;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AdapterStopTimerScheduleView extends ArrayAdapter<TimerScheduleList> {
	
	private final String TAG										=	"AdapterStopTimerScheduleView";
	private final boolean DEVELOPE_MODE								=	Constants.DEVELOPE_MODE;

	private ArrayList<TimerScheduleList> mItems;
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private int mResId;
	private int mCurrentListIndex;
	
	public AdapterStopTimerScheduleView(Context context, int resId, ArrayList<TimerScheduleList> items) {
		super(context, resId, items);
		
		this.mContext = context;
		this.mResId = resId;
		this.mItems = items;
		this.mLayoutInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.mCurrentListIndex = 0;
	}
	
	public void setCurrentListIndex(int index) {
		this.mCurrentListIndex = index;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if(convertView == null) {
			convertView = mLayoutInflater.inflate(mResId, null);
			
			holder = new ViewHolder();
			
			holder.textTitle = (TextView)convertView.findViewById(R.id.stop_timer_schedule_list_row_title);
			holder.textTime = (TextView)convertView.findViewById(R.id.stop_timer_schedule_list_row_time);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		TimerScheduleList item = mItems.get(position);
		
		holder.textTitle.setText(item.getTitle());
		
		// 스케쥴 별 시간 설정 코드
		if(item.getHour() <= 0 && item.getMin() <= 0) {
			holder.textTime.setText(item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() <= 0 && item.getMin() > 0) {
			holder.textTime.setText(item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else if(item.getHour() > 0 && item.getMin() > 0) {
			holder.textTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		} else {
			holder.textTime.setText(item.getHour() + mContext.getString(R.string.adapter_time_format_hour) + " " + item.getMin() + mContext.getString(R.string.adapter_time_format_min) + " " + item.getSec() + mContext.getString(R.string.adapter_time_format_sec));
		}

		if(position < mCurrentListIndex) {						//	이미 지나간 스케쥴
//			holder.textTitle.setTextColor(Color.BLUE);
//			holder.textTime.setTextColor(Color.BLUE);
			
			holder.textTitle.setTextColor(Color.rgb(122, 120, 116));
			holder.textTime.setTextColor(Color.rgb(122, 120, 116));
			
		} else if(position == mCurrentListIndex) {				//	곧 실행 될 스케쥴
//			holder.textTitle.setTextColor(Color.RED);
//			holder.textTime.setTextColor(Color.RED);
			
//			holder.textTitle.setTextColor(Color.rgb(148, 190, 66));
//			holder.textTime.setTextColor(Color.rgb(148, 190, 66));
			
			holder.textTitle.setTextColor(Color.rgb(253, 209, 2));
			holder.textTime.setTextColor(Color.rgb(253, 209, 2));
			
		} else {												//	아직 실행 안된 스케쥴
			holder.textTitle.setTextColor(Color.WHITE);
			holder.textTime.setTextColor(Color.WHITE);
		}

		return convertView;
	}
	
	@Override
	public boolean isEnabled(int position) {
		return false;
	}
	
	class ViewHolder {
		public TextView textTitle;
		public TextView textTime;
	}
}
