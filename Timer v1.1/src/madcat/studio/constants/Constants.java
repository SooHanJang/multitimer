package madcat.studio.constants;

public class Constants {

	public static final boolean DEVELOPE_MODE									=	false;
	
	public static final int APP_NUMBER											=	3211;
	
	public static final int LOADING_DELAY										=	1;					//	단위 Sec
	
	// 환경설정 관련 변수
	public static final String CONFIG_PREF_NAME									=	"TIMER_CONFIG_PREF";
	
	public static final String PREF_APP_FIRST									=	"PREF_APP_FIRST";
	public static final String PREF_ALARM_INDEX_PATH							=	"PREF_ALARM_INDEX_PATH";
	public static final String PREf_SCHEDULE_ALARM_INDEX						=	"PREF_SCHEDULE_ALARM_INDEX";
	public static final String PREF_DISPLAY_MODE								=	"PREF_DISPLAY_MODE";
	public static final String PREF_CHOOSE_THEME_INDEX							=	"PREF_CHOOSE_THEME_INDEX";
	
	// 데이터베이스 관련 변수
	public static final String DATABASE_NAME									=	"SmartTimer.db";
	public static final int DATABASE_VERSION									=	1;
	public static final int DATABASE_FILE_INDEX									=	1;
	public static final String DATABASE_ROOT_DIR								=	"/data/data/madcat.studio.timer/databases/";
	
	// 데이터베이스 테이블 관련 변수
	public static final String TABLE_CATEGORY									=	"CATEGORY";
	public static final String CATEGORY_ID										=	"_id";
	public static final String CATEGORY_NAME									=	"_name";
//	public static final String CATEGORY_SIZE									=	"_size";
	
	public static final String TABLE_TIMER										=	"TIMER";
	public static final String TIMER_ID											=	"_id";
	public static final String TIMER_CATEGORY									=	"_category";
	public static final String TIMER_NAME										=	"_name";
	public static final String TIMER_HOUR										=	"_hour";
	public static final String TIMER_MIN										=	"_min";
	public static final String TIMER_SEC										=	"_sec";
	public static final String TIMER_ALARM_TYPE									=	"_alarm_type";
	public static final String TIMER_ARRAY_FLAG									=	"_array_flag";
	public static final String TIMER_ARRAY										=	"_array";
	
	public static final String PUT_TIMER_MODE									=	"PUT_TIMER_MODE";
	public static final String PUT_TIMER_LOAD_KEY								=	"PUT_TIMER_START_KEY";
	
	public static final String PUT_TIMER_CATEGORY								=	"PUT_TIMER_CATEGORY";
	public static final String PUT_TIMER_DIALOG_MODE							=	"PUT_TIMER_DIALOG_MODE";
	public static final String PUT_TIMER_DIALOG_MODIFY_NAME						=	"PUT_TIMER_DIALOG_MODIFY_NAME";
	
	public static final String PUT_TIMER_LIST_MODE								=	"PUT_TIMER_LIST_MODE";
	public static final String PUT_TIMER_SEARCH_TIMER_NAME						=	"PUT_TIMER_SEARCH_TIMER_NAME";
	public static final String PUT_TIMER_SEARCH_TIMER_ARRAY						=	"PUT_TIMER_SEARCH_TIMER_ARRAY";
	
	public static final String PUT_TIMER_COMPELETE_ALARM_NAME					=	"PUT_TIMER_COMPLETE_ALARM_NAME";
	public static final String PUT_TIMER_COMPELETE_ALARM_TYPE					=	"PUT_TIMER_COMPLETE_ALARM_TYPE";
	
	public static final String DEFAULT_ALL_CATEGORY								=	"All";
	
	public static final String WHITE_SPACE										=	" ";
	
}
